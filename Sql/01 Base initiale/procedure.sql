use gsb;

if object_id('getLesVisiteurs','P') is not null 
	drop procedure getLesVisiteurs;
go
create procedure getLesVisiteurs as
	select nom, prenom
	from visiteur 
	order by nom;
go


if object_id('verifierConnexion','P') is not null 
	drop procedure verifierConnexion;
go
create procedure verifierConnexion(@nom varchar(25), @mdp date) as
	select id, nom, prenom, rue, codePostal, ville, dateEmbauche
		from visiteur 
		where visiteur.nom = @nom and visiteur.dateEmbauche = @mdp
go

if object_id('getLesTypes','P') is not null 
	drop procedure getLesTypes;
go
create procedure getLesTypes as
	Select id, libelle from TypePraticien order by libelle;
go

if object_id('getLesSpecialites','P') is not null 
	drop procedure getLesSpecialites;
go
create procedure getLesSpecialites as
	Select id, libelle from Specialite order by libelle; 
go


if object_id('getLesPraticiens','P') is not null 
	drop procedure getLesPraticiens;
go

create procedure getLesPraticiens(@idVisiteur varchar(3)) as
	  SELECT id, nom, prenom, rue, codePostal, ville, email, telephone, idType, idSpecialite 
      FROM praticien 
	  where substring(codePostal, 1, 2) = (SELECT substring(codePostal, 1, 2) from visiteur WHERE visiteur.id = @idVisiteur)
	  order by nom, prenom;

go


if object_id('getLesMotifs','P') is not null 
	drop procedure getLesMotifs;
go
create procedure getLesMotifs as
	Select id, libelle 
	from Motif
	order by libelle; 
go


if object_id('getLesFamilles','P') is not null 
	drop procedure getLesFamilles;
go
create procedure getLesFamilles as
	Select id, libelle 
	from Famille 
	order by libelle;
go

if object_id('getLesMedicaments','P') is not null 
	drop procedure getLesMedicaments;
go
         
create procedure getLesMedicaments as
	SELECT id, nom, composition, effets, contreIndication, idFamille 
	FROM medicament
	order by nom;
go

if object_id('getLesVisites','P') is not null 
	drop procedure getLesVisites;
go
         
create procedure getLesVisites(@idVisiteur varchar(3)) as
	SELECT id, date, heure, bilan, idMotif, idPraticien, premierMedicament, secondMedicament 
	from visite
	where idVisiteur = @idVisiteur
	order by date, heure;
go

-- ajout d'une visite

if object_id('ajouterVisite','P') is not null 
	drop procedure ajouterVisite;
go

create procedure ajouterVisite(@idVisiteur varchar(3), @idPraticien smallint, @idMotif int, @date date, @heure varchar(15), @idVisite int output ) as
		insert into visite (idVisiteur, idPraticien, idMotif, date, heure) values 
			(@idVisiteur, @idPraticien, @idMotif, @date, @heure)
		select @idVisite = (select @@IDENTITY);
go



-- modifier la planification d'une visite

if object_id('modifierPlanificationVisite','P') is not null 
	drop procedure modifierPlanificationVisite;
go

create procedure modifierPlanificationVisite(@idVisite int,  @date date, @heure varchar(15)) as
	update visite 
		set date = @date, heure = @heure
		where id = @idVisite
go


-- enregistrer le bilan d'une visite

if object_id('enregistrerBilanVisite','P') is not null 
	drop procedure enregistrerBilanVisite;
go

create procedure enregistrerBilanVisite(@idVisite int,  @bilan ntext, @premierMedicament varchar(10), @secondMedicament varchar(10)) as 
	update visite 
		set bilan = @bilan, premierMedicament = @premierMedicament, secondMedicament = @secondMedicament
		where id = @idVisite
go


if object_id('enregistrerBilanVisite2','P') is not null 
	drop procedure enregistrerBilanVisite2;
go

create procedure enregistrerBilanVisite2(@idVisite int,  @bilan ntext, @premierMedicament varchar(10)) as 
	update visite 
		set bilan = @bilan, premierMedicament = @premierMedicament, secondMedicament = null
		where id = @idVisite
go

-- supprimer une visite

if object_id('supprimerVisite','P') is not null 
	drop procedure supprimerVisite;
go

create procedure supprimerVisite(@idVisite int) as
	Delete from visite where id = @idVisite
go

