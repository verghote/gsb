use gsb;

delete from visite;
delete from motif;

delete from medicament;
delete from famille;

delete from praticien;
delete from typepraticien;
delete from specialite;

delete from visiteur;


insert into famille(id,libelle) values 
	('AA','Antalgiques en association'),
	 ('AAA','Antalgiques antipyr�tiques en association'),
	 ('AAC','Antid�presseur d''action centrale'),
	 ('AAH','Antivertigineux antihistaminique H1'),
	 ('ABA','Antibiotique antituberculeux'),
	 ('ABC','Antibiotique antiacn�ique local'),
	 ('ABP','Antibiotique de la famille des b�ta-lactamines (p�nicilline A)'),
	 ('AFC','Antibiotique de la famille des cyclines'),
	 ('AFM','Antibiotique de la famille des macrolides'),
	 ('AH','Antihistaminique H1 local'),
	 ('AIM','Antid�presseur imipraminique (tricyclique)'),
	 ('AIN','Antid�presseur inhibiteur s�lectif de la recapture de la s�rotonine'),
	 ('ALO','Antibiotique local (ORL)'),
	 ('ANS','Antid�presseur IMAO non s�lectif'),
	 ('AO','Antibiotique ophtalmique'),
	 ('AP','Antipsychotique normothymique'),
	 ('AUM','Antibiotique urinaire minute'),
	 ('CRT','Cortico�de, antibiotique et antifongique � usage local'),
	 ('HYP','Hypnotique antihistaminique'),
	 ('PSA','Psychostimulant, antiasth�nique');

insert into medicament(id,nom,composition,effets,contreIndication,idFamille) values
	 ('3MYC7','TRIMYCINE','Triamcinolone (ac�tonide) + N�omycine + Nystatine','Ce m�dicament est un cortico�de � activit� forte ou tr�s forte associ� � un antibiotique et un antifongique, utilis� en application locale dans certaines atteintes cutan�es surinfect�es.','Ce m�dicament est contre-indiqu� en cas d''allergie � l''un des constituants, d''infections de la peau ou de parasitisme non trait�s, d''acn�. Ne pas appliquer sur une plaie, ni sous un pansement occlusif.','CRT'),
	 ('ADIMOL9','ADIMOL','Amoxicilline + Acide clavulanique','Ce m�dicament, plus puissant que les p�nicillines simples, est utilis� pour traiter des infections bact�riennes sp�cifiques.','Ce m�dicament est contre-indiqu� en cas d''allergie aux p�nicillines ou aux c�phalosporines.','ABP'),
	 ('AMOPIL7','AMOPIL','Amoxicilline','Ce m�dicament, plus puissant que les p�nicillines simples, est utilis� pour traiter des infections bact�riennes sp�cifiques.','Ce m�dicament est contre-indiqu� en cas d''allergie aux p�nicillines. Il doit �tre administr� avec prudence en cas d''allergie aux c�phalosporines.','ABP'),
	 ('AMOX45','AMOXAR','Amoxicilline','Ce m�dicament, plus puissant que les p�nicillines simples, est utilis� pour traiter des infections bact�riennes sp�cifiques.','La prise de ce m�dicament peut rendre positifs les tests de d�pistage du dopage.','ABP'),
	 ('AMOXIG12','AMOXI G�','Amoxicilline','Ce m�dicament, plus puissant que les p�nicillines simples, est utilis� pour traiter des infections bact�riennes sp�cifiques.','Ce m�dicament est contre-indiqu� en cas d''allergie aux p�nicillines. Il doit �tre administr� avec prudence en cas d''allergie aux c�phalosporines.','ABP'),
	 ('APATOUX22','APATOUX Vitamine C','Tyrothricine + T�traca�ne + Acide ascorbique (Vitamine C)','Ce m�dicament est utilis� pour traiter les affections de la bouche et de la gorge.','Ce m�dicament est contre-indiqu� en cas d''allergie � l''un des constituants, en cas de ph�nylc�tonurie et chez l''enfant de moins de 6 ans.','ALO'),
	 ('BACTIG10','BACTIGEL','Erythromycine','Ce m�dicament est utilis� en application locale pour traiter l''acn� et les infections cutan�es bact�riennes associ�es.','Ce m�dicament est contre-indiqu� en cas d''allergie aux antibiotiques de la famille des macrolides ou des lincosanides.','ABC'),
	 ('BACTIV13','BACTIVIL','Erythromycine','Ce m�dicament est utilis� pour traiter des infections bact�riennes sp�cifiques.','Ce m�dicament est contre-indiqu� en cas d''allergie aux macrolides (dont le chef de file est l''�rythromycine).','AFM'),
	 ('BITALV','BIVALIC','Dextropropoxyph�ne + Parac�tamol','Ce m�dicament est utilis� pour traiter les douleurs d''intensit� mod�r�e ou intense.','Ce m�dicament est contre-indiqu� en cas d''allergie aux m�dicaments de cette famille, d''insuffisance h�patique ou d''insuffisance r�nale.','AAA'),
	 ('CARTION6','CARTION','Acide ac�tylsalicylique (aspirine) + Acide ascorbique (Vitamine C) + Parac�tamol','Ce m�dicament est utilis� dans le traitement symptomatique de la douleur ou de la fi�vre.','Ce m�dicament est contre-indiqu� en cas de troubles de la coagulation (tendances aux h�morragies), d''ulc�re gastroduod�nal, maladies graves du foie.','AAA'),
	 ('CLAZER6','CLAZER','Clarithromycine','Ce m�dicament est utilis� pour traiter des infections bact�riennes sp�cifiques. Il est �galement utilis� dans le traitement de l''ulc�re gastro-duod�nal, en association avec d''autres m�dicaments.','Ce m�dicament est contre-indiqu� en cas d''allergie aux macrolides (dont le chef de file est l''�rythromycine).','AFM'),
	 ('DEPRIL9','DEPRAMIL','Clomipramine','Ce m�dicament est utilis� pour traiter les �pisodes d�pressifs s�v�res, certaines douleurs rebelles, les troubles obsessionnels compulsifs et certaines �nur�sies chez l''enfant.','Ce m�dicament est contre-indiqu� en cas de glaucome ou d''ad�nome de la prostate, d''infarctus r�cent, ou si vous avez re�u un traitement par IMAO durant les 2 semaines pr�c�dentes ou en cas d''allergie aux antid�presseurs imipraminiques.','AIM'),
	 ('DIMIRTAM6','DIMIRTAM','Mirtazapine','Ce m�dicament est utilis� pour traiter les �pisodes d�pressifs s�v�res.','La prise de ce produit est contre-indiqu�e en cas de d''allergie � l''un des constituants.','AAC'),
	 ('DOLRIL7','DOLORIL','Acide ac�tylsalicylique (aspirine) + Acide ascorbique (Vitamine C) + Parac�tamol','Ce m�dicament est utilis� dans le traitement symptomatique de la douleur ou de la fi�vre.','Ce m�dicament est contre-indiqu� en cas d''allergie au parac�tamol ou aux salicylates.','AAA'),
	 ('DORNOM8','NORMADOR','Doxylamine','Ce m�dicament est utilis� pour traiter l''insomnie chez l''adulte.','Ce m�dicament est contre-indiqu� en cas de glaucome, de certains troubles urinaires (r�tention urinaire) et chez l''enfant de moins de 15 ans.','HYP'),
	 ('EQUILARX6','EQUILAR','M�clozine','Ce m�dicament est utilis� pour traiter les vertiges et pour pr�venir le mal des transports.','Ce m�dicament ne doit pas �tre utilis� en cas d''allergie au produit, en cas de glaucome ou de r�tention urinaire.','AAH'),
	 ('EVILR7','EVEILLOR','Adrafinil','Ce m�dicament est utilis� pour traiter les troubles de la vigilance et certains symptomes neurologiques chez le sujet ag�.','Ce m�dicament est contre-indiqu� en cas d''allergie � l''un des constituants.','PSA'),
	 ('INSXT5','INSECTIL','Diph�nydramine','Ce m�dicament est utilis� en application locale sur les piq�res d''insecte et l''urticaire.','Ce m�dicament est contre-indiqu� en cas d''allergie aux antihistaminiques.','AH'),
	 ('JOVAI8','JOVENIL','Josamycine','Ce m�dicament est utilis� pour traiter des infections bact�riennes sp�cifiques.','Ce m�dicament est contre-indiqu� en cas d''allergie aux macrolides (dont le chef de file est l''�rythromycine).','AFM'),
	 ('LIDOXY23','LIDOXYTRACINE','Oxyt�tracycline +Lidoca�ne','Ce m�dicament est utilis� en injection intramusculaire pour traiter certaines infections sp�cifiques.','Ce m�dicament est contre-indiqu� en cas d''allergie � l''un des constituants. Il ne doit pas �tre associ� aux r�tino�des.','AFC'),
	 ('LITHOR12','LITHORINE','Lithium','Ce m�dicament est indiqu� dans la pr�vention des psychoses maniaco-d�pressives ou pour traiter les �tats maniaques.','Ce m�dicament ne doit pas �tre utilis� si vous �tes allergique au lithium. Avant de prendre ce traitement, signalez � votre m�decin traitant si vous souffrez d''insuffisance r�nale, ou si vous avez un r�gime sans sel.','AP'),
	 ('PARMOL16','PARMOCODEINE','Cod�ine + Parac�tamol','Ce m�dicament est utilis� pour le traitement des douleurs lorsque des antalgiques simples ne sont pas assez efficaces.','Ce m�dicament est contre-indiqu� en cas d''allergie � l''un des constituants, chez l''enfant de moins de 15 Kg, en cas d''insuffisance h�patique ou respiratoire, d''asthme, de ph�nylc�tonurie et chez la femme qui allaite.','AA'),
	 ('PHYSOI8','PHYSICOR','Sulbutiamine','Ce m�dicament est utilis� pour traiter les baisses d''activit� physique ou psychique, souvent dans un contexte de d�pression.','Ce m�dicament est contre-indiqu� en cas d''allergie � l''un des constituants.','PSA'),
	 ('PIRIZ8','PIRIZAN','Pyrazinamide','Ce m�dicament est utilis�, en association � d''autres antibiotiques, pour traiter la tuberculose.','Ce m�dicament est contre-indiqu� en cas d''allergie � l''un des constituants, d''insuffisance r�nale ou h�patique, d''hyperuric�mie ou de porphyrie.','ABA'),
	 ('POMDI20','POMADINE','Bacitracine','Ce m�dicament est utilis� pour traiter les infections oculaires de la surface de l''oeil.','Ce m�dicament est contre-indiqu� en cas d''allergie aux antibiotiques appliqu�s localement.','AO'),
	 ('TROXT21','TROXADET','Parox�tine','Ce m�dicament est utilis� pour traiter la d�pression et les troubles obsessionnels compulsifs. Il peut �galement �tre utilis� en pr�vention des crises de panique avec ou sans agoraphobie.','Ce m�dicament est contre-indiqu� en cas d''allergie au produit.','AIN'),
	 ('TXISOL22','TOUXISOL Vitamine C','Tyrothricine + Acide ascorbique (Vitamine C)','Ce m�dicament est utilis� pour traiter les affections de la bouche et de la gorge.','Ce m�dicament est contre-indiqu� en cas d''allergie � l''un des constituants et chez l''enfant de moins de 6 ans.','ALO'),
	 ('URIEG6','URIREGUL','Fosfomycine trom�tamol','Ce m�dicament est utilis� pour traiter les infections urinaires simples chez la femme de moins de 65 ans.','La prise de ce m�dicament est contre-indiqu�e en cas d''allergie � l''un des constituants et d''insuffisance r�nale.','AUM');


insert into typePraticien(id, libelle, lieu) values
	('MH','M�decin Hospitalier','Hopital ou clinique'),
	('MV','M�decine de Ville','Cabinet'),
	('PH','Pharmacien Hospitalier','Hopital ou clinique'),
	('PO','Pharmacien Officine','Pharmacie'),
	('PS','Personnel de sant�','Centre param�dical');

insert into specialite(id,libelle) values
	('ACP','anatomie et cytologie pathologiques'),
	('AMV','ang�iologie, m�decine vasculaire'),
	('ARC','anesth�siologie et r�animation chirurgicale'),
	('BM','biologie m�dicale'),
	('CAC','cardiologie et affections cardio-vasculaires'),
	('CCT','chirurgie cardio-vasculaire et thoracique'),
	('CG','chirurgie g�n�rale'),
	('CMF','chirurgie maxillo-faciale'),
	('COM','canc�rologie, oncologie m�dicale'),
	('COT','chirurgie orthop�dique et traumatologie'),
	('CPR','chirurgie plastique reconstructrice et esth�tique'),
	('CU','chirurgie urologique'),
	('CV','chirurgie vasculaire'),
	('DN','diab�tologie-nutrition, nutrition'),
	('DV','dermatologie et v�n�r�ologie'),
	('EM','endocrinologie et m�tabolismes'),
	('ETD','�valuation et traitement de la douleur'),
	('GEH','gastro-ent�rologie et h�patologie (appareil digestif)'),
	('GMO','gyn�cologie m�dicale, obst�trique'),
	('GO','gyn�cologie-obst�trique'),
	('HEM','maladies du sang (h�matologie)'),
	('MBS','m�decine et biologie du sport'),
	('MDT','m�decine du travail'),
	('MMO','m�decine manuelle - ost�opathie'),
	('MN','m�decine nucl�aire'),
	('MPR','m�decine physique et de r�adaptation'),
	('MTR','m�decine tropicale, pathologie infectieuse et tropicale'),
	('NEP','n�phrologie'),
	('NRC','neurochirurgie'),
	('NRL','neurologie'),
	('ODM','orthop�die dento maxillo-faciale'),
	('OPH','ophtalmologie'),
	('ORL','oto-rhino-laryngologie'),
	('PEA','psychiatrie de l''enfant et de l''adolescent'),
	('PME','p�diatrie maladies des enfants'),
	('PNM','pneumologie'),
	('PSC','psychiatrie'),
	('RAD','radiologie (radiodiagnostic et imagerie m�dicale)'),
	('RDT','radioth�rapie (oncologie option radioth�rapie)'),
	('RGM','reproduction et gyn�cologie m�dicale'),
	('RHU','rhumatologie'),
	('STO','stomatologie'),
	('SXL','sexologie'),
	('TXA','toxicomanie et alcoologie');

SET IDENTITY_INSERT  praticien ON 
insert into praticien(id, nom, prenom, rue, codePostal, ville, idType, idSpecialite) values 
	 (1,'Notini','Alain','114 r Authie','80000','AMIENS','MH','CPR'),
	 (2,'Gosselin','Albert','13 r Devon','80100','ABBEVILLE','MV',NULL),
	 (3,'Delahaye','Andr�','36 av 6 Juin','80420','FLIXECOURT','PS',NULL),
	 (4,'Leroux','Andr�','47 av Robert Schuman','80600','DOULLENS','PH',NULL),
	 (5,'Desmoulins','Anne','31 r St Jean','80300','ALBERT','PO',NULL),
	 (6,'Mouel','Anne','27 r Auvergne','80000','AMIENS','MH',NULL),
	 (7,'Desgranges-Lentz','Antoine','1 r Albert de Mun','80200','PERONNE','MV','MBS'),
	 (8,'Marcouiller','Arnaud','31 r St Jean','80000','AMIENS','PS',NULL),
	 (9,'Dupuy','Benoit','9 r Demolombe','80100','ABBEVILLE','PH',NULL),
	 (10,'Lerat','Bernard','31 r St Jean','59000','LILLE','PO',NULL),
	 (11,'Mar�ais-Lefebvre','Bertrand','86Bis r Basse','67000','STRASBOURG','MH',NULL),
	 (12,'Boscher','Bruno','94 r Falaise','10000','TROYES','MV','MBS'),
	 (13,'Morel','Catherine','21 r Chateaubriand','75000','PARIS','PS',NULL),
	 (14,'Guivarch','Chantal','4 av G�n Laperrine','45000','ORLEANS','PH',NULL),
	 (15,'Bessin-Grosdoit','Christophe','92 r Falaise','06000','NICE','PO',NULL),
	 (16,'Rossa','Claire','14 av Thi�s','06000','NICE','MH',NULL),
	 (17,'Cauchy','Denis','5 av Ste Th�r�se','11000','NARBONNE','MV',NULL),
	 (18,'Gaff�','Dominique','9 av 1�re Arm�e Fran�aise','35000','RENNES','PS',NULL),
	 (19,'Guenon','Dominique','98 bd Mar Lyautey','44000','NANTES','PH',NULL),
	 (20,'Pr�vot','Dominique','29 r Lucien Nelle','87000','LIMOGES','PO',NULL),
	 (21,'Houchard','Eliane','9 r Demolombe','49100','ANGERS','MH',NULL),
	 (22,'Desmons','Elisabeth','51 r Berni�res','29000','QUIMPER','MV',NULL),
	 (23,'Flament','Elisabeth','11 r Pasteur','35000','RENNES','PS',NULL),
	 (24,'Goussard','Emmanuel','9 r Demolombe','41000','BLOIS','PH',NULL),
	 (25,'Desprez','Eric','9 r Vaucelles','33000','BORDEAUX','PO',NULL),
	 (26,'Coste','Evelyne','29 r Lucien Nelle','19000','TULLE','MH',NULL),
	 (27,'Lefebvre','Fr�d�ric','2 pl Wurzburg','55000','VERDUN','MV',NULL),
	 (28,'Lem�e','Fr�d�ric','29 av 6 Juin','56000','VANNES','PS',NULL),
	 (29,'Martin','Fr�d�ric','B�t A 90 r Bayeux','70000','VESOUL','PH',NULL),
	 (30,'Marie','Fr�d�rique','172 r Caponi�re','70000','VESOUL','PO',NULL),
	 (31,'Rosenstech','Genevi�ve','27 r Auvergne','75000','PARIS','MH',NULL),
	 (32,'Pontavice','Ghislaine','8 r Gaillon','86000','POITIERS','MV',NULL),
	 (33,'Leveneur-Mosquet','Guillaume','47 av Robert Schuman','64000','PAU','PS',NULL),
	 (34,'Blanchais','Guy','30 r Authie','08000','SEDAN','PH',NULL),
	 (35,'Leveneur','Hugues','7 pl St Gilles','62000','ARRAS','PO',NULL),
	 (36,'Mosquet','Isabelle','22 r Jules Verne','76000','ROUEN','MH',NULL),
	 (37,'Giraudon','Jean-Christophe','1 r Albert de Mun','38100','VIENNE','MV',NULL),
	 (38,'Marie','Jean-Claude','26 r H�rouville','69000','LYON','PS',NULL),
	 (39,'Maury','Jean-Fran�ois','5 r Pierre Girard','71000','CHALON SUR SAONE','PH',NULL),
	 (40,'Dennel','Jean-Louis','7 pl St Gilles','28000','CHARTRES','PO',NULL),
	 (41,'Ain','Jean-Pierre','4 r�sid Olympia','02000','LAON','MH',NULL),
	 (42,'Chemery','Jean-Pierre','51 pl Ancienne Boucherie','14000','CAEN','MV',NULL),
	 (43,'Comoz','Jean-Pierre','35 r Auguste Lechesne','18000','BOURGES','PS',NULL),
	 (44,'Desfaudais','Jean-Pierre','7 pl St Gilles','29000','BREST','PH',NULL),
	 (45,'Phan','J�rôme','9 r Clos Caillet','79000','NIORT','PO',NULL),
	 (46,'Riou','Line','43 bd G�n Vanier','77000','MARNE LA VALLEE','MH',NULL),
	 (47,'Chubilleau','Louis','46 r Eglise','17000','SAINTES','MV',NULL),
	 (48,'Lebrun','Lucette','178 r Auge','54000','NANCY','PS',NULL),
	 (49,'Goessens','Marc','6 av 6 Juin','39000','DOLE','PH',NULL),
	 (50,'Laforge','Marc','5 r�sid Prairie','50000','SAINT LO','PO',NULL),
	 (51,'Millereau','Marc','36 av 6 Juin','72000','LA FERTE BERNARD','MH',NULL),
	 (52,'Dauverne','Marie-Christine','69 av Charlemagne','21000','DIJON','MV',NULL),
	 (53,'Vittorio','Myriam','3 pl Champlain','94000','BOISSY SAINT LEGER','PS',NULL),
	 (54,'Lapasset','Nhieu','31 av 6 Juin','52000','CHAUMONT','PH',NULL),
	 (55,'Plantet-Besnier','Nicole','10 av 1�re Arm�e Fran�aise','86000','CHATELLEREAULT','PO',NULL),
	 (56,'Chubilleau','Pascal','3 r Hastings','15000','AURRILLAC','MH',NULL),
	 (57,'Robert','Pascal','31 r St Jean','93000','BOBIGNY','MV',NULL),
	 (58,'Jean','Pascale','114 r Authie','49100','SAUMUR','PS',NULL),
	 (59,'Chanteloube','Patrice','14 av Thi�s','13000','MARSEILLE','PH',NULL),
	 (60,'Lecuirot','Patrice','r�sid St P�res 55 r Pigaci�re','54000','NANCY','PO',NULL),
	 (61,'Gandon','Patrick','47 av Robert Schuman','37000','TOURS','MH',NULL),
	 (62,'Mirouf','Patrick','22 r Puits Picard','74000','ANNECY','MV',NULL),
	 (63,'Boireaux','Philippe','14 av Thi�s','10000','CHALON EN CHAMPAGNE','PS',NULL),
	 (64,'Cendrier','Philippe','7 pl St Gilles','12000','RODEZ','PH',NULL),
	 (65,'Duhamel','Philippe','114 r Authie','34000','MONTPELLIER','PO',NULL),
	 (66,'Grigy','Philippe','15 r M�lingue','44000','CLISSON','MH',NULL),
	 (67,'Linard','Philippe','1 r Albert de Mun','81000','ALBI','MV',NULL),
	 (68,'Lozier','Philippe','8 r Gaillon','31000','TOULOUSE','PS',NULL),
	 (69,'Dech�tre','Pierre','63 av Thi�s','23000','MONTLUCON','PH',NULL),
	 (70,'Goessens','Pierre','22 r Jean Romain','40000','MONT DE MARSAN','PO',NULL),
	 (71,'Lem�nager','Pierre','39 av 6 Juin','57000','METZ','MH',NULL),
	 (72,'N�e','Pierre','39 av 6 Juin','82000','MONTAUBAN','MV',NULL),
	 (73,'Guyot','Pierre-Laurent','43 bd G�n Vanier','48000','MENDE','PS',NULL),
	 (74,'Chauchard','Roger','9 r Vaucelles','13000','MARSEILLE','PH',NULL),
	 (75,'Mabire','Roland','11 r Boutiques','67000','STRASBOURG','PO',NULL),
	 (76,'Leroy','Soazig','45 r Boutiques','61000','ALENCON','MH',NULL),
	 (77,'Guyot','St�phane','26 r H�rouville','46000','FIGEAC','MV',NULL),
	 (78,'Delposen','Sylvain','39 av 6 Juin','27000','DREUX','PS',NULL),
	 (79,'Rault','Sylvie','15 bd Richemond','02000','SOISSON','PH',NULL),
	 (80,'Renouf','Sylvie','98 bd Mar Lyautey','88000','EPINAL','PO',NULL),
	 (81,'Alliet-Grach','Thierry','14 av Thi�s','07000','PRIVAS','MH',NULL),
	 (82,'Bayard','Thierry','92 r Falaise','42000','SAINT ETIENNE','MV',NULL),
	 (83,'Gauchet','Thierry','7 r Desmoueux','38100','GRENOBLE','PS',NULL),
	 (84,'Bobichon','Tristan','219 r Caponi�re','09000','FOIX','PH',NULL),
	 (85,'Duchemin-Laniel','V�ronique','130 r St Jean','33000','LIBOURNE','PO',NULL),
	 (86,'Laurent','Youn�s','34 r Demolombe','53000','MAYENNE','MH',NULL);


insert into praticien(id, nom, prenom, rue, codePostal, ville, telephone, idType) values 
	 (101,'Guyon', 'Pascal', '3 Place des provinces francaises', '80000', 'AMIENS',  '03 22 43 11 50', 'PO'),
	 (102,'Pavaut', 'Lucien', '2 Avenue Jules Ferry', '80470', 'Dreuil-l�s-Amiens', '03 22 54 11 08', 'PO'),
	 (103,'Fauquet', 'Stp�phane', 'Place du Pays d''Auge', '80000', 'Amiens', '03 22 43 11 37', 'PO'),
	 (104,'Legris', 'Pierre', '410 Rue d''Abbeville', '80000', 'Amiens', '03 22 43 14 54', 'PO'),
	 (105,'Demoulin', 'Nenry', '16 Rue d''Abbeville', '80000', 'Amiens', '03 22 43 01 30', 'PO'),
	 (106,'Desmarest', 'Lucie', '6 rue Edouard Lucas', '80000', 'Amiens', '03 22 69 64 50', 'PO'),
	 (107,'Fachon', 'Elodie', '11/13 Rue Jean Catelas', '80000', 'Amiens', '03 22 71 64 15', 'PO'),
	 (108, 'Lafayenne', 'Antoine', '4 Place Gambetta', '80000', 'Amiens', '03 22 97 52 52', 'PO'),
	 (109,'Dumas', 'Alexandre', '45 Rue de Rouen', '80000', 'Amiens', '03 22 95 41 52', 'PO'),
	 (110,'Legrand', 'Pierre', 'Rue Botticelli', '80000', 'Amiens', '03 22 69 69 00', 'PO'),
	 (111,'Paro�elle', 'Lyse', '64 Chauss�e du Bois', '80100', 'Abbeville', '03 22 20 69 69', 'PO'),
	 (112,'Ballesdent', 'Jo�l', '9 Rue Jean de Ponthieu', '80100', 'Abbeville', '03 22 24 00 83', 'PO'),
	 (113,'Lambert', 'Jean', '59 Chauss�e Marcad�', '80100', 'Abbeville', '03 22 24 06 51', 'PO'),
	 (114,'Quenot', 'Alain', '68 Rue du Bourg', '80600', 'Doullens', '03 22 36 75 44', 'PO'),
	 (115,'Carpentier', 'Thierry', '15 Porte de Doullens', '80600', 'Beauquesne', '03 22 32 85 84', 'PO'),
	 (116,'Le Nancq', 'Val�rie', '22Bis Rue Armand Devillers', '80630', 'Beauval', '03 22 32 91 49', 'PO'),
	 (117,'Le Borne', 'Paul', '7 Rue du Commandement Unique', '80600', 'Doullens', '03 22 77 02 79', 'PO');


insert into praticien(id, nom, prenom, rue, codePostal, ville, telephone, idType) values 
	 (121,'Guyon', 'Pascal', '3 Place des provinces francaises', '2A100', 'Bastia',  '04 22 43 11 50', 'PO'),
	 (122,'Pavaut', 'Lucien', '2 Avenue Jules Ferry', '2A100', 'Bastia', '04 22 54 11 08', 'PO'),
	 (123,'Fauquet', 'Stp�phane', 'Place du Pays d''Auge', '2A100', 'Bastia', '04 22 43 11 37', 'PO');

SET IDENTITY_INSERT  praticien OFF 

-- g�n�ration des attributs non renseign�s

-- g�n�ration automatique des num�ro de t�l�phone

update praticien 
   set telephone = concat('06 ', id % 9, id * 2 % 9, ' ' , id % 7 , id % 9 ,' ', id * 24  % 9, id * 33 % 9, ' ',  id * 8  % 9, id * 29 % 9)
   where telephone is null;

-- g�n�ration automatique aresse mail nom.prenom@laposte.net

update praticien 
   set email = concat(lower(nom), '.', lower(prenom), '@laposte.net');



insert into visiteur(id, nom, prenom, rue, codePostal, ville, dateEmbauche) values
	 ('a17','Andre','David','1 r Aimon de Chiss�e','80000','AMIENS','26/08/1991'),
	 ('a18','Ansart', 'Alexis', '4 r des buissons', '2A100', 'BASTIA', '20/09/1985'),
	 ('a55','Bedos','Christian','1 r B�n�dictins','65000','TARBES','17/07/1987'),
	 ('a93','Tusseau','Louis','22 r Renou','86000','POITIERS','02/01/1999'),
	 ('b13','Bentot','Pascal','11 av 6 Juin','67000','STRASBOURG','11/03/1996'),
	 ('b16','Bioret','Luc','1 r Linne','35000','RENNES','21/03/1997'),
	 ('b19','Bunisset','Francis','10 r Nicolas Chorier','85000','LA ROCHE SUR YON','31/01/1999'),
	 ('b25','Bunisset','Denise','1 r Lionne','49100','ANGERS','03/07/1994'),
	 ('b28','Cacheux','Bernard','114 r Authie','34000','MONTPELLIER','02/08/2000'),
	 ('b34','Cadic','Eric','123 r Caponi�re','41000','BLOIS','06/12/1993'),
	 ('b4','Charoze','Catherine','100 pl G�ants','33000','BORDEAUX','25/09/1997'),
	 ('b50','Clepkens','Christophe','12 r F�d�rico Garcia Lorca','13000','MARSEILLE','18/01/1998'),
	 ('b59','Cottin','Vincenne','36 sq Capucins','05000','GAP','21/10/1995'),
	 ('c14','Daburon','Fran�ois','13 r Champs Elys�es','06000','NICE','01/02/1989'),
	 ('c3','Debeauvais','Philippe','13 r Charles Peguy','10000','TROYES','05/05/1992'),
	 ('c54','Debelle','Michel','181 r Caponi�re','88000','EPINAL','09/04/1991'),
	 ('d13','Debelle','Jeanne','134 r Stalingrad','44000','NANTES','05/12/1991'),
	 ('d51','Debroise','Michel','2 av 6 Juin','70000','VESOUL','18/11/1997'),
	 ('e22','Desmarquest','Nathalie','14 r F�d�rico Garcia Lorca','54000','NANCY','24/03/1989'),
	 ('e24','Desnost','Pierre','16 r Barral de Montferrat','55000','VERDUN','17/05/1993'),
	 ('e39','Dudouit','Fr�d�ric','18 quai Xavier Jouvin','75000','PARIS','26/04/1988'),
	 ('e49','Duncombe','Claude','19 av Alsace Lorraine','09000','FOIX','19/02/1996'),
	 ('e5','Enault-Pascreau','C�line','25B r Stalingrad','40000','MONT DE MARSAN','27/11/1990'),
	 ('e52','Eynde','Val�rie','3 r Henri Moissan','76000','ROUEN','31/10/1991'),
	 ('f21','Finck','Jacques','rte Montreuil Bellay','74000','ANNECY','08/06/1993'),
	 ('f39','Fr�mont','Fernande','4 r Jean Giono','69000','LYON','15/02/1997'),
	 ('f4','Gest','Alain','30 r Authie','46000','FIGEAC','03/05/1994'),
	 ('g53','Gombert','Luc','32 r Emile Gueymard','56000','VANNES','02/10/1985'),
	 ('g7','Guindon','Caroline','40 r Mar Montgomery','87000','LIMOGES','13/01/1996'),
	 ('h13','Guindon','Fran�ois','44 r Picoti�re','19000','TULLE','08/05/1993'),
	 ('h30','Igigabel','Guy','33 gal Arlequin','94000','CRETEIL','26/04/1998'),
	 ('h35','Jourdren','Pierre','34 av Jean Perrot','15000','AURRILLAC','26/08/1993'),
	 ('h40','Juttard','Pierre-Raoul','34 cours Jean Jaur�s','08000','SEDAN','01/11/1992'),
	 ('j45','Labour�-Morel','Saout','38 cours Berriat','52000','CHAUMONT','25/02/1998'),
	 ('j50','Landr�','Philippe','4 av G�n Laperrine','59000','LILLE','16/12/1992'),
	 ('j8','Langeard','Hugues','39 av Jean Perrot','93000','BAGNOLET','18/06/1998'),
	 ('k4','Lanne','Bernard','4 r Bayeux','30000','NIMES','21/11/1996'),
	 ('k53','Le','No�l','4 av Beauvert','68000','MULHOUSE','23/03/1983'),
	 ('l14','Le','Jean','39 r Raspail','53000','LAVAL','02/02/1995'),
	 ('l23','Leclercq','Servane','11 r Quinconce','18000','BOURGES','05/06/1995'),
	 ('l46','Lecornu','Jean-Bernard','4 bd Mar Foch','72000','LA FERTE BERNARD','24/01/1997'),
	 ('l56','Lecornu','Ludovic','4 r Abel Servien','25000','BESANCON','27/02/1996'),
	 ('m35','Lejard','Agn�s','4 r Anthoard','82000','MONTAUBAN','06/10/1987'),
	 ('m45','Lesaulnier','Pascal','47 r Thiers','57000','METZ','13/10/1990'),
	 ('n42','Letessier','St�phane','5 chem Capuche','27000','EVREUX','06/03/1996'),
	 ('n58','Loirat','Didier','Les P�chers cit� Bourg la Croix','45000','ORLEANS','30/08/1992'),
	 ('n59','Maffezzoli','Thibaud','5 r Chateaubriand','02000','LAON','19/12/1994'),
	 ('o26','Mancini','Anne','5 r D''Agier','48000','MENDE','05/01/1995'),
	 ('p32','Marcouiller','G�rard','7 pl St Gilles','91000','ISSY LES MOULINEAUX','24/12/1992'),
	 ('p40','Michel','Jean-Claude','5 r Gabriel P�ri','61000','FLERS','14/12/1992'),
	 ('p41','Montecot','Fran�oise','6 r Paul Val�ry','17000','SAINTES','27/07/1998'),
	 ('p42','Notini','Veronique','5 r Lieut Chabal','60000','BEAUVAIS','12/12/1994'),
	 ('p49','Onfroy','Den','5 r Sidonie Jacolin','37000','TOURS','03/10/1977'),
	 ('p6','Pascreau','Charles','57 bd Mar Foch','64000','PAU','30/03/1997'),
	 ('p7','Pernot','Claude-No�l','6 r Alexandre 1 de Yougoslavie','11000','NARBONNE','01/03/1990'),
	 ('p8','Perrier','Ma�tre','6 r Aubert Dubayet','71000','CHALON SUR SAONE','23/06/1991'),
	 ('q17','Petit','Jean-Louis','7 r Ernest Renan','50000','SAINT LO','06/09/1997'),
	 ('r24','Piquery','Patrick','9 r Vaucelles','14000','CAEN','29/07/1984'),
	 ('r58','Quiquandon','Jo�l','7 r Ernest Renan','29000','QUIMPER','30/06/1990'),
	 ('s10','Retailleau','Josselin','88Bis r Saumuroise','39000','DOLE','14/11/1995'),
	 ('s21','Retailleau','Pascal','32 bd Ayrault','23000','MONTLUCON','25/09/1992'),
	 ('t43','Souron','Maryse','7B r Gay Lussac','21000','DIJON','09/03/1995'),
	 ('t47','Tiphagne','Patrick','7B r Gay Lussac','62000','ARRAS','29/08/1996'),
	 ('t55','Tr�het','Alain','7D chem Barral','12000','RODEZ','29/11/1994'),
	 ('t60','Tusseau','Josselin','63 r Bon Repos','28000','CHARTRES','29/03/1991');

SET IDENTITY_INSERT  motif ON 
insert into motif(id, libelle) values
	(1,'Visite p�riodique'),
	(2,'Actualisation des produits'),
	(3,'Baisse activit�'),
	(4,'Sollication praticien'),
	(5,'Autre');

SET IDENTITY_INSERT motif OFF;

SET IDENTITY_INSERT  visite ON 
insert into visite (id, date, heure, idMotif, idVisiteur, idPraticien, premierMedicament, secondMedicament, bilan) values 
  (1, Cast(getDate() - 4 as Date), '15:00', 1, 'a17', 1, 'ADIMOL9', 'JOVAI8', 'Bilan fiche visite n� 1'),
  (2, Cast(getDate() - 4 as Date), '17:00', 1, 'a17', 2, 'LIDOXY23', null, 'Bilan fiche visite n� 2'),
  (3, Cast(getDate() - 3 as Date), '10:30', 2, 'a17', 3, 'AMOX45', 'LIDOXY23', 'Bilan fiche visite n� 3'),
  (4, Cast(getDate() - 3 as Date), '11:30', 1, 'a17', 4, 'DEPRIL9', 'JOVAI8', 'Bilan fiche visite n� 4'),
  (5, Cast(getDate() - 3 as Date), '14:00', 3, 'a17', 5, 'AMOXIG12', 'TXISOL22', 'Bilan fiche visite n� 5'),
  (6, Cast(getDate() - 3 as Date), '17:00', 4, 'a17', 6, 'AMOPIL7', 'PIRIZ8', 'Bilan fiche visite n� 6'),
  (7, Cast(getDate() - 2 as Date), '10:00', 5, 'a17', 7, '3MYC7', null, 'Bilan fiche visite n� 7');

insert into visite (id, date, heure, idMotif, idVisiteur, idPraticien) values 
  (8, Cast(getDate() - 2 as Date), '14:00', 1, 'a17', 8),
  (9, Cast(getDate()  as Date), '15:00', 1, 'a17', 101),
  (10, Cast(getDate() as Date), '17:00', 1, 'a17', 102),
  (11, Cast(getDate() + 3 as Date), '10:30', 2, 'a17', 103),
  (12, Cast(getDate() + 3 as Date), '11:30', 1, 'a17', 104),
  (13, Cast(getDate() + 3 as Date), '14:00', 3, 'a17', 105),
  (14, Cast(getDate() + 3 as Date), '17:00', 4, 'a17', 106),
  (15, Cast(getDate() + 4 as Date), '10:00', 5, 'a17', 107),
  (16, Cast(getDate() + 4 as Date), '14:00', 1, 'a17', 108);


insert into visite (id, date, heure, idMotif, idVisiteur, idPraticien, premierMedicament, secondMedicament, bilan) values 
  (17, Cast(getDate() - 4 as Date), '15:00', 1, 'a18', 121, 'ADIMOL9', 'JOVAI8', 'Bilan fiche visite n� 17'),
  (18, Cast(getDate() - 4 as Date), '17:00', 1, 'a18', 122, 'LIDOXY23', null, 'Bilan fiche visite n� 18'),
  (19, Cast(getDate() - 3 as Date), '10:30', 2, 'a18', 123, 'AMOX45', 'LIDOXY23', 'Bilan fiche visite n� 19');
  
SET IDENTITY_INSERT  visite OFF 


