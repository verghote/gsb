use master;

-- suppression de la base existante
-- aucune autre fen�tre ne doit �tre connet�e � la base 
-- la base de donn�es ne doit �tre en cours d'utilisation : dans un autre onglet ou dans un programme en cours d'ex�cution
-- La suppression n�cessite de relancer toutes les scripts fournis : declencheur, insert, procedure ...  

if DB_ID(N'gsb') is not Null    
	drop database gsb;
go

create database gsb;
go

use gsb;

create table famille
(
	id varchar(3) not null,
	libelle varchar(80),
	constraint PK_famille PRIMARY KEY (id)
);

create table medicament
(
	id varchar(10) not null,
	nom varchar(25),
	composition varchar(255),
	effets varchar(255),
	contreIndication varchar(255),
	idFamille varchar(3) not null,
	constraint pk_medicament PRIMARY KEY (id),
	constraint fk_medicament_famille foreign key (idFamille) references famille(id)
);


create table specialite (
	id varchar(5) not null,
	libelle varchar(150),
	constraint PK_specialite PRIMARY KEY (id)
);

create table typePraticien (
	id varchar(3) not null,
	libelle varchar(25),
	lieu varchar(35),
	constraint PK_typePraticien PRIMARY KEY (id)
);

create table praticien (
	id smallint not null identity,
	nom varchar(25),
	prenom varchar(30),
	rue varchar(50),
	codePostal varchar(5),
	ville varchar(25),
	telephone varchar(14) null,
	email varchar(75) null,
	idType varchar(3) not null,
	idSpecialite varchar(5),
	constraint pk_praticien PRIMARY KEY (id),
	constraint fk_praticien_typePraticien foreign key (idType) references typePraticien (id),
	constraint fk_praticien_specialite foreign key (idSpecialite) references specialite (id),
);

create table visiteur (
	id varchar(3) not null,
	nom varchar(25),
	prenom varchar(50),
	rue varchar(50),
	codePostal varchar(5),
	ville varchar(45),
	dateEmbauche Date,
	constraint PK_visiteur PRIMARY KEY (id)
);


create table motif
(
	id int identity,
	libelle varchar(60),
	constraint PK_motif PRIMARY KEY (id)
);


create table visite
(
	id int identity,
	date date not null,
	heure varchar(15) not null,
	bilan ntext null,
	idMotif int not null,
	idVisiteur varchar(3) not null,
	idPraticien smallint not null,
	premierMedicament varchar(10) null,
	secondMedicament varchar(10) null,
	constraint pk_visite PRIMARY KEY (id),
	constraint fk_visite_motif foreign key (idMotif) references motif(id),
	constraint fk_visite_visiteur foreign key (idVisiteur) references visiteur(id),
	constraint fk_visite_praticien foreign key (idPraticien) references praticien(id),
	constraint fk_visite_medicament1 foreign key (premierMedicament) references medicament(id),
	constraint fk_visite_medicament2 foreign key (secondMedicament) references medicament(id),

);