use gsb;

delete from visite;


SET IDENTITY_INSERT  visite ON 
insert into visite (id, date, heure, idMotif, idVisiteur, idPraticien, premierMedicament, secondMedicament, bilan) values 
  (1, Cast(getDate() - 4 as Date), '15:00', 1, 'a17', 1, 'ADIMOL9', 'JOVAI8', 'Bilan fiche visite n� 1'),
  (2, Cast(getDate() - 4 as Date), '17:00', 1, 'a17', 2, 'LIDOXY23', null, 'Bilan fiche visite n� 2'),
  (3, Cast(getDate() - 3 as Date), '10:30', 2, 'a17', 3, 'AMOX45', 'LIDOXY23', 'Bilan fiche visite n� 3'),
  (4, Cast(getDate() - 3 as Date), '11:30', 1, 'a17', 4, 'DEPRIL9', 'JOVAI8', 'Bilan fiche visite n� 4'),
  (5, Cast(getDate() - 3 as Date), '14:00', 3, 'a17', 5, 'AMOXIG12', 'TXISOL22', 'Bilan fiche visite n� 5'),
  (6, Cast(getDate() - 3 as Date), '17:00', 4, 'a17', 6, 'AMOPIL7', 'PIRIZ8', 'Bilan fiche visite n� 6'),
  (7, Cast(getDate() - 2 as Date), '10:00', 5, 'a17', 7, '3MYC7', null, 'Bilan fiche visite n� 7');

insert into visite (id, date, heure, idMotif, idVisiteur, idPraticien) values 
  (8, Cast(getDate() - 2 as Date), '14:00', 1, 'a17', 8),
  (9, Cast(getDate()  as Date), '15:00', 1, 'a17', 101),
  (10, Cast(getDate() as Date), '17:00', 1, 'a17', 102),
  (11, Cast(getDate() + 3 as Date), '10:30', 2, 'a17', 103),
  (12, Cast(getDate() + 3 as Date), '11:30', 1, 'a17', 104),
  (13, Cast(getDate() + 3 as Date), '14:00', 3, 'a17', 105),
  (14, Cast(getDate() + 3 as Date), '17:00', 4, 'a17', 106),
  (15, Cast(getDate() + 4 as Date), '10:00', 5, 'a17', 107),
  (16, Cast(getDate() + 4 as Date), '14:00', 1, 'a17', 108);


insert into visite (id, date, heure, idMotif, idVisiteur, idPraticien, premierMedicament, secondMedicament, bilan) values 
  (17, Cast(getDate() - 4 as Date), '15:00', 1, 'a18', 121, 'ADIMOL9', 'JOVAI8', 'Bilan fiche visite n� 17'),
  (18, Cast(getDate() - 4 as Date), '17:00', 1, 'a18', 122, 'LIDOXY23', null, 'Bilan fiche visite n� 18'),
  (19, Cast(getDate() - 3 as Date), '10:30', 2, 'a18', 123, 'AMOX45', 'LIDOXY23', 'Bilan fiche visite n� 19');
  
SET IDENTITY_INSERT  visite OFF 

select * from visite
