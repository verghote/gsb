use gsb;

if object_id('ville','U') is not null 
	drop table ville ;
	
if object_id('departement','U') is not null 
	drop table departement;

if object_id('region','U') is not null 
	drop table region ;

 Create Table region (
	id varchar(2) NOT NULL,
	nom varchar(50) NULL,
	Constraint pk_region Primary Key (id)
 );

create table departement (
	id varchar(2),
	nom varchar(50),
	prefecture varchar(50),
	idRegion varchar(2),
	constraint pk_departement PRIMARY KEY (id),
	constraint fk_region foreign key (idRegion) references region(id)
);

create table ville (
	idDepartement varchar(2),
	idCommune int,
	nom varchar(75),
	codePostal varchar(5),
	constraint pk_ville PRIMARY KEY (idDepartement,idCommune),
	constraint fk_ville_departement foreign key (idDepartement) references departement(id)
);
