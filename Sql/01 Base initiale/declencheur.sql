use GSB;

-- contr�le que le praticien visit� se trouve bien dans le d�partement du visiteur

if OBJECT_ID('controleVisite','Tr') is not null
	drop trigger controleVisite;
go


create trigger controleVisite on visite after insert as
	if exists(
		select 1
			from inserted 
			join praticien on praticien.id = inserted.idPraticien
			join visiteur on visiteur.id = inserted.idVisiteur
			where substring(praticien.codePostal,1,2) != substring(visiteur.codePostal,1,2)) 
	
	begin
		raiserror('Le praticien n''est pas de la m�me d�partement que le visiteur',16,1)
		rollback
	end


go

-- contr�le de l'unicit� du visiteur dans le d�partement

if OBJECT_ID('controleVisiteur','Tr') is not null
	drop trigger controleVisiteur;
go

create trigger controleVisiteur on visiteur after insert as
	if exists( select 1 
	           from inserted 
  		       where substring(inserted.codePostal,1,2) in ( select substring(visiteur.codePostal,1,2) from visiteur where inserted.id != visiteur.id))
	begin
		raiserror('Un visiteur existe d�ja dans un d�partement habit� par un des visiteurs ajout�s',16,1)
		rollback
	end			    