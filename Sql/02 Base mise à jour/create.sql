use gsb;

if object_id('medicamentDistribue','U') is not null 
	drop table medicamentDistribue;

if object_id('distribution','U') is not null 
 begin
	delete from distribution;
	drop table distribution;
 end

-- m�morisation des m�dicaments distribu�s et des quantit�s associ�s


Create table medicamentDistribue (
	idVisite int not null,
	idMedicament varchar(10) not null,
	quantite int not null,
	constraint pk_medicamentDistribue Primary Key (idVisite, idMedicament),
	constraint fk_medicamentDistribue_visite foreign key (idVisite) references visite (id),
	constraint fk_medicamentDistribue_medicament foreign key (idMedicament) references medicament (id)
);


-- on ajoute une table qui m�morise le totaldistribue pour chaque visiteur et chaque m�dicament
-- de cette fa�on le visiteur peut connaitre les quantit�s totales qu'il a distribu�es pour chaque m�dicament

-- cr�ation de la table 

Create table distribution (
  idVisiteur varchar(3) not null,
  idMedicament varchar(10) not null,
  totalDistribue int not null constraint d_distribution_totaldistribue default 0,
  constraint pk_distribution Primary Key (idVisiteur, idMedicament),
  constraint fk_distribution_visiteur foreign key (idVisiteur) references visiteur (id) on delete cascade,
  constraint fk_distribution_medicament foreign key (idMedicament) references medicament (id) on delete cascade
 );


				


 