use gsb

-- Ajout du champ totalDistribue dans la table médicament

alter table Medicament add totalDistribue int not null
	constraint d_Medicament_totalDistribue default 0;