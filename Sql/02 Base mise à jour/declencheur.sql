use GSB;


-- mise en place des trigger de mise � jour du champ totalDistribue de la table m�dicament et de la table distribution




 if OBJECT_ID('augmenterTotalDistribue','Tr') is not null
	drop trigger augmenterTotalDistribue;
go

create trigger augmenterTotalDistribue on MedicamentDistribue after insert, update as
 print '+'
 update medicament
  set totalDistribue = totalDistribue + (select sum(quantite)
										 from inserted
										 where inserted.idMedicament = medicament.id)
  from inserted
  where inserted.idMedicament = medicament.id	

  update distribution
     set totalDistribue =  totalDistribue + (select sum(quantite)
											from inserted , visite
											where distribution.idMedicament = inserted.idMedicament
											and distribution.idVisiteur = visite.idVisiteur  
											and visite.id = inserted.idVisite)
 
  from inserted , visite
  where distribution.idMedicament = inserted.idMedicament
  and distribution.idVisiteur = visite.idVisiteur  
  and visite.id = inserted.idVisite	

go

if OBJECT_ID('diminuerTotalDistribue','Tr') is not null
	drop trigger diminuerTotalDistribue;
go

create trigger diminuerTotalDistribue on MedicamentDistribue after delete, update as
 print 'a'
 update medicament
  set totalDistribue = totalDistribue - (select sum(quantite)
										 from deleted
										 where deleted.idMedicament = medicament.id)
from deleted
where deleted.idMedicament = medicament.id	

update distribution
     set totalDistribue =  totalDistribue - (select sum(quantite)
											 from deleted , visite
											 where distribution.idMedicament = deleted.idMedicament
											 and distribution.idVisiteur = visite.idVisiteur  
											 and visite.id = deleted.idVisite)
 
  from deleted , visite
  where distribution.idMedicament = deleted.idMedicament
  and distribution.idVisiteur = visite.idVisiteur  
  and visite.id = deleted.idVisite	
go

-- Ajout des lignes d'inventaire � la cr�ation d'un m�dicament

if OBJECT_ID('ajoutMedicament','Tr') is not null
	drop trigger ajoutMedicament;
go

create trigger ajoutMedicament on medicament after insert as
	insert into distribution(idVisiteur, idMedicament)
		select visiteur.id, inserted.id
		From visiteur, inserted;

go


