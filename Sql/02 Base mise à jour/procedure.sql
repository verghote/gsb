use gsb;

-- nouvelle procédure pour gérer les médicaments distribués

if object_id('getLesMedicamentsDistribues','P') is not null 
	drop procedure getLesMedicamentsDistribues;
go

create procedure getLesMedicamentsDistribues(@idVisiteur varchar(3)) as
	SELECT idVisite, idMedicament, quantite 
	from MedicamentDistribue 
	  Join visite on MedicamentDistribue.idVisite = Visite.id
	where Visite.idVisiteur = @idVisiteur
go

if object_id('enregistrerMedicamentsDistribues','P') is not null 
	drop procedure enregistrerMedicamentsDistribues;
go

create procedure enregistrerMedicamentsDistribues(@idVisite int,  @idMedicament varchar(10), @quantite int) as 
	insert into MedicamentDistribue(idVisite, idMedicament, quantite) values 
		(@idVisite, @idMEdicament, @quantite)
go

if object_id('supprimerMedicamentsDistribues','P') is not null 
	drop procedure supprimerMedicamentsDistribues;
go

create procedure supprimerMedicamentsDistribues(@idVisite int) as 
	delete from MedicamentDistribue
	where idVisite = @idVisite
go



-- procédure pour récupérer les villes appartemant au département du visiteur

if object_id('getLesVilles','P') is not null 
	drop procedure getLesVilles;
go

create procedure getLesVilles(@idVisiteur varchar(3)) as
	SELECT nom, codePostal  
	from Ville 
	where substring(codePostal, 1, 2) = (SELECT substring(codePostal, 1, 2) from visiteur WHERE visiteur.id = @idVisiteur)
	order by nom; 
go


-- execute getLesVilles 'a17';


-- procédure pour enregistrer un nouveau praticien

if object_id('ajouterPraticien','P') is not null 
	drop procedure ajouterPraticien;
go

create procedure ajouterPraticien(@nom varchar(25),  @prenom varchar(30), @rue varchar(50), @codePostal varchar(5), @ville varchar(25), @telephone varchar(14), @email varchar(75), @idType varchar(3), @idSpecialite varchar(5), @idPraticien int output) as 
  select @idPraticien = (select max(id) + 1 from praticien);
  insert into praticien(id, nom, prenom, rue, codePostal, ville, telephone, email, idType, idSpecialite) values 
		(@idPraticien, @nom,  @prenom, @rue, @codePostal, @ville, @telephone, @email, @idType, @idSpecialite);

go


if object_id('ajouterPraticien2','P') is not null 
	drop procedure ajouterPraticien2;
go

create procedure ajouterPraticien2(@nom varchar(25),  @prenom varchar(30), @rue varchar(50), @codePostal varchar(5), @ville varchar(25), @telephone varchar(14), @email varchar(75), @idType varchar(3), @idPraticien int output) as 
  select @idPraticien = ( select max(id) + 1 from praticien);  insert into praticien(id, nom, prenom, rue, codePostal, ville, telephone, email, idType) values 
		(@idPraticien, @nom,  @prenom, @rue, @codePostal, @ville, @telephone, @email, @idType);
  select @idPraticien = (select @@IDENTITY);

go


-- procédure pour modifier les coordonnées d'un praticien


if object_id('modifierPraticien','P') is not null 
	drop procedure modifierPraticien;
go

create procedure modifierPraticien(@id int, @nom varchar(25),  @prenom varchar(30), @rue varchar(50), @codePostal varchar(5), @ville varchar(25), @telephone varchar(14), @email varchar(75), @idType varchar(3), @idSpecialite varchar(5)) as 
  update praticien
    set nom = @nom, prenom = @prenom, rue = @rue, codePostal = @codePostal, ville = @ville, telephone = @telephone, email = @email, idType = @idType , idSpecialite = @idSpecialite 
	where id = @id
go


if object_id('modifierPraticien2','P') is not null 
	drop procedure modifierPraticien2;
go

create procedure modifierPraticien2(@id int, @nom varchar(25),  @prenom varchar(30), @rue varchar(50), @codePostal varchar(5), @ville varchar(25), @telephone varchar(14), @email varchar(75), @idType varchar(3)) as 
  update praticien
    set nom = @nom, prenom = @prenom, rue = @rue, codePostal = @codePostal, ville = @ville, telephone = @telephone, email = @email, idType = @idType , idSpecialite = null 
	where id = @id
go


if object_id('getTotalMedicamentsDistribues','P') is not null 
	drop procedure getTotalMedicamentsDistribues;
go

create procedure getTotalMedicamentsDistribues(@idVisiteur varchar(3)) as
	SELECT nom,  distribution.totalDistribue as total
	from Medicament 
	  Join distribution on Medicament.id = distribution.idMedicament
	where distribution.idVisiteur = @idVisiteur
go




