use Gsb;


-- r�initialisation des visites
delete from medicamentDistribue;
delete from visite;

-- R�mise � 0 des champs calcul�s totalDistribu� par s�curit� (si probl�me avec les d�clencheurs)
update medicament set totalDistribue = 0;
update distribution set  totaldistribue = 0;

-- cr�ation des visites


SET IDENTITY_INSERT  visite ON 
insert into visite (id, date, heure, idMotif, idVisiteur, idPraticien, premierMedicament, secondMedicament, bilan) values 
  (1, Cast(getDate() - 4 as Date), '15:00', 1, 'a17', 1, 'ADIMOL9', 'JOVAI8', 'Bilan fiche visite n� 1'),
  (2, Cast(getDate() - 4 as Date), '17:00', 1, 'a17', 2, 'LIDOXY23', null, 'Bilan fiche visite n� 2'),
  (3, Cast(getDate() - 3 as Date), '10:30', 2, 'a17', 3, 'AMOX45', 'LIDOXY23', 'Bilan fiche visite n� 3'),
  (4, Cast(getDate() - 3 as Date), '11:30', 1, 'a17', 4, 'DEPRIL9', 'JOVAI8', 'Bilan fiche visite n� 4'),
  (5, Cast(getDate() - 3 as Date), '14:00', 3, 'a17', 5, 'AMOXIG12', 'TXISOL22', 'Bilan fiche visite n� 5'),
  (6, Cast(getDate() - 3 as Date), '17:00', 4, 'a17', 6, 'AMOPIL7', 'PIRIZ8', 'Bilan fiche visite n� 6'),
  (7, Cast(getDate() - 2 as Date), '10:00', 5, 'a17', 7, '3MYC7', null, 'Bilan fiche visite n� 7');

insert into visite (id, date, heure, idMotif, idVisiteur, idPraticien) values 
  (8, Cast(getDate() - 2 as Date), '14:00', 1, 'a17', 8),
  (9, Cast(getDate()  as Date), '15:00', 1, 'a17', 101),
  (10, Cast(getDate() as Date), '17:00', 1, 'a17', 102),
  (11, Cast(getDate() + 3 as Date), '10:30', 2, 'a17', 103),
  (12, Cast(getDate() + 3 as Date), '11:30', 1, 'a17', 104),
  (13, Cast(getDate() + 3 as Date), '14:00', 3, 'a17', 105),
  (14, Cast(getDate() + 3 as Date), '17:00', 4, 'a17', 106),
  (15, Cast(getDate() + 4 as Date), '10:00', 5, 'a17', 107),
  (16, Cast(getDate() + 4 as Date), '14:00', 1, 'a17', 108);


insert into visite (id, date, heure, idMotif, idVisiteur, idPraticien, premierMedicament, secondMedicament, bilan) values 
  (17, Cast(getDate() - 4 as Date), '15:00', 1, 'a18', 121, 'ADIMOL9', 'JOVAI8', 'Bilan fiche visite n� 17'),
  (18, Cast(getDate() - 4 as Date), '17:00', 1, 'a18', 122, 'LIDOXY23', null, 'Bilan fiche visite n� 18'),
  (19, Cast(getDate() - 3 as Date), '10:30', 2, 'a18', 123, 'AMOX45', 'LIDOXY23', 'Bilan fiche visite n� 19');
  
SET IDENTITY_INSERT  visite OFF 



-- ajout des m�dicaments distribu�s

print 'debut'

insert into medicamentdistribue(idVisite, idMedicament, quantite) values
	(1, 'ADIMOL9', 5),
	(1, 'JOVAI8' , 2),
	(1, 'LIDOXY23', 3),
	(2, 'DEPRIL9', 1),
	(2, 'JOVAI8', 2),
	(2, 'AMOXIG12', 1),
	(2, '3MYC7', 2),
	(3, 'AMOX45', 4),
	(3, 'AMOXIG12', 2),
	(4, 'PIRIZ8', 1),
	(5, 'TXISOL22',2),
	(5, 'DEPRIL9', 2);


insert into medicamentdistribue(idVisite, idMedicament, quantite) values
	(17, 'ADIMOL9', 5),
	(17, 'JOVAI8' , 2),
	(17, 'LIDOXY23', 3),
	(18, 'DEPRIL9', 1),
	(18, 'JOVAI8', 2),
	(18, 'AMOXIG12', 1),
	(18, '3MYC7', 2),
	(19, 'AMOX45', 4),
	(19, 'AMOXIG12', 2),
	(19, 'PIRIZ8', 1),
	(19, 'TXISOL22',2),
	(19, 'DEPRIL9', 2);


select * from medicament where totalDistribue > 0;
select * from distribution where totalDistribue > 0;

-- r�sultat attendu

-- m�dicament : 4 10 8 6 6 8 6 2 4
-- distribution
--          a17 2 5 4 3 3 4 3 1 2
--          a18 2 5 4 3 3 4 3 1 2        

