﻿namespace GSB
{
    partial class FrmBilan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAjouterEchantillon = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.zoneSaisie = new System.Windows.Forms.Panel();
            this.txtIdVisite = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtHeure = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSpecialite = new System.Windows.Forms.TextBox();
            this.lblSpecialite = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPraticien = new System.Windows.Forms.TextBox();
            this.txtMotif = new System.Windows.Forms.TextBox();
            this.txtVille = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.messageBilan = new System.Windows.Forms.Label();
            this.messagePremierMedicament = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.txtBilan = new System.Windows.Forms.TextBox();
            this.cbxSecondMedicament = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxPremierMedicament = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.zoneSelection = new System.Windows.Forms.Panel();
            this.cbxVisite = new System.Windows.Forms.ComboBox();
            this.message = new System.Windows.Forms.Label();
            this.zoneSaisie.SuspendLayout();
            this.zoneSelection.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitre
            // 
            this.lblTitre.Size = new System.Drawing.Size(1048, 49);
            // 
            // btnAjouterEchantillon
            // 
            this.btnAjouterEchantillon.Location = new System.Drawing.Point(-218, 63);
            this.btnAjouterEchantillon.Margin = new System.Windows.Forms.Padding(2);
            this.btnAjouterEchantillon.Name = "btnAjouterEchantillon";
            this.btnAjouterEchantillon.Size = new System.Drawing.Size(146, 26);
            this.btnAjouterEchantillon.TabIndex = 21;
            this.btnAjouterEchantillon.Text = "Ajouter échantillon";
            this.btnAjouterEchantillon.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-229, 19);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Famille";
            // 
            // zoneSaisie
            // 
            this.zoneSaisie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zoneSaisie.Controls.Add(this.txtIdVisite);
            this.zoneSaisie.Controls.Add(this.label15);
            this.zoneSaisie.Controls.Add(this.txtDate);
            this.zoneSaisie.Controls.Add(this.txtHeure);
            this.zoneSaisie.Controls.Add(this.label16);
            this.zoneSaisie.Controls.Add(this.label17);
            this.zoneSaisie.Controls.Add(this.txtSpecialite);
            this.zoneSaisie.Controls.Add(this.lblSpecialite);
            this.zoneSaisie.Controls.Add(this.txtType);
            this.zoneSaisie.Controls.Add(this.label10);
            this.zoneSaisie.Controls.Add(this.txtPraticien);
            this.zoneSaisie.Controls.Add(this.txtMotif);
            this.zoneSaisie.Controls.Add(this.txtVille);
            this.zoneSaisie.Controls.Add(this.label12);
            this.zoneSaisie.Controls.Add(this.label13);
            this.zoneSaisie.Controls.Add(this.label14);
            this.zoneSaisie.Controls.Add(this.label7);
            this.zoneSaisie.Controls.Add(this.label3);
            this.zoneSaisie.Controls.Add(this.messageBilan);
            this.zoneSaisie.Controls.Add(this.messagePremierMedicament);
            this.zoneSaisie.Controls.Add(this.txtId);
            this.zoneSaisie.Controls.Add(this.label6);
            this.zoneSaisie.Controls.Add(this.btnEnregistrer);
            this.zoneSaisie.Controls.Add(this.txtBilan);
            this.zoneSaisie.Controls.Add(this.cbxSecondMedicament);
            this.zoneSaisie.Controls.Add(this.label2);
            this.zoneSaisie.Controls.Add(this.label5);
            this.zoneSaisie.Controls.Add(this.cbxPremierMedicament);
            this.zoneSaisie.Controls.Add(this.label1);
            this.zoneSaisie.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zoneSaisie.Location = new System.Drawing.Point(0, 73);
            this.zoneSaisie.Margin = new System.Windows.Forms.Padding(2);
            this.zoneSaisie.Name = "zoneSaisie";
            this.zoneSaisie.Size = new System.Drawing.Size(1048, 527);
            this.zoneSaisie.TabIndex = 33;
            // 
            // txtIdVisite
            // 
            this.txtIdVisite.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtIdVisite.Enabled = false;
            this.txtIdVisite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdVisite.Location = new System.Drawing.Point(149, 150);
            this.txtIdVisite.Margin = new System.Windows.Forms.Padding(2);
            this.txtIdVisite.Name = "txtIdVisite";
            this.txtIdVisite.Size = new System.Drawing.Size(93, 26);
            this.txtIdVisite.TabIndex = 99;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(11, 156);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(140, 20);
            this.label15.TabIndex = 98;
            this.label15.Text = "Numéro de la fiche";
            // 
            // txtDate
            // 
            this.txtDate.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtDate.Enabled = false;
            this.txtDate.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(149, 193);
            this.txtDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(270, 26);
            this.txtDate.TabIndex = 97;
            // 
            // txtHeure
            // 
            this.txtHeure.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtHeure.Enabled = false;
            this.txtHeure.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeure.Location = new System.Drawing.Point(149, 239);
            this.txtHeure.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeure.Name = "txtHeure";
            this.txtHeure.Size = new System.Drawing.Size(171, 26);
            this.txtHeure.TabIndex = 96;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(10, 245);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 20);
            this.label16.TabIndex = 95;
            this.label16.Text = "Horaire";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(10, 194);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 20);
            this.label17.TabIndex = 94;
            this.label17.Text = "Date";
            // 
            // txtSpecialite
            // 
            this.txtSpecialite.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtSpecialite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpecialite.Location = new System.Drawing.Point(149, 404);
            this.txtSpecialite.Multiline = true;
            this.txtSpecialite.Name = "txtSpecialite";
            this.txtSpecialite.Size = new System.Drawing.Size(270, 55);
            this.txtSpecialite.TabIndex = 93;
            // 
            // lblSpecialite
            // 
            this.lblSpecialite.AutoSize = true;
            this.lblSpecialite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpecialite.Location = new System.Drawing.Point(11, 406);
            this.lblSpecialite.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSpecialite.Name = "lblSpecialite";
            this.lblSpecialite.Size = new System.Drawing.Size(75, 18);
            this.lblSpecialite.TabIndex = 92;
            this.lblSpecialite.Text = "Spécialité";
            // 
            // txtType
            // 
            this.txtType.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtType.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtType.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtType.Location = new System.Drawing.Point(149, 370);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(270, 26);
            this.txtType.TabIndex = 91;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 370);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 18);
            this.label10.TabIndex = 90;
            this.label10.Text = "Type praticien";
            // 
            // txtPraticien
            // 
            this.txtPraticien.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtPraticien.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPraticien.Location = new System.Drawing.Point(149, 338);
            this.txtPraticien.Name = "txtPraticien";
            this.txtPraticien.Size = new System.Drawing.Size(270, 26);
            this.txtPraticien.TabIndex = 89;
            // 
            // txtMotif
            // 
            this.txtMotif.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtMotif.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotif.Location = new System.Drawing.Point(149, 307);
            this.txtMotif.Name = "txtMotif";
            this.txtMotif.Size = new System.Drawing.Size(270, 26);
            this.txtMotif.TabIndex = 88;
            // 
            // txtVille
            // 
            this.txtVille.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtVille.Enabled = false;
            this.txtVille.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVille.Location = new System.Drawing.Point(149, 274);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(270, 26);
            this.txtVille.TabIndex = 87;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 307);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 18);
            this.label12.TabIndex = 86;
            this.label12.Text = "Motif";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(11, 280);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 20);
            this.label13.TabIndex = 85;
            this.label13.Text = "Lieu";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(11, 338);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 20);
            this.label14.TabIndex = 84;
            this.label14.Text = "Praticien";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 112);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 18);
            this.label7.TabIndex = 83;
            this.label7.Text = "Fiche visite ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(292, 18);
            this.label3.TabIndex = 45;
            this.label3.Text = "Informations à renseigner après la visite";
            // 
            // messageBilan
            // 
            this.messageBilan.AutoSize = true;
            this.messageBilan.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageBilan.ForeColor = System.Drawing.Color.Red;
            this.messageBilan.Location = new System.Drawing.Point(438, 420);
            this.messageBilan.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageBilan.Name = "messageBilan";
            this.messageBilan.Size = new System.Drawing.Size(39, 17);
            this.messageBilan.TabIndex = 44;
            this.messageBilan.Text = "msg";
            this.messageBilan.Visible = false;
            // 
            // messagePremierMedicament
            // 
            this.messagePremierMedicament.AutoSize = true;
            this.messagePremierMedicament.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messagePremierMedicament.ForeColor = System.Drawing.Color.Red;
            this.messagePremierMedicament.Location = new System.Drawing.Point(438, 164);
            this.messagePremierMedicament.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messagePremierMedicament.Name = "messagePremierMedicament";
            this.messagePremierMedicament.Size = new System.Drawing.Size(39, 17);
            this.messagePremierMedicament.TabIndex = 43;
            this.messagePremierMedicament.Text = "msg";
            this.messagePremierMedicament.Visible = false;
            // 
            // txtId
            // 
            this.txtId.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtId.Enabled = false;
            this.txtId.Font = new System.Drawing.Font("Georgia", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtId.ForeColor = System.Drawing.Color.Red;
            this.txtId.Location = new System.Drawing.Point(239, 46);
            this.txtId.Margin = new System.Windows.Forms.Padding(2);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(50, 28);
            this.txtId.TabIndex = 42;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 50);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 18);
            this.label6.TabIndex = 41;
            this.label6.Text = "Identifiant de la fiche visite ";
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.BackColor = System.Drawing.Color.Red;
            this.btnEnregistrer.Font = new System.Drawing.Font("Georgia", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistrer.Location = new System.Drawing.Point(441, 455);
            this.btnEnregistrer.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(182, 38);
            this.btnEnregistrer.TabIndex = 39;
            this.btnEnregistrer.Text = "Enregistrer le bilan";
            this.btnEnregistrer.UseVisualStyleBackColor = false;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // txtBilan
            // 
            this.txtBilan.AcceptsReturn = true;
            this.txtBilan.AcceptsTab = true;
            this.txtBilan.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBilan.Location = new System.Drawing.Point(441, 299);
            this.txtBilan.Margin = new System.Windows.Forms.Padding(2);
            this.txtBilan.Multiline = true;
            this.txtBilan.Name = "txtBilan";
            this.txtBilan.Size = new System.Drawing.Size(404, 119);
            this.txtBilan.TabIndex = 38;
            // 
            // cbxSecondMedicament
            // 
            this.cbxSecondMedicament.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxSecondMedicament.FormattingEnabled = true;
            this.cbxSecondMedicament.Location = new System.Drawing.Point(441, 225);
            this.cbxSecondMedicament.Name = "cbxSecondMedicament";
            this.cbxSecondMedicament.Size = new System.Drawing.Size(260, 26);
            this.cbxSecondMedicament.TabIndex = 37;
            this.cbxSecondMedicament.SelectedIndexChanged += new System.EventHandler(this.cbxSecondMedicament_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(438, 203);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 18);
            this.label2.TabIndex = 36;
            this.label2.Text = "Second médicament proposé";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(438, 112);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(223, 18);
            this.label5.TabIndex = 34;
            this.label5.Text = "Premier médicament présenté";
            // 
            // cbxPremierMedicament
            // 
            this.cbxPremierMedicament.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPremierMedicament.FormattingEnabled = true;
            this.cbxPremierMedicament.Location = new System.Drawing.Point(441, 135);
            this.cbxPremierMedicament.Name = "cbxPremierMedicament";
            this.cbxPremierMedicament.Size = new System.Drawing.Size(260, 26);
            this.cbxPremierMedicament.TabIndex = 35;
            this.cbxPremierMedicament.SelectedIndexChanged += new System.EventHandler(this.cbxPremierMedicament_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(438, 268);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 18);
            this.label1.TabIndex = 33;
            this.label1.Text = "Bilan de la visite";
            // 
            // zoneSelection
            // 
            this.zoneSelection.Controls.Add(this.cbxVisite);
            this.zoneSelection.Controls.Add(this.message);
            this.zoneSelection.Dock = System.Windows.Forms.DockStyle.Top;
            this.zoneSelection.Location = new System.Drawing.Point(0, 73);
            this.zoneSelection.Name = "zoneSelection";
            this.zoneSelection.Size = new System.Drawing.Size(1048, 86);
            this.zoneSelection.TabIndex = 47;
            // 
            // cbxVisite
            // 
            this.cbxVisite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxVisite.FormattingEnabled = true;
            this.cbxVisite.Location = new System.Drawing.Point(301, 21);
            this.cbxVisite.Name = "cbxVisite";
            this.cbxVisite.Size = new System.Drawing.Size(260, 26);
            this.cbxVisite.TabIndex = 53;
            // l'événement sur changement doit être ajouté après la mise à jour de la propriété DataSource 
            // this.cbxVisite.SelectedIndexChanged += new System.EventHandler(this.cbxVisite_SelectedIndexChanged);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.Location = new System.Drawing.Point(33, 24);
            this.message.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(237, 18);
            this.message.TabIndex = 44;
            this.message.Text = "Sélection de la visite à compléter";
            // 
            // FrmBilan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 648);
            this.Controls.Add(this.zoneSelection);
            this.Controls.Add(this.zoneSaisie);
            this.Controls.Add(this.btnAjouterEchantillon);
            this.Controls.Add(this.label4);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmBilan";
            this.Load += new System.EventHandler(this.FrmBilan_Load);
            this.Controls.SetChildIndex(this.lblTitre, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.btnAjouterEchantillon, 0);
            this.Controls.SetChildIndex(this.zoneSaisie, 0);
            this.Controls.SetChildIndex(this.zoneSelection, 0);
            this.zoneSaisie.ResumeLayout(false);
            this.zoneSaisie.PerformLayout();
            this.zoneSelection.ResumeLayout(false);
            this.zoneSelection.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAjouterEchantillon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel zoneSaisie;
        public System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.TextBox txtBilan;
        private System.Windows.Forms.ComboBox cbxSecondMedicament;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbxPremierMedicament;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label messageBilan;
        private System.Windows.Forms.Label messagePremierMedicament;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIdVisite;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtHeure;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSpecialite;
        private System.Windows.Forms.Label lblSpecialite;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPraticien;
        private System.Windows.Forms.TextBox txtMotif;
        private System.Windows.Forms.TextBox txtVille;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel zoneSelection;
        private System.Windows.Forms.ComboBox cbxVisite;
        private System.Windows.Forms.Label message;
    }
}