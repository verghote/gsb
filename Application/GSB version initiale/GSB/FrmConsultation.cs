﻿// ------------------------------------------
// Nom du fichier : FrmConsultation.cs
// Objet : Formulaire de consultation de l'ensemble des visites
// Auteur : M. Verghote
// Date mise à jour : 02/03/2019
// ------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmConsultation : FrmBase
    {
        private Visite uneVisite; // visite en cours de consultation

        public FrmConsultation()
        {
            InitializeComponent();
        }
        
        private void FrmConsultation_Load(object sender, EventArgs e)
        {
            parametrerComposant();

            if (Globale.db.LesVisites.Count > 0)
            {
                
                remplirDgvVisites();
                uneVisite = (Visite)dgvVisites[0, 0].Value;
                remplirVisite();
            }
            else
            {
                message.Text = "Aucun visite enregistrée pour le moment.";
                message.ForeColor = Color.Red;
                dgvVisites.Visible = false;
                zoneFiche.Visible = false;

            }
        }

        private void dgvVisites_SelectionChanged(object sender, EventArgs e)
        {
            uneVisite = (Visite)dgvVisites.SelectedRows[0].Cells[0].Value;
            remplirVisite();
        }

        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Consultation des visites";
            #endregion

            #region paramétrage dgvVisites

            // Police de caractères
            dgvVisites.DefaultCellStyle.Font = new Font("Georgia", 11);

            // hauteur des lignes
            dgvVisites.RowTemplate.Height = 30;

            // style pour l'entête
            DataGridViewCellStyle style = dgvVisites.ColumnHeadersDefaultCellStyle;
            style.BackColor = Color.Aquamarine;
            style.ForeColor = Color.White;
            style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            style.Font = new Font("Georgia", 12, FontStyle.Bold);

            // définition du mode de sélection des lignes
            dgvVisites.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            // la ligne d'entête est visible
            dgvVisites.ColumnHeadersVisible = true;
            // La colonne d'entête est visible
            dgvVisites.RowHeadersVisible = true;
            // l'utilisateur ne peut ni ajouter ni supprimer des lignes
            dgvVisites.AllowUserToDeleteRows = false;
            dgvVisites.AllowUserToAddRows = false;

            // définition des colonnes : date, heure, ville, 
            dgvVisites.ColumnCount = 5;
            // dgvVisites.Width = 880;

            // les colonnes vont ajuster leur taille au contenu
            dgvVisites.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            dgvVisites.Columns[0].Visible = false;

            dgvVisites.Columns[1].Name = "Date";
            dgvVisites.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvVisites.Columns[2].Name = "Heure";
            dgvVisites.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvVisites.Columns[3].Name = "Lieu";
            dgvVisites.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvVisites.Columns[4].Name = "Praticien";
            dgvVisites.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            #endregion

        }

        private void remplirDgvVisites()
        {
            dgvVisites.Rows.Clear();
            List<Visite> lesVisites = Globale.db.LesVisites;
            foreach (Visite uneVisite in lesVisites)
            {
                dgvVisites.Rows.Add(uneVisite, uneVisite.Date.ToLongDateString(), uneVisite.Heure, uneVisite.LePraticien.Ville, uneVisite.LePraticien.NomPrenom);
            }
        }
       
        private void remplirVisite()
        {
            txtEmail.Text = uneVisite.LePraticien.Email;
            txtMedicament1.Text = uneVisite.PremierMedicament == null ? string.Empty : uneVisite.PremierMedicament.Nom;
            txtMedicament2.Text = uneVisite.SecondMedicament == null ? string.Empty : uneVisite.SecondMedicament.Nom;
            txtRue.Text = uneVisite.LePraticien.Rue;
            txtTelephone.Text = uneVisite.LePraticien.Telephone;
            txtType.Text = uneVisite.LePraticien.Type.Libelle;
            txtSpecialite.Text = uneVisite.LePraticien.Specialite == null ? string.Empty : uneVisite.LePraticien.Specialite.Libelle;
            txtMotif.Text = uneVisite.LeMotif.Libelle;
            txtBilan.Text = uneVisite.Bilan;
        }
    }
}
