﻿namespace GSB
{
    partial class FrmAjout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblVisiteur = new System.Windows.Forms.Label();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.cbxPraticien = new System.Windows.Forms.ComboBox();
            this.cbxMotif = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.zoneSaisie = new System.Windows.Forms.Panel();
            this.messageHeure = new System.Windows.Forms.Label();
            this.messageMotif = new System.Windows.Forms.Label();
            this.messagePraticien = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHeure = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvVisites = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.zoneSaisie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisites)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitre
            // 
            this.lblTitre.Size = new System.Drawing.Size(1104, 49);
            // 
            // lblVisiteur
            // 
            this.lblVisiteur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVisiteur.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVisiteur.Location = new System.Drawing.Point(0, 0);
            this.lblVisiteur.Name = "lblVisiteur";
            this.lblVisiteur.Size = new System.Drawing.Size(1472, 47);
            this.lblVisiteur.TabIndex = 1;
            this.lblVisiteur.Text = "Visiteur";
            // 
            // btnAjouter
            // 
            this.btnAjouter.BackColor = System.Drawing.Color.Red;
            this.btnAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouter.Location = new System.Drawing.Point(64, 289);
            this.btnAjouter.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(182, 38);
            this.btnAjouter.TabIndex = 12;
            this.btnAjouter.Text = "Ajouter";
            this.btnAjouter.UseVisualStyleBackColor = false;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouterFiche_Click);
            // 
            // cbxPraticien
            // 
            this.cbxPraticien.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxPraticien.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxPraticien.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPraticien.FormattingEnabled = true;
            this.cbxPraticien.Location = new System.Drawing.Point(110, 69);
            this.cbxPraticien.Name = "cbxPraticien";
            this.cbxPraticien.Size = new System.Drawing.Size(246, 26);
            this.cbxPraticien.TabIndex = 0;
            // 
            // cbxMotif
            // 
            this.cbxMotif.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxMotif.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxMotif.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxMotif.FormattingEnabled = true;
            this.cbxMotif.Location = new System.Drawing.Point(110, 124);
            this.cbxMotif.Name = "cbxMotif";
            this.cbxMotif.Size = new System.Drawing.Size(246, 26);
            this.cbxMotif.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 124);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Motif";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 69);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Praticien";
            // 
            // dtpDate
            // 
            this.dtpDate.CalendarFont = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Location = new System.Drawing.Point(110, 180);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(173, 26);
            this.dtpDate.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 180);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Date";
            // 
            // zoneSaisie
            // 
            this.zoneSaisie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zoneSaisie.Controls.Add(this.messageHeure);
            this.zoneSaisie.Controls.Add(this.messageMotif);
            this.zoneSaisie.Controls.Add(this.messagePraticien);
            this.zoneSaisie.Controls.Add(this.label4);
            this.zoneSaisie.Controls.Add(this.txtHeure);
            this.zoneSaisie.Controls.Add(this.btnAjouter);
            this.zoneSaisie.Controls.Add(this.label1);
            this.zoneSaisie.Controls.Add(this.label6);
            this.zoneSaisie.Controls.Add(this.dtpDate);
            this.zoneSaisie.Controls.Add(this.label3);
            this.zoneSaisie.Controls.Add(this.label2);
            this.zoneSaisie.Controls.Add(this.cbxMotif);
            this.zoneSaisie.Controls.Add(this.cbxPraticien);
            this.zoneSaisie.Location = new System.Drawing.Point(719, 128);
            this.zoneSaisie.Name = "zoneSaisie";
            this.zoneSaisie.Size = new System.Drawing.Size(376, 346);
            this.zoneSaisie.TabIndex = 1;
            // 
            // messageHeure
            // 
            this.messageHeure.AutoSize = true;
            this.messageHeure.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageHeure.ForeColor = System.Drawing.Color.Red;
            this.messageHeure.Location = new System.Drawing.Point(20, 255);
            this.messageHeure.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageHeure.Name = "messageHeure";
            this.messageHeure.Size = new System.Drawing.Size(39, 17);
            this.messageHeure.TabIndex = 17;
            this.messageHeure.Text = "msg";
            this.messageHeure.Visible = false;
            // 
            // messageMotif
            // 
            this.messageMotif.AutoSize = true;
            this.messageMotif.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageMotif.ForeColor = System.Drawing.Color.Red;
            this.messageMotif.Location = new System.Drawing.Point(20, 152);
            this.messageMotif.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageMotif.Name = "messageMotif";
            this.messageMotif.Size = new System.Drawing.Size(39, 17);
            this.messageMotif.TabIndex = 15;
            this.messageMotif.Text = "msg";
            this.messageMotif.UseWaitCursor = true;
            this.messageMotif.Visible = false;
            // 
            // messagePraticien
            // 
            this.messagePraticien.AutoSize = true;
            this.messagePraticien.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messagePraticien.ForeColor = System.Drawing.Color.Red;
            this.messagePraticien.Location = new System.Drawing.Point(19, 97);
            this.messagePraticien.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messagePraticien.Name = "messagePraticien";
            this.messagePraticien.Size = new System.Drawing.Size(39, 17);
            this.messagePraticien.TabIndex = 14;
            this.messagePraticien.Text = "msg";
            this.messagePraticien.Visible = false;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 13);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(266, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Nouveau rendez vous";
            // 
            // txtHeure
            // 
            this.txtHeure.AutoCompleteCustomSource.AddRange(new string[] {
            "08:00",
            "08:30",
            "09:00",
            "09:30",
            "10:00",
            "10:30",
            "11:00",
            "11:30",
            "12:00",
            "12:30",
            "13:00",
            "13:30",
            "14:00",
            "14:30",
            "15:00",
            "15:30",
            "16:00",
            "16:30",
            "17:00",
            "17:30",
            "18:00",
            "18:30",
            "19:00"});
            this.txtHeure.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtHeure.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtHeure.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeure.Location = new System.Drawing.Point(111, 230);
            this.txtHeure.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtHeure.Name = "txtHeure";
            this.txtHeure.Size = new System.Drawing.Size(171, 26);
            this.txtHeure.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 236);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Horaire";
            // 
            // dgvVisites
            // 
            this.dgvVisites.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVisites.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgvVisites.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVisites.Location = new System.Drawing.Point(9, 128);
            this.dgvVisites.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvVisites.Name = "dgvVisites";
            this.dgvVisites.RowTemplate.Height = 24;
            this.dgvVisites.Size = new System.Drawing.Size(323, 371);
            this.dgvVisites.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 99);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(266, 20);
            this.label5.TabIndex = 14;
            this.label5.Text = "Liste des rendez vous";
            // 
            // FrmAjout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 550);
            this.Controls.Add(this.dgvVisites);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.zoneSaisie);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "FrmAjout";
            this.Text = "Saisie d\'une visite";
            this.Load += new System.EventHandler(this.FmrSaisieVisite_Load);
            this.Controls.SetChildIndex(this.lblTitre, 0);
            this.Controls.SetChildIndex(this.zoneSaisie, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.dgvVisites, 0);
            this.zoneSaisie.ResumeLayout(false);
            this.zoneSaisie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisites)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.Label lblVisiteur;
        private System.Windows.Forms.ComboBox cbxPraticien;
        private System.Windows.Forms.ComboBox cbxMotif;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel zoneSaisie;
        private System.Windows.Forms.TextBox txtHeure;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvVisites;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label messageMotif;
        private System.Windows.Forms.Label messagePraticien;
        private System.Windows.Forms.Label messageHeure;
    }
}