﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public class Donnee
    {
       
        // constructeur
        public Donnee()
        {
            LesMedicaments = new List<Medicament>();
            LesPraticiens = new List<Praticien>();
            LesMotifs = new List<Motif>();
            LesFamilles = new SortedDictionary<string, Famille>();
            LesVisites = new List<Visite>();
            LesTypes = new List<TypePraticien>();
            LesSpecialites = new List<Specialite>();
        }

        #region propriétés

        public SortedDictionary<string, Famille> LesFamilles { get; }
        public List<Medicament> LesMedicaments { get; }
        public List<Praticien> LesPraticiens { get; }
        public List<Motif> LesMotifs { get; }
        public List<Visite> LesVisites { get; }
        public List<TypePraticien> LesTypes { get; }
        public List<Specialite> LesSpecialites { get; }

        #endregion

        #region méthode 

        public void ajouterFamille(Famille uneFamille)
        {
            LesFamilles.Add(uneFamille.Id, uneFamille);
        }

        public void ajouterMedicament(Medicament unMedicamment)
        {
            LesMedicaments.Add(unMedicamment);
        }

        public void ajouterType(TypePraticien unType)
        {
            LesTypes.Add(unType);
        }

        public void ajouterSpecialite(Specialite uneSpecialite)
        {
            LesSpecialites.Add(uneSpecialite);
        }


        public void ajouterPraticien(Praticien unPraticien)
        {
            LesPraticiens.Add(unPraticien);
        }

        public void ajouterMotif(Motif unMotif)
        {
            LesMotifs.Add(unMotif);
        }

        public void ajouterVisite(Visite uneVisite)
        {
            LesVisites.Add(uneVisite);
        }

        

        
        #endregion
          
    }
}
