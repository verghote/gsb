﻿namespace GSB
{
    partial class FrmBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBase));
            this.labelGsb = new System.Windows.Forms.Label();
            this.lblVisiteur = new System.Windows.Forms.Label();
            this.lblTitre = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consulterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.créerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLaPlannificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enregistrerLeBilanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seDéconnecterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelGsb
            // 
            this.labelGsb.BackColor = System.Drawing.Color.Aqua;
            this.labelGsb.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelGsb.Font = new System.Drawing.Font("Georgia", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGsb.Location = new System.Drawing.Point(0, 291);
            this.labelGsb.Name = "labelGsb";
            this.labelGsb.Size = new System.Drawing.Size(584, 30);
            this.labelGsb.TabIndex = 8;
            this.labelGsb.Text = "GSB - Galaxy Swiss Bourdin";
            this.labelGsb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVisiteur
            // 
            this.lblVisiteur.BackColor = System.Drawing.Color.Aqua;
            this.lblVisiteur.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblVisiteur.Font = new System.Drawing.Font("Georgia", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVisiteur.Location = new System.Drawing.Point(0, 261);
            this.lblVisiteur.Name = "lblVisiteur";
            this.lblVisiteur.Size = new System.Drawing.Size(584, 30);
            this.lblVisiteur.TabIndex = 4;
            this.lblVisiteur.Text = "Visiteur";
            this.lblVisiteur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTitre
            // 
            this.lblTitre.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitre.Font = new System.Drawing.Font("Georgia", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitre.Location = new System.Drawing.Point(0, 28);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(584, 60);
            this.lblTitre.TabIndex = 9;
            this.lblTitre.Text = "Titre";
            this.lblTitre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.seDéconnecterToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(584, 28);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consulterToolStripMenuItem,
            this.créerToolStripMenuItem,
            this.modifierLaPlannificationToolStripMenuItem,
            this.enregistrerLeBilanToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.fichierToolStripMenuItem.Text = "Visite";
            // 
            // consulterToolStripMenuItem
            // 
            this.consulterToolStripMenuItem.Name = "consulterToolStripMenuItem";
            this.consulterToolStripMenuItem.Size = new System.Drawing.Size(284, 26);
            this.consulterToolStripMenuItem.Text = "Consulter";
            this.consulterToolStripMenuItem.Click += new System.EventHandler(this.consulterToolStripMenuItem_Click);
            // 
            // créerToolStripMenuItem
            // 
            this.créerToolStripMenuItem.Name = "créerToolStripMenuItem";
            this.créerToolStripMenuItem.Size = new System.Drawing.Size(284, 26);
            this.créerToolStripMenuItem.Text = "Créer un nouveau rendez-vous";
            this.créerToolStripMenuItem.Click += new System.EventHandler(this.créerToolStripMenuItem_Click);
            // 
            // modifierLaPlannificationToolStripMenuItem
            // 
            this.modifierLaPlannificationToolStripMenuItem.Name = "modifierLaPlannificationToolStripMenuItem";
            this.modifierLaPlannificationToolStripMenuItem.Size = new System.Drawing.Size(284, 26);
            this.modifierLaPlannificationToolStripMenuItem.Text = "Modifier la plannification";
            this.modifierLaPlannificationToolStripMenuItem.Click += new System.EventHandler(this.modifierLaPlannificationToolStripMenuItem_Click);
            // 
            // enregistrerLeBilanToolStripMenuItem
            // 
            this.enregistrerLeBilanToolStripMenuItem.Name = "enregistrerLeBilanToolStripMenuItem";
            this.enregistrerLeBilanToolStripMenuItem.Size = new System.Drawing.Size(284, 26);
            this.enregistrerLeBilanToolStripMenuItem.Text = "Enregistrer le bilan";
            this.enregistrerLeBilanToolStripMenuItem.Click += new System.EventHandler(this.enregistrerLeBilanToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(67, 24);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // seDéconnecterToolStripMenuItem
            // 
            this.seDéconnecterToolStripMenuItem.Name = "seDéconnecterToolStripMenuItem";
            this.seDéconnecterToolStripMenuItem.Size = new System.Drawing.Size(123, 24);
            this.seDéconnecterToolStripMenuItem.Text = "Se déconnecter";
            this.seDéconnecterToolStripMenuItem.Click += new System.EventHandler(this.seDéconnecterToolStripMenuItem_Click);
            // 
            // FrmBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(584, 321);
            this.Controls.Add(this.lblTitre);
            this.Controls.Add(this.lblVisiteur);
            this.Controls.Add(this.labelGsb);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmBase";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmBase_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelGsb;
        private System.Windows.Forms.Label lblVisiteur;
        protected System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.MenuStrip menuStrip1;
        protected System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem consulterToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem créerToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem modifierLaPlannificationToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem enregistrerLeBilanToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem seDéconnecterToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
    }
}