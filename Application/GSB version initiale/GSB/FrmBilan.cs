﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmBilan : FrmBase
    {
        private Visite uneVisite; // mémorise la visite en cours de mise à jour
        private List<Visite> lesVisites =  Globale.db.LesVisites.FindAll(element => element.Date <= DateTime.Today && element.Bilan == null);

        public FrmBilan()
        {
            InitializeComponent();
        }

        #region procédures événementielles

        private void FrmBilan_Load(object sender, EventArgs e)
        {
            parametrerComposant();
            remplirVisite();
        }


        private void cbxVisite_SelectedIndexChanged(object sender, EventArgs e)
        {
            uneVisite = (Visite)cbxVisite.SelectedItem;
            remplirVisite();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            modification();
        }

       
        
        private void cbxPremierMedicament_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbxPremierMedicament.BackColor = Color.White;
        }

        private void cbxSecondMedicament_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbxSecondMedicament.BackColor = Color.White;
        }

       

        #endregion

        #region méthodes

        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Enregistrement du bilan d'une visite";
            #endregion

            #region paramétrage zone de liste des Visites à compléter
            cbxVisite.DataSource = lesVisites;
            cbxVisite.DisplayMember = "LePraticien";
            cbxVisite.ValueMember = "Id";
            cbxVisite.SelectedIndex = -1;

            // l'événement sur changement dans une zone de liste se déclenche lors de la modification de la propriété DataSource
            // l'événement doit donc être ajouté après avoir modifier la propriété DataSource
            cbxVisite.SelectedIndexChanged += new System.EventHandler(this.cbxVisite_SelectedIndexChanged);
            cbxVisite.SelectedIndex = 0;

            #endregion

            #region paramétrage zone de liste pour les médicaments proposés

            cbxPremierMedicament.DataSource = Globale.db.LesMedicaments;
            cbxPremierMedicament.DisplayMember = "Nom";
            cbxPremierMedicament.ValueMember = "Id";
            cbxPremierMedicament.SelectedIndex = -1;

            // important : Pour pouvoir utiliser le même dataSource sur deux listes
            // il faut créer un nouveau context sinon les deux listes sont synchroniées
            cbxSecondMedicament.BindingContext = new BindingContext();

            cbxSecondMedicament.DataSource = Globale.db.LesMedicaments;
            cbxSecondMedicament.DisplayMember = "Nom";
            cbxSecondMedicament.ValueMember = "Id";
            cbxSecondMedicament.SelectedItem = null;
            #endregion
        }

        private void remplirVisite()
        {
            txtIdVisite.Text = uneVisite.Id.ToString();
            txtDate.Text = uneVisite.Date.ToLongDateString();
            txtHeure.Text = uneVisite.Heure;
            txtVille.Text = uneVisite.LePraticien.Ville;
            txtMotif.Text = uneVisite.LeMotif.Libelle;
            txtPraticien.Text = uneVisite.LePraticien.NomPrenom;
            txtType.Text = uneVisite.LePraticien.Type.Libelle;
            if (uneVisite.LePraticien.Specialite != null)
            {
                lblSpecialite.Visible = true;
                txtSpecialite.Visible = true;
                txtSpecialite.Text = uneVisite.LePraticien.Specialite.Libelle;
            }
            else
            {
                lblSpecialite.Visible = false;
                txtSpecialite.Visible = false;
            }

            cbxPremierMedicament.SelectedItem = uneVisite.PremierMedicament == null ? null : uneVisite.PremierMedicament;
            cbxSecondMedicament.SelectedItem = uneVisite.SecondMedicament == null ? null : uneVisite.SecondMedicament;
            txtBilan.Text = uneVisite.Bilan == null ? string.Empty : uneVisite.Bilan;

            zoneSaisie.Visible = true;
        }


        private void modification()
        {
            bool premierMedicamentOk = controlerPremierMedicament();
            bool bilanOk = controlerBilan();
            if (premierMedicamentOk && bilanOk)
            {
                modifier();
            }
        }

        private bool controlerPremierMedicament()
        {
            if (cbxPremierMedicament.SelectedIndex == -1)
            {
                messagePremierMedicament.Text = "Veuillez sélectionner un médicament.";
                messagePremierMedicament.Visible = true;
                return false;
            }
            messagePremierMedicament.Text = "";
            messagePremierMedicament.Visible = false;
            return true;
        }

        private bool controlerBilan()
        {
            if (txtBilan.Text == string.Empty)
            {
                messageBilan.Text = "Le bilan doit être complété ";
                messageBilan.Visible = true;
                return false;
            }
            messageBilan.Text = "";
            messageBilan.Visible = false;
            return true;
        }

        private void modifier()
        {
            // enregistrement du premier médicament présenté
            Medicament premierMedicament = (Medicament)cbxPremierMedicament.SelectedItem;
            Medicament secondMedicament = null;
            bool ok;
            string message;
            if (cbxSecondMedicament.SelectedIndex >= 0)
            {
                secondMedicament = (Medicament)cbxSecondMedicament.SelectedItem;
                ok = Passerelle.enregistrerBilanVisite(uneVisite.Id, txtBilan.Text, premierMedicament.Id, secondMedicament.Id, out message);

            }
            else
            {
                ok = Passerelle.enregistrerBilanVisite(uneVisite.Id, txtBilan.Text, premierMedicament.Id, null, out message);
            }
            if (ok)
            {
                // mise à jour des données 
                uneVisite.PremierMedicament = premierMedicament;
                uneVisite.SecondMedicament = secondMedicament;
                uneVisite.Bilan = txtBilan.Text;
                MessageBox.Show("Bilan visite mémorisé", "", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion


    }
}


