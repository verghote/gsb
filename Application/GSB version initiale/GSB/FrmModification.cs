﻿// ------------------------------------------
// Nom du fichier : FrmModification.cs
// Objet : Formulaire permettant de modifier la date et l'horaire d'un rendez-vous
// Auteur : M. Verghote
// Date mise à jour : 16/03/2019
// ------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmModification : FrmBase
    {
        private Visite uneVisite; // visite en cours de modification

        public FrmModification()
        {
            InitializeComponent();
        }

        private void FrmModification_Load(object sender, EventArgs e)
        {
            parametrerComposant();
            remplirDgvVisites();
            remplirVisite();
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            modification();
        }

        private void dgvVisites_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            // s'agit-il d'une demande de suppression
            
            string message;
            if (e.ColumnIndex == 5)
            {
                if (MessageBox.Show("Confirmez vous la suppression de ce rendez -vous ?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    bool ok = Passerelle.supprimerVisite(uneVisite.Id, out message);
                    if (ok)
                    {
                        // mise à jour des données : à réaliser avant la mise à jour de l'interface
                        Globale.db.LesVisites.RemoveAll(element => element.Id == uneVisite.Id);


                        // mise à jour de l'interface : suppression de la ligne sélectionné dans le dgvVisites
                        DataGridViewRow ligne = dgvVisites.SelectedRows[0];
                        dgvVisites.Rows.Remove(ligne);
                        remplirVisite();

                    } else
                    {
                        MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void dgvVisites_SelectionChanged(object sender, EventArgs e)
        {
            // suite à la modification d'une date le composant est rafraichi et il n'y a pas de ligne sélectionnéé 
            if (dgvVisites.SelectedRows.Count == 1)
            {
                uneVisite = (Visite)dgvVisites.SelectedRows[0].Cells[0].Value;
                remplirVisite();
            }
        }

        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Modification de la planification d'un rendez-vous";
            #endregion


            #region paramétrage du composant dateTimePicker : la prise de rendez vous s'effectue sur les deux mois à venir
            dtpDate.MinDate = DateTime.Today;
            dtpDate.MaxDate = DateTime.Today.AddDays(60);
            #endregion

            #region paramétrage de l'heure
            txtHeure.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtHeure.AutoCompleteSource = AutoCompleteSource.CustomSource;

            var source = new AutoCompleteStringCollection();
            source.AddRange(new string[]
                            {"08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00"});
            txtHeure.AutoCompleteCustomSource = source;
            #endregion

            #region Parametrage dgvVisites

            // définition du style par défaut pour les cellules d'entête de colonne
            DataGridViewCellStyle style = dgvVisites.ColumnHeadersDefaultCellStyle;
            style.BackColor = Color.Aquamarine;
            style.ForeColor = Color.White;
            style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            style.Font = new Font("Georgia", 12, FontStyle.Bold);

            // définition du mode de sélection des lignes
            dgvVisites.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            // la ligne d'entête est visible
            dgvVisites.ColumnHeadersVisible = true;
            // La colonne d'entête n'est pas visible
            dgvVisites.RowHeadersVisible = false;
            // l'utilisateur ne peut ni ajouter ni supprimer des lignes
            dgvVisites.AllowUserToDeleteRows = false;
            dgvVisites.AllowUserToAddRows = false;

            // définition des colonnes : date, heure, ville, 
            dgvVisites.ColumnCount = 5;
            dgvVisites.Width = 880;
            dgvVisites.Columns[0].Visible = false;

            dgvVisites.Columns[1].Name = "Date";
            dgvVisites.Columns[1].Width = 300;
            dgvVisites.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvVisites.Columns[2].Name = "Heure";
            dgvVisites.Columns[2].Width = 100;
            dgvVisites.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvVisites.Columns[3].Name = "Lieu";
            dgvVisites.Columns[3].Width = 200;
            dgvVisites.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvVisites.Columns[4].Name = "Praticien";
            dgvVisites.Columns[4].Width = 300;
            dgvVisites.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            // ajout d'une colonne contenant le bouton supprimer
            DataGridViewButtonColumn uneColonneBouton = new DataGridViewButtonColumn();
            uneColonneBouton.HeaderText = "Action";
            uneColonneBouton.Name = "Action";
            uneColonneBouton.Text = "Supprimer";
            uneColonneBouton.UseColumnTextForButtonValue = true;
            dgvVisites.Columns.Add(uneColonneBouton);

        

            #endregion
            
        }


        // remplir le datagridview dgvVisites
        private void remplirDgvVisites()
        {
            dgvVisites.Rows.Clear();
            List<Visite> lesProchainesVisites = Globale.db.LesVisites.FindAll(element => element.Date >= DateTime.Today);
            foreach (Visite uneVisite in lesProchainesVisites)
            {
                dgvVisites.Rows.Add(uneVisite, uneVisite.Date.ToLongDateString(), uneVisite.Heure, uneVisite.LePraticien.Ville, uneVisite.LePraticien.NomPrenom);
            }
        }

        private void remplirVisite()
        {
            if (dgvVisites.Rows.Count > 0)
            {

                txtId.Text = uneVisite.Id.ToString();
                txtHeure.Text = uneVisite.Heure;
                dtpDate.Text = uneVisite.Date.ToShortDateString();
            }
            else
            {
                message.Text = "Aucune visite n'est programmée dans les jours à venir";
                message.ForeColor = Color.Red;
                zoneSaisie.Visible = false;
                dgvVisites.Visible = false;
                modifierLaPlannificationToolStripMenuItem.Enabled = false;
            }
        }

        private void modification()
        {
            if (controlerHeure())
            {
                int id = Int32.Parse(txtId.Text);
                DateTime uneDate = dtpDate.Value;
                string uneHeure = txtHeure.Text;

                string message;
                bool ok = Passerelle.modifierPlanificationVisite(id, uneDate, uneHeure, out message);
                if (ok)
                {
                    // mise à jour des données 
                    Visite uneVisite = Globale.db.LesVisites.Find(element => element.Id == id);
                    uneVisite.Date = uneDate;
                    uneVisite.Heure = uneHeure;
                    Globale.db.LesVisites.Sort();

                    // mise à jour de l'interface : Attention, cela déclenche SelectionChanged 
                    remplirDgvVisites();
                }
                else
                {
                    MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private bool controlerHeure()
        {
            if (txtHeure.Text == string.Empty)
            {
                messageHeure.Text = "L'heure du rendez-vous doit être précisée ";
                messageHeure.Visible = true;
                return false;
            }
            messageHeure.Text = "";
            messageHeure.Visible = false;
            return true;
        }
    }
}
