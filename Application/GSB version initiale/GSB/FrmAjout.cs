﻿// ------------------------------------------
// Nom du fichier : FrmAjout.cs
// Objet : Formulaire de saisie d'une nouvelle visite
// Auteur : M. Verghote
// Date mise à jour : 16/03/2019
// ------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmAjout : FrmBase
    {

        public FrmAjout()
        {
            InitializeComponent();
        }

        private void FmrSaisieVisite_Load(object sender, EventArgs e)
        {
            parametrerComposant();
            remplirdgvVisites();
        }
        

        private void btnAjouterFiche_Click(object sender, EventArgs e)
        {
            ajout();
        }


        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Enregistrer un nouveau rendez-vous";
            #endregion

            #region paramètrage des zones de liste
            // alimentation de la zone de liste déroulante contenant les praticiens
            cbxPraticien.DataSource = Globale.db.LesPraticiens;
            cbxPraticien.DisplayMember = "NomPrenom";
            cbxPraticien.ValueMember = "Id";
            cbxPraticien.SelectedIndex = -1;

            // alimentation de la zone de liste déroulante contenant les motifs
            cbxMotif.DataSource = Globale.db.LesMotifs;
            cbxMotif.DisplayMember = "Libelle";
            cbxMotif.ValueMember = "Id";
            cbxMotif.SelectedItem = null;
            #endregion

            #region paramétrage du composant dateTimePicker : la prise de rendez vous s'effectue sur les deux mois à venir
            dtpDate.MinDate = DateTime.Today;
            dtpDate.MaxDate = DateTime.Today.AddDays(60);
            #endregion

            #region Parametrage dgvVisites

            // définition du style par défaut pour les cellules d'entête de colonne
            DataGridViewCellStyle style = dgvVisites.ColumnHeadersDefaultCellStyle;
            style.BackColor = Color.Aquamarine;
            style.ForeColor = Color.White;
            style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            style.Font = new Font("Georgia", 12, FontStyle.Bold);

            // définition du mode de sélection des lignes
            dgvVisites.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            // la ligne d'entête est visible
            dgvVisites.ColumnHeadersVisible = true;
            // La colonne d'entête n'est pas visible
            dgvVisites.RowHeadersVisible = false;
            // l'utilisateur ne peut ni ajouter ni supprimer des lignes
            dgvVisites.AllowUserToDeleteRows = false;
            dgvVisites.AllowUserToAddRows = false;

            // définition des colonnes : date, heure, ville, 
            dgvVisites.ColumnCount = 4;
            dgvVisites.Width = 680;

            dgvVisites.Columns[0].Name = "Date";
            dgvVisites.Columns[0].Width = 250;
            dgvVisites.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvVisites.Columns[1].Name = "Heure";
            dgvVisites.Columns[1].Width = 100;
            dgvVisites.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvVisites.Columns[2].Name = "Lieu";
            dgvVisites.Columns[2].Width = 200;
            dgvVisites.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvVisites.Columns[3].Name = "Praticien";
            dgvVisites.Columns[3].Width = 200;
            dgvVisites.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            // suppression de la mise en forme de la ligne sélectionnée
            dgvVisites.RowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            dgvVisites.RowsDefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            #endregion
        }


        // remplir le datagridview dgvVisites
        private void remplirdgvVisites()
        {
            dgvVisites.Rows.Clear();
            List<Visite> lesProchainesVisites = Globale.db.LesVisites.FindAll(element => element.Date >= DateTime.Today);
            foreach (Visite uneVisite in lesProchainesVisites)
            {
                dgvVisites.Rows.Add(uneVisite.Date.ToLongDateString(), uneVisite.Heure, uneVisite.LePraticien.Ville, uneVisite.LePraticien.NomPrenom);
            }
        }

        private void ajout()
        {
            bool praticienOk = controlerPraticien();
            bool motifOk = controlerMotif();
            // la date est contrôlée par le composant dateTimePicker
            bool heureOk = controlerHeure();
            if (praticienOk && motifOk && heureOk)
            {
                ajouter();
            }
        }

        private bool controlerPraticien()
        {
            if (cbxPraticien.SelectedIndex == -1)
            {
                messagePraticien.Text = "Veuillez sélectionner un praticien.";
                messagePraticien.Visible = true;
                return false;
            }
            messagePraticien.Text = "";
            messagePraticien.Visible = false;
            return true;
        }

        private bool controlerMotif()
        {
            if (cbxMotif.SelectedIndex == -1)
            {
                messageMotif.Text = "Veuillez sélectionner un praticien.";
                messageMotif.Visible = true;
                return false;
            }
            messageMotif.Text = "";
            messageMotif.Visible = false;
            return true;
        }

        private bool controlerHeure()
        {
            if (txtHeure.Text == string.Empty)
            {
                messageHeure.Text = "L'heure du rendez-vous doit être précisée ";
                messageHeure.Visible = true;
                return false;
            }
            messageHeure.Text = "";
            messageHeure.Visible = false;
            return true;
        }

        private void ajouter()
        {
            int idPraticien = (Int32)cbxPraticien.SelectedValue;
            int idMotif = (Int32)cbxMotif.SelectedValue;
            DateTime uneDate = dtpDate.Value;
            string uneHeure = txtHeure.Text;
            string message;
            int idVisite = Passerelle.ajouterVisite(idPraticien, idMotif, uneDate, uneHeure, out message);
            if (idVisite == 0)
            {
                MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                // mise à jour des données en mémoire : la collection des visites

                // récupération des objets liés
                Motif unMotif = Globale.db.LesMotifs.Find(element => element.Id == idMotif);
                Praticien unPraticien = Globale.db.LesPraticiens.Find(element => element.Id == idPraticien);
                // création de l'objet et ajout dans donnees
                Visite uneVisite = new Visite(idVisite, unPraticien, unMotif, dtpDate.Value.Date, uneHeure);
                Globale.db.LesVisites.Add(uneVisite);
                Globale.db.LesVisites.Sort();

                // mise à jour de l'interface
                cbxMotif.SelectedIndex = -1;
                cbxPraticien.SelectedIndex = -1;
                txtHeure.Text = "";
                remplirdgvVisites();

                // réactiver l'option permettant la modification du rendez-vous 
                modifierLaPlannificationToolStripMenuItem.Enabled = true;
            }
        }
    }
}

