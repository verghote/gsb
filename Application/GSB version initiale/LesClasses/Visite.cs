﻿using System;

namespace lesClasses
{

    public class Visite : IComparable<Visite>
    {

        public int CompareTo(Visite v)
        {
            int res = Date.CompareTo(v.Date);
            if (res == 0)
            {
                res = Heure.CompareTo(v.Heure);
            }
            return res;
        }


        // Constructeur
        public Visite(int unId, Praticien unPraticien, Motif unMotif, DateTime uneDate, string uneHeure, String unBilan, Medicament premierMedicament, Medicament secondMedicament)
        {
            Id = unId;
            LePraticien = unPraticien;
            LeMotif = unMotif;
            Date = uneDate;
            Heure = uneHeure;
            Bilan = unBilan;
            PremierMedicament = premierMedicament;
            SecondMedicament = secondMedicament;
        }

        // Surcharge du constructeur
        public Visite(int unId, Praticien unPraticien, Motif unMotif, DateTime uneDate, string uneHeure)
        {
            Id = unId;
            LePraticien = unPraticien;
            LeMotif = unMotif;
            Date = uneDate;
            Heure = uneHeure;
            Bilan = null;
            PremierMedicament = null;
            SecondMedicament = null;
        }

        // Propriétés sans attribut privé
        public int Id { get; }
        public Praticien LePraticien { get; set; }
        public Motif LeMotif { get; set; }
        public DateTime Date { get; set; }
        public string Heure { get; set; }
        public string Bilan { get; set; }
        public Medicament PremierMedicament { get; set; }
        public Medicament SecondMedicament { get; set; }
    }
}
