﻿using System;

namespace lesClasses
{
    [Serializable]
    public class Medicament : IComparable<Medicament>
    {

        public int CompareTo(Medicament o) {
            return Id.CompareTo(o.Id);
        }

        // Constructeur
        public Medicament (string unId, string unNom, string uneComposition, string desEffets, string uneContreIndication, Famille uneFamille)
        {
            Id = unId;
            Nom = unNom;
            Composition = uneComposition;
            Effets = desEffets;
            ContreIndication = uneContreIndication;
            LaFamille = uneFamille;
            LaFamille.ajouterMedicament(this);
        }

        // Propriétés
        public string Id { get; set; }
        public string Nom { get; set; }
        public string Composition { get; set; }
        public string Effets { get; set; }
        public string ContreIndication { get; set; }
        public Famille LaFamille { get; set; }
    }
}
