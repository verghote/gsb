﻿using System;
using System.Collections.Generic;

namespace lesClasses
{
    [Serializable]
    public class Famille
    {

        // Constructeur
        public Famille (string unId, string unLibelle)
        {
            Id = unId;
            Libelle = unLibelle;
            LesMedicaments = new List<Medicament>();
        }
        
        // Propriétés
        public string Id { get; set; }
        public string Libelle { get; set; }
        public List<Medicament> LesMedicaments { get; }

        // méthode
        public void ajouterMedicament(Medicament unMedicament)
        {
            LesMedicaments.Add(unMedicament);
        }
    
    }
}
