﻿using System;

namespace lesClasses
{
    public class Motif
    {
        // Constructeur
        public Motif (int unId, string unLibelle)
        {
            Id = unId;
            Libelle = unLibelle;
        }

        // Propriétés
        public int Id { get; set; }
        public string Libelle { get; set; }

    }
}
