﻿namespace lesClasses
{
    public class Praticien : Personne
    {

        // Constructeur
        public Praticien (int unId, string unNom, string unPrenom, string uneRue, string unCodePostal, string uneVille, string unEmail, string unTelephone, TypePraticien unType, Specialite uneSpecialite) 
            : base (unNom, unPrenom, uneRue, unCodePostal, uneVille)
        {
            Id = unId;
            Email = unEmail;
            Telephone = unTelephone;
            Type = unType;
            Specialite = uneSpecialite;
        }

        // Propriétés sans attribut privé
        public int Id { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public TypePraticien Type { get; set;}
        public Specialite Specialite { get; set; }

        // Méthodes 
        public override string getType()  { return "Praticien"; }
    }
}
