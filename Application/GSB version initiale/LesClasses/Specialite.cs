﻿namespace lesClasses
{
    public class Specialite
    {
        // Constructeur
        public Specialite(string unId, string unLibelle)
        {
            Id = unId;
            Libelle = unLibelle;
        }


        // Propriétés
        public string Id { get; set; }
        public string Libelle { get; set; }
    }
}
