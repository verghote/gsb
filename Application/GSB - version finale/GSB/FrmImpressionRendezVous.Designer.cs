﻿namespace GSB
{
    partial class FrmImpressionRendezVous
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmImpressionRendezVous));
            this.message = new System.Windows.Forms.Label();
            this.imgGsb = new System.Windows.Forms.PictureBox();
            this.zoneSaisie = new System.Windows.Forms.Panel();
            this.messageIntervale = new System.Windows.Forms.Label();
            this.imgApercu = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpFin = new System.Windows.Forms.DateTimePicker();
            this.imgImprimer = new System.Windows.Forms.PictureBox();
            this.dtpDebut = new System.Windows.Forms.DateTimePicker();
            this.printRendezVous = new System.Drawing.Printing.PrintDocument();
            this.choixImprimante = new System.Windows.Forms.PrintDialog();
            this.apercuRendezVous = new System.Windows.Forms.PrintPreviewDialog();
            ((System.ComponentModel.ISupportInitialize)(this.imgGsb)).BeginInit();
            this.zoneSaisie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgApercu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgImprimer)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitre
            // 
            this.lblTitre.Size = new System.Drawing.Size(1001, 60);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.ForeColor = System.Drawing.Color.Red;
            this.message.Location = new System.Drawing.Point(33, 88);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(46, 20);
            this.message.TabIndex = 15;
            this.message.Text = "msg";
            this.message.Visible = false;
            // 
            // imgGsb
            // 
            this.imgGsb.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("imgGsb.BackgroundImage")));
            this.imgGsb.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgGsb.Location = new System.Drawing.Point(12, 120);
            this.imgGsb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.imgGsb.Name = "imgGsb";
            this.imgGsb.Size = new System.Drawing.Size(400, 300);
            this.imgGsb.TabIndex = 18;
            this.imgGsb.TabStop = false;
            // 
            // zoneSaisie
            // 
            this.zoneSaisie.Controls.Add(this.messageIntervale);
            this.zoneSaisie.Controls.Add(this.imgApercu);
            this.zoneSaisie.Controls.Add(this.label2);
            this.zoneSaisie.Controls.Add(this.label1);
            this.zoneSaisie.Controls.Add(this.dtpFin);
            this.zoneSaisie.Controls.Add(this.imgImprimer);
            this.zoneSaisie.Controls.Add(this.dtpDebut);
            this.zoneSaisie.Location = new System.Drawing.Point(462, 122);
            this.zoneSaisie.Name = "zoneSaisie";
            this.zoneSaisie.Size = new System.Drawing.Size(500, 374);
            this.zoneSaisie.TabIndex = 19;
            // 
            // messageIntervale
            // 
            this.messageIntervale.AutoSize = true;
            this.messageIntervale.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageIntervale.ForeColor = System.Drawing.Color.Red;
            this.messageIntervale.Location = new System.Drawing.Point(31, 163);
            this.messageIntervale.Name = "messageIntervale";
            this.messageIntervale.Size = new System.Drawing.Size(46, 20);
            this.messageIntervale.TabIndex = 23;
            this.messageIntervale.Text = "msg";
            this.messageIntervale.Visible = false;
            // 
            // imgApercu
            // 
            this.imgApercu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("imgApercu.BackgroundImage")));
            this.imgApercu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgApercu.Location = new System.Drawing.Point(44, 223);
            this.imgApercu.Name = "imgApercu";
            this.imgApercu.Size = new System.Drawing.Size(149, 126);
            this.imgApercu.TabIndex = 22;
            this.imgApercu.TabStop = false;
            this.imgApercu.Click += new System.EventHandler(this.imgApercu_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 24);
            this.label2.TabIndex = 21;
            this.label2.Text = "Au";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 24);
            this.label1.TabIndex = 20;
            this.label1.Text = "Du";
            // 
            // dtpFin
            // 
            this.dtpFin.CalendarFont = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFin.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFin.Location = new System.Drawing.Point(74, 97);
            this.dtpFin.Name = "dtpFin";
            this.dtpFin.Size = new System.Drawing.Size(375, 30);
            this.dtpFin.TabIndex = 19;
            this.dtpFin.ValueChanged += new System.EventHandler(this.dtpFin_ValueChanged);
            // 
            // imgImprimer
            // 
            this.imgImprimer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("imgImprimer.BackgroundImage")));
            this.imgImprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgImprimer.Location = new System.Drawing.Point(247, 223);
            this.imgImprimer.Name = "imgImprimer";
            this.imgImprimer.Size = new System.Drawing.Size(149, 126);
            this.imgImprimer.TabIndex = 18;
            this.imgImprimer.TabStop = false;
            this.imgImprimer.Click += new System.EventHandler(this.imgImprimer_Click);
            // 
            // dtpDebut
            // 
            this.dtpDebut.CalendarFont = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDebut.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDebut.Location = new System.Drawing.Point(74, 27);
            this.dtpDebut.Name = "dtpDebut";
            this.dtpDebut.Size = new System.Drawing.Size(375, 30);
            this.dtpDebut.TabIndex = 12;
            this.dtpDebut.ValueChanged += new System.EventHandler(this.dtpDebut_ValueChanged);
            // 
            // printRendezVous
            // 
            this.printRendezVous.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printRendezVous_PrintPage);
            // 
            // choixImprimante
            // 
            this.choixImprimante.UseEXDialog = true;
            // 
            // apercuRendezVous
            // 
            this.apercuRendezVous.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.apercuRendezVous.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.apercuRendezVous.ClientSize = new System.Drawing.Size(400, 300);
            this.apercuRendezVous.Document = this.printRendezVous;
            this.apercuRendezVous.Enabled = true;
            this.apercuRendezVous.Icon = ((System.Drawing.Icon)(resources.GetObject("apercuRendezVous.Icon")));
            this.apercuRendezVous.Name = "apercuRendezVous";
            this.apercuRendezVous.Visible = false;
            // 
            // FrmImpressionRendezVous
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 661);
            this.Controls.Add(this.zoneSaisie);
            this.Controls.Add(this.imgGsb);
            this.Controls.Add(this.message);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmImpressionRendezVous";
            this.Text = "FrmImpressionRendezVous";
            this.Load += new System.EventHandler(this.FrmImpressionRendezVous_Load);
            this.Controls.SetChildIndex(this.lblTitre, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.imgGsb, 0);
            this.Controls.SetChildIndex(this.zoneSaisie, 0);
            ((System.ComponentModel.ISupportInitialize)(this.imgGsb)).EndInit();
            this.zoneSaisie.ResumeLayout(false);
            this.zoneSaisie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgApercu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgImprimer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.PictureBox imgGsb;
        private System.Windows.Forms.Panel zoneSaisie;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpFin;
        private System.Windows.Forms.PictureBox imgImprimer;
        private System.Windows.Forms.DateTimePicker dtpDebut;
        private System.Drawing.Printing.PrintDocument printRendezVous;
        private System.Windows.Forms.PrintDialog choixImprimante;
        private System.Windows.Forms.PictureBox imgApercu;
        private System.Windows.Forms.PrintPreviewDialog apercuRendezVous;
        private System.Windows.Forms.Label messageIntervale;
    }
}