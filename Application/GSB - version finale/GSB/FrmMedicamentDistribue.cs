﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmMedicamentDistribue : FrmBase
    {
        public FrmMedicamentDistribue()
        {
            InitializeComponent();
        }

        #region procédures événementielles

        private void FrmMedicamentDistribue_Load(object sender, EventArgs e)
        {
            parametrerComposant();
            remplirDgvMedicaments();
        }

        #endregion

        #region méthodes

        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Synthèse des médicaments distribués lors des visites";
            #endregion

            #region paramétragedgvMedicaments

            dgvMedicaments.Enabled = false;

            // Police de caractères
           dgvMedicaments.DefaultCellStyle.Font = new Font("Georgia", 12);

            // hauteur des lignes
           dgvMedicaments.RowTemplate.Height = 30;

            // pas de mise en évidence de la zone sélectionnée
            dgvMedicaments.RowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            dgvMedicaments.RowsDefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;


            // la ligne d'entête est visible
            dgvMedicaments.ColumnHeadersVisible = true;
            // La colonne d'entête n'est pas visible
           dgvMedicaments.RowHeadersVisible = false;

            // mie en forme de la ligne d'entête
            DataGridViewCellStyle style = dgvMedicaments.ColumnHeadersDefaultCellStyle;
            style.BackColor = Color.Aquamarine;
            style.ForeColor = Color.White;
            style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            style.Font = new Font("Georgia", 13, FontStyle.Bold);

            // définition des colonnes : nom et total
            dgvMedicaments.ColumnCount = 2;

            // taille, titre et alignements des colonnes

            dgvMedicaments.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

            dgvMedicaments.Columns[0].Width = 300;
            dgvMedicaments.Columns[0].Name = "Médicament";
            dgvMedicaments.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvMedicaments.Columns[1].Width = 200;
            dgvMedicaments.Columns[1].Name = "Total distribué";
            dgvMedicaments.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            #endregion
        }

        private void remplirDgvMedicaments()
        {
            foreach (KeyValuePair<string, int> unCouple in Globale.db.LesDistributions)
            {
               dgvMedicaments.Rows.Add(unCouple.Key ,unCouple.Value);
            }
        }

        #endregion
    }
}
