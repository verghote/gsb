﻿namespace GSB
{
    partial class FrmConsultation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvVisites = new System.Windows.Forms.DataGridView();
            this.message = new System.Windows.Forms.Label();
            this.zoneFiche = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvPanier = new System.Windows.Forms.DataGridView();
            this.txtBilan = new System.Windows.Forms.TextBox();
            this.txtSpecialite = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtMedicament2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.txtRue = new System.Windows.Forms.TextBox();
            this.txtMedicament1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMotif = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisites)).BeginInit();
            this.zoneFiche.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPanier)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitre
            // 
            this.lblTitre.Size = new System.Drawing.Size(1904, 49);
            // 
            // dgvVisites
            // 
            this.dgvVisites.AllowUserToAddRows = false;
            this.dgvVisites.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVisites.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgvVisites.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVisites.Location = new System.Drawing.Point(4, 136);
            this.dgvVisites.Margin = new System.Windows.Forms.Padding(2);
            this.dgvVisites.Name = "dgvVisites";
            this.dgvVisites.RowTemplate.Height = 24;
            this.dgvVisites.Size = new System.Drawing.Size(570, 523);
            this.dgvVisites.TabIndex = 0;
            this.dgvVisites.SelectionChanged += new System.EventHandler(this.dgvVisites_SelectionChanged);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Segoe UI Historic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.Location = new System.Drawing.Point(2, 100);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(303, 21);
            this.message.TabIndex = 1;
            this.message.Text = "Sélectionner la visite pour afficher le détail";
            // 
            // zoneFiche
            // 
            this.zoneFiche.Controls.Add(this.label1);
            this.zoneFiche.Controls.Add(this.dgvPanier);
            this.zoneFiche.Controls.Add(this.txtBilan);
            this.zoneFiche.Controls.Add(this.txtSpecialite);
            this.zoneFiche.Controls.Add(this.label11);
            this.zoneFiche.Controls.Add(this.txtType);
            this.zoneFiche.Controls.Add(this.label10);
            this.zoneFiche.Controls.Add(this.txtEmail);
            this.zoneFiche.Controls.Add(this.txtMedicament2);
            this.zoneFiche.Controls.Add(this.label9);
            this.zoneFiche.Controls.Add(this.txtTelephone);
            this.zoneFiche.Controls.Add(this.txtRue);
            this.zoneFiche.Controls.Add(this.txtMedicament1);
            this.zoneFiche.Controls.Add(this.label3);
            this.zoneFiche.Controls.Add(this.label6);
            this.zoneFiche.Controls.Add(this.label7);
            this.zoneFiche.Controls.Add(this.label8);
            this.zoneFiche.Controls.Add(this.label5);
            this.zoneFiche.Controls.Add(this.txtMotif);
            this.zoneFiche.Controls.Add(this.label2);
            this.zoneFiche.Location = new System.Drawing.Point(590, 136);
            this.zoneFiche.Name = "zoneFiche";
            this.zoneFiche.Size = new System.Drawing.Size(1300, 583);
            this.zoneFiche.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(598, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 18);
            this.label1.TabIndex = 81;
            this.label1.Text = "Liste des médicaments distribués";
            // 
            // dgvPanier
            // 
            this.dgvPanier.BackgroundColor = System.Drawing.Color.White;
            this.dgvPanier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPanier.Location = new System.Drawing.Point(601, 39);
            this.dgvPanier.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.dgvPanier.Name = "dgvPanier";
            this.dgvPanier.RowTemplate.Height = 28;
            this.dgvPanier.Size = new System.Drawing.Size(685, 360);
            this.dgvPanier.TabIndex = 80;
            // 
            // txtBilan
            // 
            this.txtBilan.AcceptsReturn = true;
            this.txtBilan.AcceptsTab = true;
            this.txtBilan.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBilan.Location = new System.Drawing.Point(148, 311);
            this.txtBilan.Multiline = true;
            this.txtBilan.Name = "txtBilan";
            this.txtBilan.Size = new System.Drawing.Size(255, 171);
            this.txtBilan.TabIndex = 26;
            // 
            // txtSpecialite
            // 
            this.txtSpecialite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpecialite.Location = new System.Drawing.Point(148, 136);
            this.txtSpecialite.Name = "txtSpecialite";
            this.txtSpecialite.Size = new System.Drawing.Size(436, 26);
            this.txtSpecialite.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 138);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 18);
            this.label11.TabIndex = 24;
            this.label11.Text = "Spécialité";
            // 
            // txtType
            // 
            this.txtType.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtType.Location = new System.Drawing.Point(148, 102);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(255, 26);
            this.txtType.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 102);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 18);
            this.label10.TabIndex = 22;
            this.label10.Text = "Type praticien";
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(148, 70);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(255, 26);
            this.txtEmail.TabIndex = 21;
            // 
            // txtMedicament2
            // 
            this.txtMedicament2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedicament2.Location = new System.Drawing.Point(266, 272);
            this.txtMedicament2.Margin = new System.Windows.Forms.Padding(2);
            this.txtMedicament2.Name = "txtMedicament2";
            this.txtMedicament2.Size = new System.Drawing.Size(171, 26);
            this.txtMedicament2.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 275);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(223, 22);
            this.label9.TabIndex = 19;
            this.label9.Text = "Second médicament proposé";
            // 
            // txtTelephone
            // 
            this.txtTelephone.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelephone.Location = new System.Drawing.Point(148, 39);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(255, 26);
            this.txtTelephone.TabIndex = 18;
            // 
            // txtRue
            // 
            this.txtRue.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRue.Location = new System.Drawing.Point(148, 6);
            this.txtRue.Name = "txtRue";
            this.txtRue.Size = new System.Drawing.Size(255, 26);
            this.txtRue.TabIndex = 17;
            // 
            // txtMedicament1
            // 
            this.txtMedicament1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedicament1.Location = new System.Drawing.Point(266, 231);
            this.txtMedicament1.Margin = new System.Windows.Forms.Padding(2);
            this.txtMedicament1.Name = "txtMedicament1";
            this.txtMedicament1.Size = new System.Drawing.Size(171, 26);
            this.txtMedicament1.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 233);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(223, 22);
            this.label3.TabIndex = 15;
            this.label3.Text = "Premier médicament proposé";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 39);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "Téléphone";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 12);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "Rue";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 70);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 20);
            this.label8.TabIndex = 11;
            this.label8.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 311);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 18);
            this.label5.TabIndex = 7;
            this.label5.Text = "Bilan";
            // 
            // txtMotif
            // 
            this.txtMotif.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotif.Location = new System.Drawing.Point(148, 197);
            this.txtMotif.Name = "txtMotif";
            this.txtMotif.Size = new System.Drawing.Size(255, 26);
            this.txtMotif.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI Historic", 12F);
            this.label2.Location = new System.Drawing.Point(9, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 32);
            this.label2.TabIndex = 0;
            this.label2.Text = "Motif";
            // 
            // FrmConsultation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 691);
            this.Controls.Add(this.zoneFiche);
            this.Controls.Add(this.message);
            this.Controls.Add(this.dgvVisites);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmConsultation";
            this.Load += new System.EventHandler(this.FrmConsultation_Load);
            this.Controls.SetChildIndex(this.lblTitre, 0);
            this.Controls.SetChildIndex(this.dgvVisites, 0);
            this.Controls.SetChildIndex(this.message, 0);
            this.Controls.SetChildIndex(this.zoneFiche, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisites)).EndInit();
            this.zoneFiche.ResumeLayout(false);
            this.zoneFiche.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPanier)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvVisites;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Panel zoneFiche;
        private System.Windows.Forms.TextBox txtMotif;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMedicament2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.TextBox txtRue;
        private System.Windows.Forms.TextBox txtMedicament1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSpecialite;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtBilan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvPanier;
    }
}