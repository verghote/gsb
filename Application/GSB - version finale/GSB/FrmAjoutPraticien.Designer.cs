﻿namespace GSB
{
    partial class FrmAjoutPraticien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.messageEmail = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.messageTelephone = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.messageVille = new System.Windows.Forms.Label();
            this.txtVille = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.messageRue = new System.Windows.Forms.Label();
            this.txtRue = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.messagePrenom = new System.Windows.Forms.Label();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.messageNom = new System.Windows.Forms.Label();
            this.messageSpecialite = new System.Windows.Forms.Label();
            this.messageType = new System.Windows.Forms.Label();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxSpecialite = new System.Windows.Forms.ComboBox();
            this.cbxType = new System.Windows.Forms.ComboBox();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitre
            // 
            this.lblTitre.Size = new System.Drawing.Size(1000, 49);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.messageEmail);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.messageTelephone);
            this.panel1.Controls.Add(this.txtTelephone);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.messageVille);
            this.panel1.Controls.Add(this.txtVille);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.messageRue);
            this.panel1.Controls.Add(this.txtRue);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.messagePrenom);
            this.panel1.Controls.Add(this.txtPrenom);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.messageNom);
            this.panel1.Controls.Add(this.messageSpecialite);
            this.panel1.Controls.Add(this.messageType);
            this.panel1.Controls.Add(this.txtNom);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbxSpecialite);
            this.panel1.Controls.Add(this.cbxType);
            this.panel1.Controls.Add(this.btnAjouter);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1000, 534);
            this.panel1.TabIndex = 0;
            // 
            // messageEmail
            // 
            this.messageEmail.AutoSize = true;
            this.messageEmail.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageEmail.ForeColor = System.Drawing.Color.Red;
            this.messageEmail.Location = new System.Drawing.Point(30, 347);
            this.messageEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageEmail.Name = "messageEmail";
            this.messageEmail.Size = new System.Drawing.Size(39, 17);
            this.messageEmail.TabIndex = 48;
            this.messageEmail.Text = "msg";
            this.messageEmail.Visible = false;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(121, 321);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(245, 26);
            this.txtEmail.TabIndex = 47;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(29, 327);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 20);
            this.label10.TabIndex = 46;
            this.label10.Text = "Email";
            // 
            // messageTelephone
            // 
            this.messageTelephone.AutoSize = true;
            this.messageTelephone.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageTelephone.ForeColor = System.Drawing.Color.Red;
            this.messageTelephone.Location = new System.Drawing.Point(31, 291);
            this.messageTelephone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageTelephone.Name = "messageTelephone";
            this.messageTelephone.Size = new System.Drawing.Size(39, 17);
            this.messageTelephone.TabIndex = 45;
            this.messageTelephone.Text = "msg";
            this.messageTelephone.Visible = false;
            // 
            // txtTelephone
            // 
            this.txtTelephone.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelephone.Location = new System.Drawing.Point(122, 265);
            this.txtTelephone.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(245, 26);
            this.txtTelephone.TabIndex = 44;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(30, 271);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 20);
            this.label9.TabIndex = 43;
            this.label9.Text = "Téléphone";
            // 
            // messageVille
            // 
            this.messageVille.AutoSize = true;
            this.messageVille.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageVille.ForeColor = System.Drawing.Color.Red;
            this.messageVille.Location = new System.Drawing.Point(31, 228);
            this.messageVille.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageVille.Name = "messageVille";
            this.messageVille.Size = new System.Drawing.Size(39, 17);
            this.messageVille.TabIndex = 39;
            this.messageVille.Text = "msg";
            this.messageVille.Visible = false;
            // 
            // txtVille
            // 
            this.txtVille.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtVille.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtVille.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVille.Location = new System.Drawing.Point(122, 202);
            this.txtVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(245, 26);
            this.txtVille.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(30, 208);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.TabIndex = 37;
            this.label6.Text = "Ville";
            // 
            // messageRue
            // 
            this.messageRue.AutoSize = true;
            this.messageRue.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageRue.ForeColor = System.Drawing.Color.Red;
            this.messageRue.Location = new System.Drawing.Point(31, 168);
            this.messageRue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageRue.Name = "messageRue";
            this.messageRue.Size = new System.Drawing.Size(39, 17);
            this.messageRue.TabIndex = 36;
            this.messageRue.Text = "msg";
            this.messageRue.Visible = false;
            // 
            // txtRue
            // 
            this.txtRue.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRue.Location = new System.Drawing.Point(122, 142);
            this.txtRue.Margin = new System.Windows.Forms.Padding(2);
            this.txtRue.Name = "txtRue";
            this.txtRue.Size = new System.Drawing.Size(245, 26);
            this.txtRue.TabIndex = 35;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(30, 148);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 20);
            this.label7.TabIndex = 34;
            this.label7.Text = "Rue";
            // 
            // messagePrenom
            // 
            this.messagePrenom.AutoSize = true;
            this.messagePrenom.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messagePrenom.ForeColor = System.Drawing.Color.Red;
            this.messagePrenom.Location = new System.Drawing.Point(31, 110);
            this.messagePrenom.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messagePrenom.Name = "messagePrenom";
            this.messagePrenom.Size = new System.Drawing.Size(39, 17);
            this.messagePrenom.TabIndex = 33;
            this.messagePrenom.Text = "msg";
            this.messagePrenom.Visible = false;
            // 
            // txtPrenom
            // 
            this.txtPrenom.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrenom.Location = new System.Drawing.Point(122, 84);
            this.txtPrenom.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(245, 26);
            this.txtPrenom.TabIndex = 32;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 90);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 20);
            this.label5.TabIndex = 31;
            this.label5.Text = "Prénom";
            // 
            // messageNom
            // 
            this.messageNom.AutoSize = true;
            this.messageNom.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageNom.ForeColor = System.Drawing.Color.Red;
            this.messageNom.Location = new System.Drawing.Point(31, 55);
            this.messageNom.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageNom.Name = "messageNom";
            this.messageNom.Size = new System.Drawing.Size(39, 17);
            this.messageNom.TabIndex = 30;
            this.messageNom.Text = "msg";
            this.messageNom.Visible = false;
            // 
            // messageSpecialite
            // 
            this.messageSpecialite.AutoSize = true;
            this.messageSpecialite.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageSpecialite.ForeColor = System.Drawing.Color.Red;
            this.messageSpecialite.Location = new System.Drawing.Point(419, 416);
            this.messageSpecialite.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageSpecialite.Name = "messageSpecialite";
            this.messageSpecialite.Size = new System.Drawing.Size(39, 17);
            this.messageSpecialite.TabIndex = 29;
            this.messageSpecialite.Text = "msg";
            this.messageSpecialite.UseWaitCursor = true;
            this.messageSpecialite.Visible = false;
            // 
            // messageType
            // 
            this.messageType.AutoSize = true;
            this.messageType.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageType.ForeColor = System.Drawing.Color.Red;
            this.messageType.Location = new System.Drawing.Point(30, 416);
            this.messageType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageType.Name = "messageType";
            this.messageType.Size = new System.Drawing.Size(39, 17);
            this.messageType.TabIndex = 28;
            this.messageType.Text = "msg";
            this.messageType.Visible = false;
            // 
            // txtNom
            // 
            this.txtNom.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom.Location = new System.Drawing.Point(122, 28);
            this.txtNom.Margin = new System.Windows.Forms.Padding(2);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(245, 26);
            this.txtNom.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 26;
            this.label1.Text = "Nom";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(30, 387);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 20);
            this.label3.TabIndex = 23;
            this.label3.Text = "Type";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(419, 393);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 22;
            this.label2.Text = "Specialité";
            // 
            // cbxSpecialite
            // 
            this.cbxSpecialite.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxSpecialite.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxSpecialite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxSpecialite.FormattingEnabled = true;
            this.cbxSpecialite.Location = new System.Drawing.Point(540, 387);
            this.cbxSpecialite.Name = "cbxSpecialite";
            this.cbxSpecialite.Size = new System.Drawing.Size(246, 26);
            this.cbxSpecialite.TabIndex = 21;
            // 
            // cbxType
            // 
            this.cbxType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxType.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxType.FormattingEnabled = true;
            this.cbxType.Location = new System.Drawing.Point(121, 387);
            this.cbxType.Name = "cbxType";
            this.cbxType.Size = new System.Drawing.Size(246, 26);
            this.cbxType.TabIndex = 20;
            // 
            // btnAjouter
            // 
            this.btnAjouter.BackColor = System.Drawing.Color.Red;
            this.btnAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouter.Location = new System.Drawing.Point(685, 471);
            this.btnAjouter.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(182, 38);
            this.btnAjouter.TabIndex = 19;
            this.btnAjouter.Text = "Ajouter";
            this.btnAjouter.UseVisualStyleBackColor = false;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // FrmAjoutPraticien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 689);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmAjoutPraticien";
            this.Text = "Nouveau praticien";
            this.Load += new System.EventHandler(this.FrmAjoutPraticien_Load);
            this.Controls.SetChildIndex(this.lblTitre, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.Label messageNom;
        private System.Windows.Forms.Label messageSpecialite;
        private System.Windows.Forms.Label messageType;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxSpecialite;
        private System.Windows.Forms.ComboBox cbxType;
        private System.Windows.Forms.Label messageEmail;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label messageTelephone;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label messageVille;
        private System.Windows.Forms.TextBox txtVille;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label messageRue;
        private System.Windows.Forms.TextBox txtRue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label messagePrenom;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.Label label5;
    }
}