﻿namespace GSB
{
    partial class FrmBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBase));
            this.labelGsb = new System.Windows.Forms.Label();
            this.lblVisiteur = new System.Windows.Forms.Label();
            this.lblTitre = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consulterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.créerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLaPlannificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enregistrerLeBilanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerLesRendezvousToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicamentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ficheMédicamentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.médicamentsDistribuésToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visiteurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gérerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seDéconnecterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelGsb
            // 
            this.labelGsb.BackColor = System.Drawing.Color.Aqua;
            this.labelGsb.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelGsb.Font = new System.Drawing.Font("Georgia", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGsb.Location = new System.Drawing.Point(0, 237);
            this.labelGsb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelGsb.Name = "labelGsb";
            this.labelGsb.Size = new System.Drawing.Size(438, 24);
            this.labelGsb.TabIndex = 8;
            this.labelGsb.Text = "GSB - Galaxy Swiss Bourdin";
            this.labelGsb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVisiteur
            // 
            this.lblVisiteur.BackColor = System.Drawing.Color.Aqua;
            this.lblVisiteur.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblVisiteur.Font = new System.Drawing.Font("Georgia", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVisiteur.Location = new System.Drawing.Point(0, 213);
            this.lblVisiteur.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblVisiteur.Name = "lblVisiteur";
            this.lblVisiteur.Size = new System.Drawing.Size(438, 24);
            this.lblVisiteur.TabIndex = 4;
            this.lblVisiteur.Text = "Visiteur";
            this.lblVisiteur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTitre
            // 
            this.lblTitre.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitre.Font = new System.Drawing.Font("Georgia", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitre.Location = new System.Drawing.Point(0, 24);
            this.lblTitre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(438, 49);
            this.lblTitre.TabIndex = 9;
            this.lblTitre.Text = "Titre";
            this.lblTitre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.medicamentToolStripMenuItem,
            this.visiteurToolStripMenuItem,
            this.seDéconnecterToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(438, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consulterToolStripMenuItem,
            this.créerToolStripMenuItem,
            this.modifierLaPlannificationToolStripMenuItem,
            this.enregistrerLeBilanToolStripMenuItem,
            this.imprimerLesRendezvousToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.fichierToolStripMenuItem.Text = "Visite";
            // 
            // consulterToolStripMenuItem
            // 
            this.consulterToolStripMenuItem.Name = "consulterToolStripMenuItem";
            this.consulterToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.consulterToolStripMenuItem.Text = "Consulter";
            this.consulterToolStripMenuItem.Click += new System.EventHandler(this.consulterToolStripMenuItem_Click);
            // 
            // créerToolStripMenuItem
            // 
            this.créerToolStripMenuItem.Name = "créerToolStripMenuItem";
            this.créerToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.créerToolStripMenuItem.Text = "Créer un nouveau rendez-vous";
            this.créerToolStripMenuItem.Click += new System.EventHandler(this.créerToolStripMenuItem_Click);
            // 
            // modifierLaPlannificationToolStripMenuItem
            // 
            this.modifierLaPlannificationToolStripMenuItem.Name = "modifierLaPlannificationToolStripMenuItem";
            this.modifierLaPlannificationToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.modifierLaPlannificationToolStripMenuItem.Text = "Modifier la plannification";
            this.modifierLaPlannificationToolStripMenuItem.Click += new System.EventHandler(this.modifierLaPlannificationToolStripMenuItem_Click);
            // 
            // enregistrerLeBilanToolStripMenuItem
            // 
            this.enregistrerLeBilanToolStripMenuItem.Name = "enregistrerLeBilanToolStripMenuItem";
            this.enregistrerLeBilanToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.enregistrerLeBilanToolStripMenuItem.Text = "Enregistrer un bilan";
            this.enregistrerLeBilanToolStripMenuItem.Click += new System.EventHandler(this.enregistrerLeBilanToolStripMenuItem_Click);
            // 
            // imprimerLesRendezvousToolStripMenuItem
            // 
            this.imprimerLesRendezvousToolStripMenuItem.Name = "imprimerLesRendezvousToolStripMenuItem";
            this.imprimerLesRendezvousToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.imprimerLesRendezvousToolStripMenuItem.Text = "Imprimer les rendez-vous";
            this.imprimerLesRendezvousToolStripMenuItem.Click += new System.EventHandler(this.imprimerLesRendezvousToolStripMenuItem_Click);
            // 
            // medicamentToolStripMenuItem
            // 
            this.medicamentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ficheMédicamentToolStripMenuItem,
            this.médicamentsDistribuésToolStripMenuItem});
            this.medicamentToolStripMenuItem.Name = "medicamentToolStripMenuItem";
            this.medicamentToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.medicamentToolStripMenuItem.Text = "Médicament";
            // 
            // ficheMédicamentToolStripMenuItem
            // 
            this.ficheMédicamentToolStripMenuItem.Name = "ficheMédicamentToolStripMenuItem";
            this.ficheMédicamentToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.ficheMédicamentToolStripMenuItem.Text = "Fiche médicament";
            this.ficheMédicamentToolStripMenuItem.Click += new System.EventHandler(this.ficheMédicamentToolStripMenuItem_Click);
            // 
            // médicamentsDistribuésToolStripMenuItem
            // 
            this.médicamentsDistribuésToolStripMenuItem.Name = "médicamentsDistribuésToolStripMenuItem";
            this.médicamentsDistribuésToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.médicamentsDistribuésToolStripMenuItem.Text = "Médicaments distribués";
            this.médicamentsDistribuésToolStripMenuItem.Click += new System.EventHandler(this.médicamentsDistribuésToolStripMenuItem_Click);
            // 
            // visiteurToolStripMenuItem
            // 
            this.visiteurToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeToolStripMenuItem,
            this.ajouterToolStripMenuItem,
            this.gérerToolStripMenuItem});
            this.visiteurToolStripMenuItem.Name = "visiteurToolStripMenuItem";
            this.visiteurToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.visiteurToolStripMenuItem.Text = "Praticien";
            // 
            // listeToolStripMenuItem
            // 
            this.listeToolStripMenuItem.Name = "listeToolStripMenuItem";
            this.listeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.listeToolStripMenuItem.Text = "Liste";
            this.listeToolStripMenuItem.Click += new System.EventHandler(this.listeToolStripMenuItem_Click);
            // 
            // ajouterToolStripMenuItem
            // 
            this.ajouterToolStripMenuItem.Name = "ajouterToolStripMenuItem";
            this.ajouterToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ajouterToolStripMenuItem.Text = "Ajouter";
            this.ajouterToolStripMenuItem.Click += new System.EventHandler(this.ajouterToolStripMenuItem_Click);
            // 
            // gérerToolStripMenuItem
            // 
            this.gérerToolStripMenuItem.Name = "gérerToolStripMenuItem";
            this.gérerToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.gérerToolStripMenuItem.Text = "Modifier";
            this.gérerToolStripMenuItem.Click += new System.EventHandler(this.gérerToolStripMenuItem_Click);
            // 
            // seDéconnecterToolStripMenuItem
            // 
            this.seDéconnecterToolStripMenuItem.Name = "seDéconnecterToolStripMenuItem";
            this.seDéconnecterToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.seDéconnecterToolStripMenuItem.Text = "Se déconnecter";
            this.seDéconnecterToolStripMenuItem.Click += new System.EventHandler(this.seDéconnecterToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // FrmBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(438, 261);
            this.Controls.Add(this.lblTitre);
            this.Controls.Add(this.lblVisiteur);
            this.Controls.Add(this.labelGsb);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmBase";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmBase_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelGsb;
        private System.Windows.Forms.Label lblVisiteur;
        protected System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consulterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem créerToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem modifierLaPlannificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enregistrerLeBilanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seDéconnecterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimerLesRendezvousToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medicamentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ficheMédicamentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visiteurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gérerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem médicamentsDistribuésToolStripMenuItem;
    }
}