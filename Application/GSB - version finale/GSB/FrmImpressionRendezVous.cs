﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmImpressionRendezVous : FrmBase
    {
        private List<Visite> lesVisites;

        public FrmImpressionRendezVous()
        {
            InitializeComponent();
        }

        #region procédures événementielles
        private void FrmImpressionRendezVous_Load(object sender, EventArgs e)
        {
            lesVisites = Globale.db.LesVisites.FindAll(element => element.Date >= DateTime.Today);

            if (lesVisites.Count > 0)
            {
                parametrerComposant();
            }
            else
            {
                message.Text = "Aucun rendez-vous planifié pour le moment.";
                zoneSaisie.Visible = false;
            }
        }

        private void dtpFin_ValueChanged(object sender, EventArgs e)
        {
            messageIntervale.Visible = false;
        }
             

        private void dtpDebut_ValueChanged(object sender, EventArgs e)
        {
            dtpFin.MinDate = dtpDebut.Value.AddDays(7);
            messageIntervale.Visible = false;
        }

        private void imgImprimer_Click(object sender, EventArgs e)
        {
            if (lesVisites.Exists(element => element.Date >= dtpDebut.Value && element.Date <= dtpFin.Value))
            {
                messageIntervale.Visible = false;
                printRendezVous.DocumentName = "Rendez-vous";
                // sélection de l'imprimante
                choixImprimante.Document = printRendezVous;
                DialogResult result = choixImprimante.ShowDialog();
                if (result == DialogResult.OK)
                {
                    printRendezVous.Print();
                }
            } else
            {
                messageIntervale.Text = "Aucun rendez-vous planifié sur cette période.";
                messageIntervale.Visible = true;
            }
        }

        private void imgApercu_Click(object sender, EventArgs e)
        {
            if (lesVisites.Exists(element => element.Date >= dtpDebut.Value && element.Date <= dtpFin.Value))
            {
                messageIntervale.Visible = false;

                printRendezVous.DocumentName = "Rendez-vous";
                printRendezVous.DefaultPageSettings.Landscape = true;
                // sélection de l'imprimante
                // choixImprimante.Document = printRendezVous;
                // DialogResult result = choixImprimante.ShowDialog();
                // if (result == DialogResult.OK)
                // {
                apercuRendezVous.Document = printRendezVous;
                apercuRendezVous.WindowState = FormWindowState.Maximized;
                apercuRendezVous.ShowDialog();
                // }

            }
            else
            {
                messageIntervale.Text = "Aucun rendez-vous planifié sur cette période.";
                messageIntervale.Visible = true;
            }
        }


        private void printRendezVous_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            // définition de la police, de la couleur et du format
            Font unePolice = new Font("Arial", 8);
            SolidBrush uneCouleur = new SolidBrush(Color.Black);
            StringFormat unFormat = new StringFormat();
            unFormat.LineAlignment = StringAlignment.Center;

            // imprimer l'encadrement du titre
            int x = 50;
            int y = 30;
            int largeur = 1000;
            int hauteur = 30;
            Rectangle unRectangle = new Rectangle(x, y, largeur, hauteur);
            Pen styleTrait = new Pen(Color.Black, 1);
            e.Graphics.DrawRectangle(styleTrait, unRectangle);

            // imprimer le titre centré dans l'encadrement 
            String texteAImprimer = "Mes rendez-vous entre le " + dtpDebut.Value.ToLongDateString() + " et le " + dtpFin.Value.ToLongDateString();
            unFormat.Alignment = StringAlignment.Center;
            e.Graphics.DrawString(texteAImprimer, unePolice, uneCouleur, unRectangle, unFormat);

            // impression de l'entête du tableau
            // Chaque colonne est associée à un rectangle pour permettre un alignement de son contenu

            // colonne Référence
            x = 50;
            y = 70;
            largeur = 170;
            hauteur = 25;
            texteAImprimer = "Date";
            unRectangle = new Rectangle(x, y, largeur, hauteur);
            unFormat.Alignment = StringAlignment.Near;
            e.Graphics.DrawString(texteAImprimer, unePolice, uneCouleur, unRectangle, unFormat);

            // colonne Heure
            x += largeur;
            largeur = 80;
            texteAImprimer = "Heure";
            unFormat.Alignment = StringAlignment.Center;
            unRectangle = new Rectangle(x, y, largeur, hauteur);
            e.Graphics.DrawString(texteAImprimer, unePolice, uneCouleur, unRectangle, unFormat);

            // colonne Praticien
            x += largeur;
            largeur = 200;
            texteAImprimer = "Praticien";
            unFormat.Alignment = StringAlignment.Near;
            unRectangle = new Rectangle(x, y, largeur, hauteur);
            e.Graphics.DrawString(texteAImprimer, unePolice, uneCouleur, unRectangle, unFormat);

            // colonne Téléphone
            x += largeur;
            largeur = 120;
            texteAImprimer = "Téléphone";
            unFormat.Alignment = StringAlignment.Near;
            unRectangle = new Rectangle(x, y, largeur, hauteur);
            e.Graphics.DrawString(texteAImprimer, unePolice, uneCouleur, unRectangle, unFormat);
            
            // colonne Lieu
            x += largeur;
            largeur = 250;
            texteAImprimer = "Lieu";
            unFormat.Alignment = StringAlignment.Near;
            unRectangle = new Rectangle(x, y, largeur, hauteur);
            e.Graphics.DrawString(texteAImprimer, unePolice, uneCouleur, unRectangle, unFormat);

            // colonne Motif
            x += largeur;
            largeur = 250;
            texteAImprimer = "Motif";
            unFormat.Alignment = StringAlignment.Near;
            unRectangle = new Rectangle(x, y, largeur, hauteur);
            e.Graphics.DrawString(texteAImprimer, unePolice, uneCouleur, unRectangle, unFormat);


            // Impression du trait séparant l'entête du tableau du corps
            styleTrait = new Pen(Color.Black, 2);
            Point unPoint = new Point(50, 100);
            Point point2 = new Point(1070, 100);
            e.Graphics.DrawLine(styleTrait, unPoint, point2);

            // Impression des lignes à partir de la lecture des visites
            unPoint += new Size(0, 15);
            foreach (Visite uneVisite in lesVisites.FindAll(element => element.Date >= dtpDebut.Value && element.Date <= dtpFin.Value)) 
            {
                
                // La date
                largeur = 170;
                unRectangle = new Rectangle(unPoint.X, unPoint.Y, largeur, hauteur);
                unFormat.Alignment = StringAlignment.Near;
                e.Graphics.DrawString(uneVisite.Date.ToLongDateString(), unePolice, uneCouleur, unRectangle, unFormat);

                // L'horaire
                unPoint += new Size(largeur, 0);
                largeur = 80;
                unRectangle = new Rectangle(unPoint.X, unPoint.Y, largeur, hauteur);
                unFormat.Alignment = StringAlignment.Center;
                e.Graphics.DrawString(uneVisite.Heure, unePolice, uneCouleur, unRectangle, unFormat);

                // Le praticien
                unPoint += new Size(largeur, 0);
                largeur = 200;
                unRectangle = new Rectangle(unPoint.X, unPoint.Y, largeur, hauteur);
                unFormat.Alignment = StringAlignment.Near;
                e.Graphics.DrawString(uneVisite.LePraticien.NomPrenom, unePolice, uneCouleur, unRectangle, unFormat);


                // Le téléphone
                unPoint += new Size(largeur, 0);
                largeur = 120;
                unRectangle = new Rectangle(unPoint.X, unPoint.Y, largeur, hauteur);
                unFormat.Alignment = StringAlignment.Near;
                e.Graphics.DrawString(uneVisite.LePraticien.Telephone, unePolice, uneCouleur, unRectangle, unFormat);


                // Le lieu
                unPoint += new Size(largeur, 0);
                largeur = 250;
                unRectangle = new Rectangle(unPoint.X, unPoint.Y, largeur, hauteur);
                unFormat.Alignment = StringAlignment.Near;
                e.Graphics.DrawString(uneVisite.LePraticien.Ville + " " + uneVisite.LePraticien.Rue, unePolice, uneCouleur, unRectangle, unFormat);

                // Le motif
                unPoint += new Size(largeur, 0);
                largeur = 250;
                unRectangle = new Rectangle(unPoint.X, unPoint.Y, largeur, hauteur);
                unFormat.Alignment = StringAlignment.Near;
                e.Graphics.DrawString(uneVisite.LeMotif.Libelle, unePolice, uneCouleur, unRectangle, unFormat);


                // on se place au début de la ligne suivante 
                unPoint.X = 50;
                unPoint.Y += hauteur;
            }
        }

        #endregion

        #region méthodes
        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Impression des rendez-vous sur une période";
            #endregion

            #region paramètrage de l'intervalle
            dtpDebut.MinDate = DateTime.Today;
            dtpDebut.MaxDate = DateTime.Today.AddDays(53);

            dtpFin.MinDate = DateTime.Today.AddDays(7);
            dtpFin.MaxDate = DateTime.Today.AddDays(60);
            #endregion
        }

        #endregion
    }
} 

