﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public class Donnee
    {
       
        // constructeur
        public Donnee()
        {
            LesMedicaments = new List<Medicament>();
            LesPraticiens = new List<Praticien>();
            LesMotifs = new List<Motif>();
            LesFamilles = new SortedDictionary<string, Famille>();
            LesVisites = new List<Visite>();
            LesTypes = new List<TypePraticien>();
            LesSpecialites = new List<Specialite>();
            LesVilles = new List<Ville>();
            LesDistributions = new SortedDictionary<string, int>();
        }

        #region propriétés

        public SortedDictionary<string, Famille> LesFamilles { get; }
        public List<Medicament> LesMedicaments { get; }
        public List<Praticien> LesPraticiens { get; }
        public List<Motif> LesMotifs { get; }
        public List<Visite> LesVisites { get; }
        public List<TypePraticien> LesTypes { get; }
        public List<Specialite> LesSpecialites { get; }
        public List<Ville> LesVilles { get; }
        public SortedDictionary<string, int> LesDistributions { get; }
        #endregion

        #region méthode 

        

        
        #endregion
          
    }
}
