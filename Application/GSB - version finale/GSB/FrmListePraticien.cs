﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmListePraticien : FrmBase
    {
        private Praticien lePraticien;

        public FrmListePraticien()
        {
            InitializeComponent();
        }

        #region procédures événementielles
        private void FrmListePraticien_Load(object sender, EventArgs e)
        {
            parametrerComposant();
            remplirDgvPraticiens();
        }

        private void dgvPraticiens_SelectionChanged(object sender, EventArgs e)
        {
            lePraticien = (Praticien)dgvPraticiens.SelectedRows[0].Cells[0].Value;
            txtEmail.Text = lePraticien.Email;
            txtRue.Text = lePraticien.Rue;
            txtTelephone.Text = lePraticien.Telephone;
            txtType.Text = lePraticien.Type.Libelle;
            txtSpecialite.Text = lePraticien.Specialite == null ? string.Empty : lePraticien.Specialite.Libelle;
        }

        #endregion

        #region méthodes
        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Liste des praticiens";
            #endregion

            #region paramétrage dgvPraticiens

            // Police de caractères
            dgvPraticiens.DefaultCellStyle.Font = new Font("Georgia", 11);

            // hauteur des lignes
            dgvPraticiens.RowTemplate.Height = 30;

            // style pour l'entête
            DataGridViewCellStyle style = dgvPraticiens.ColumnHeadersDefaultCellStyle;
            style.BackColor = Color.Aquamarine;
            style.ForeColor = Color.White;
            style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            style.Font = new Font("Georgia", 12, FontStyle.Bold);

            // définition du mode de sélection des lignes
            dgvPraticiens.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            // la ligne d'entête est visible
            dgvPraticiens.ColumnHeadersVisible = true;
            // La colonne d'entête est visible
            dgvPraticiens.RowHeadersVisible = true;
            // l'utilisateur ne peut ni ajouter ni supprimer des lignes
            dgvPraticiens.AllowUserToDeleteRows = false;
            dgvPraticiens.AllowUserToAddRows = false;

            // définition des colonnes : objet praticien, nom et prenom, ville, dernière visite 
            dgvPraticiens.ColumnCount = 4;
            // dgvPraticiens.Width = 880;

            // les colonnes vont ajuster leur taille au contenu
            dgvPraticiens.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            dgvPraticiens.Columns[0].Visible = false;

            dgvPraticiens.Columns[1].Name = "Nom et prénom";
            dgvPraticiens.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvPraticiens.Columns[2].Name = "Ville";
            dgvPraticiens.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvPraticiens.Columns[3].Name = "Dernière visite";
            dgvPraticiens.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            #endregion
        }

        private void remplirDgvPraticiens()
        {
            List<Praticien> lesPraticiens = Globale.db.LesPraticiens;
            int ligne = 0;
            bool surligner;
            foreach (Praticien unPraticien in lesPraticiens)
            {
                
                // recherche de la date de la dernière visite
                List<Visite> lesVisites = Globale.db.LesVisites.FindAll(element => element.LePraticien == unPraticien);
                string dateDerniereVisite;
                if (lesVisites.Count == 0)
                {
                    dateDerniereVisite = "Aucune visite pour le moment";
                    surligner = true;
                }
                else
                {
                    DateTime laDate = lesVisites[lesVisites.Count - 1].Date;
                    surligner = (DateTime.Today - laDate).Days > 56;
                    dateDerniereVisite = laDate.ToLongDateString();
                    
                }
                dgvPraticiens.Rows.Add(unPraticien, unPraticien.NomPrenom, unPraticien.Ville, dateDerniereVisite);
                if (surligner)
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        this.dgvPraticiens.Rows[ligne].Cells[i].Style.BackColor = Color.Red;
                    }
                }
                
                ligne++;
            }
        }

        #endregion

    }
}
