﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmModificationPraticien : FrmBase
    {

        private Praticien lePraticien;

        public FrmModificationPraticien()
        {
            InitializeComponent();
        }


        #region procédures événementielles

        private void FrmModificationPraticien_Load(object sender, EventArgs e)
        {
            parametrerComposant();
        }

        private void cbxPraticien_SelectedIndexChanged(object sender, EventArgs e)
        {
            lePraticien = (Praticien)cbxPraticien.SelectedItem;
            remplirPraticien();
        }

        
        private void btnModifier_Click(object sender, EventArgs e)
        {
            modification();
        }

        #endregion

        #region méthodes
        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Modifier les coordonnées d'un praticien";
            #endregion

            #region paramètrage des zones de liste

            

                   
            // alimentation de la zone de liste déroulante contenant les types de praticiens
            cbxType.DataSource = Globale.db.LesTypes;
            cbxType.DisplayMember = "Libelle";
            cbxType.ValueMember = "Id";
            cbxType.SelectedIndex = -1;

            // alimentation de la zone de liste déroulante contenant les spécialités
            cbxSpecialite.DataSource = Globale.db.LesSpecialites;
            cbxSpecialite.DisplayMember = "Libelle";
            cbxSpecialite.ValueMember = "Id";
            cbxSpecialite.SelectedItem = null;
            #endregion

            #region Paramètrage auto complétion sur la ville
            // paramétrage de l'heure
            txtVille.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtVille.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var source = new AutoCompleteStringCollection();
            foreach (Ville uneVille in Globale.db.LesVilles)
            {
                source.Add(uneVille.Nom);
            }
            txtVille.AutoCompleteCustomSource = source;


            // alimentation de la zone de liste déroulante contenant les praticiens
            // pb : déclenche l'événement SelectedIndexChanged : il faut donc ajouter l'événement après 
            // https://stackoverflow.com/questions/14111879/how-to-prevent-selectedindexchanged-event-when-datasource-is-bound
            cbxPraticien.DataSource = Globale.db.LesPraticiens;
            cbxPraticien.DisplayMember = "NomPrenom";
            cbxPraticien.ValueMember = "Id";
            cbxPraticien.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbxPraticien.AutoCompleteSource = AutoCompleteSource.ListItems;
            this.cbxPraticien.SelectedIndexChanged += new System.EventHandler(this.cbxPraticien_SelectedIndexChanged);
            cbxPraticien.SelectedIndex = 0;
            lePraticien = Globale.db.LesPraticiens[0];
            remplirPraticien();

            #endregion
        }


        private void remplirPraticien()
        {
            btnModifier.Enabled = true;
            if (lePraticien.Specialite == null)
                cbxSpecialite.SelectedItem = null;
            else
                cbxSpecialite.SelectedItem = lePraticien.Specialite;
            cbxType.SelectedItem = lePraticien.Type;
            txtNom.Text = lePraticien.Nom;
            txtPrenom.Text = lePraticien.Prenom;
            txtRue.Text = lePraticien.Rue;
            txtVille.Text = lePraticien.Ville;
            txtTelephone.Text = lePraticien.Telephone;
            txtEmail.Text = lePraticien.Email;
        }


        private void modification()
        {
            // pas de contrôle sur les zone de liste qui peuvent rester vide
            bool nomOk = controlerNom();
            bool prenomOk = controlerPrenom();
            bool rueOk = controlerRue();
            bool villeOk = controlerVille();
            bool emailOk = controlerEmail();
            bool telephoneOk = controlerTelephone();
            bool typeOk = controlerType();
            if (nomOk && prenomOk && rueOk && villeOk && emailOk && telephoneOk && typeOk)
            {
                modifier();
            }
        }

        private bool controlerNom()
        {
            if (txtNom.Text == string.Empty)
            {
                messageNom.Text = "Le nom du praticien doit être précisé";
                messageNom.Visible = true;
                return false;
            }
            messageNom.Text = "";
            messageNom.Visible = false;
            return true;
        }

        private bool controlerPrenom()
        {
            if (txtPrenom.Text == string.Empty)
            {
                messagePrenom.Text = "Le prénom du praticien doit être précisé";
                messagePrenom.Visible = true;
                return false;
            }
            messagePrenom.Text = "";
            messagePrenom.Visible = false;
            return true;
        }

        private bool controlerRue()
        {
            if (txtRue.Text == string.Empty)
            {
                messageRue.Text = "La rue du praticien doit être précisée";
                messageRue.Visible = true;
                return false;
            }
            messageRue.Text = "";
            messageRue.Visible = false;
            return true;
        }

        private bool controlerVille()
        {
            if (txtVille.Text == string.Empty)
            {
                messageVille.Text = "La ville du praticien doit être précisée";
                messageVille.Visible = true;
                return false;
            }
            Ville uneVille = Globale.db.LesVilles.Find(element => element.Nom == txtVille.Text);
            if (uneVille == null)
            {
                messageVille.Text = "Vous devez sélectionner une ville dans la liste";
                messageVille.Visible = true;
                return false;
            }
            messageVille.Text = "";
            messageVille.Visible = false;
            return true;
        }

        private bool controlerEmail()
        {
            if (txtEmail.Text == string.Empty)
            {
                messageEmail.Text = "L'adresse mail du praticien doit être précisée";
                messageEmail.Visible = true;
                return false;
            }
            Regex uneExpression = new Regex(@"^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*[.][a-zA-Z]{2,3}$");
            if (!uneExpression.IsMatch(txtEmail.Text))
            {
                messageEmail.Text = "L'adresse mail n'est pas valide";
                messageEmail.Visible = true;
                return false;
            }

            messageEmail.Text = "";
            messageEmail.Visible = false;
            return true;
        }

        private bool controlerTelephone()
        {
            if (txtTelephone.Text == string.Empty)
            {
                messageTelephone.Text = "Le téléphone du praticien doit être précisée";
                messageTelephone.Visible = true;
                return false;
            }

            Regex uneExpression = new Regex(@"^0[1-9]([\s.-]*\d{2}){4}$");
            if (!uneExpression.IsMatch(txtTelephone.Text))
            {
                messageTelephone.Text = "Le numéro de téléphone n'est pas valide";
                messageTelephone.Visible = true;
                return false;
            }

            messageTelephone.Text = "";
            messageTelephone.Visible = false;
            return true;
        }

        private bool controlerType()
        {
            if (cbxType.SelectedIndex == -1)
            {
                messageType.Text = "Veuillez sélectionner le type de praticien.";
                messageType.Visible = true;
                return false;
            }
            messageType.Text = "";
            messageType.Visible = false;
            return true;
        }

        private void modifier()
        {
            int id = lePraticien.Id;
            string nom = txtNom.Text;
            string prenom = txtPrenom.Text;
            string rue = txtRue.Text;
            string ville = txtVille.Text;
            Ville uneVille = Globale.db.LesVilles.Find(element => element.Nom == txtVille.Text);
            string cp = uneVille.Code;
            string email = txtEmail.Text;
            string telephone = txtTelephone.Text;
            // mise en forme du téléphone : on retire tous les espaces éventuels et on ajoute un espace tous les deux chiffres
            // telephone  = telephone.Replace(" ", string.Empty);
            telephone = Regex.Replace(telephone, @"\s+", "");
            telephone = telephone.Substring(0, 2) + ' ' + telephone.Substring(2, 2) + ' ' + telephone.Substring(4, 2) + ' ' + telephone.Substring(6, 2) + ' ' + telephone.Substring(8);
            TypePraticien unType = (TypePraticien) cbxType.SelectedItem;
            string idType = unType.Id;
            Specialite laSpecialite = null;
            String idSpecialite = null;
            if (cbxSpecialite.SelectedIndex >= 0)
            {
                laSpecialite = (Specialite)cbxSpecialite.SelectedItem;
                idSpecialite = laSpecialite.Id;
            }


            string message;
            bool ok = Passerelle.modifierPraticien(id, nom, prenom, rue, cp, ville, telephone, email, idType, idSpecialite, out message);
            if (!ok)
            {
                MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                // mise à jour des données en mémoire : l'objet lePraticien
                lePraticien.Nom = nom;
                lePraticien.Prenom = prenom;
                lePraticien.Rue = rue;
                lePraticien.Ville = ville;
                lePraticien.CodePostal = cp;
                lePraticien.Email = email;
                lePraticien.Telephone = telephone;
                lePraticien.Type = unType;
                if (laSpecialite != null) 
                  lePraticien.Specialite = laSpecialite;

                Globale.db.LesPraticiens.Sort();

                // mise à jour de l'interface
                MessageBox.Show("Praticien modifié", "", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }






        #endregion


    }
}
