﻿// ------------------------------------------
// Nom du fichier : global.cs
// Objet : classe statique Globale regroupant les données globales de l'application
// Auteur : M. Verghote
// Date mise à jour : 03/03/2019
// ------------------------------------------

using lesClasses;
using System.Windows.Forms;

namespace GSB
{
    class Globale
    {
        // données globale de l'application
        // db : les données en mémoire
        // leVisiteur : l'objet visiteur connecté
        // FormulairePArent : Pointe le formulaire de connexion afin de pouvoir le fermer quand l'utilisateur quitte l'application par le menu
        public static Donnee db;
        public static Visiteur leVisiteur = null;
        public const string TITRE = "Laboratoire pharmaceutique Galaxy-Swiss Bourdin - Gestion des visites";
        public static Form FormulaireParent { get; set; }


    }
}
