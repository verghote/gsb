﻿// ------------------------------------------
// Nom du fichier : FrmConnexion.cs
// Objet : Formulaire de connexion
// Auteur : M. Verghote
// Date mise à jour : 01/05/2019
// ------------------------------------------

using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace GSB
{
    public partial class FrmConnexion : Form
    {
        public FrmConnexion()
        {
            InitializeComponent();
        }

        #region procédures événementielles

        private void FrmConnexion_Load(object sender, EventArgs e)
        {
            parametrerComposant();
            txtMdp.Text = "19910826";
            txtNom.Text = "andre";
        }

        private void btnConnecter_Click(object sender, EventArgs e)
        {
            bool nomOk = controlerNom();
            bool mdpOk = controlerMdp();
            if (nomOk && mdpOk)
            {
                string nom = txtNom.Text;
                string mdp = txtMdp.Text;

                int day = int.Parse(mdp.Substring(6, 2));
                int month = int.Parse(mdp.Substring(4, 2));
                int year = int.Parse(mdp.Substring(0, 4));
                mdp = year + "-" + month + "-" + day;

                string message;
                bool ok = Passerelle.getVisiteur(nom, mdp, out Globale.leVisiteur, out message);
                if (ok)
                {
                    // chargement des données
                    Globale.db = Passerelle.chargerDonnees(Globale.leVisiteur.Id);
                    Globale.FormulaireParent = this;
                    FrmMenu unFrmMenu = new FrmMenu();
                    this.Hide();
                    unFrmMenu.Show();
                }
                else
                {
                    messageMdp.Text = message;
                    messageMdp.Visible = true;
                    // MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region méthodes

        private void parametrerComposant()
        {
            #region paramètrage de la fenêtre
            this.Text = Globale.TITRE;
            this.ControlBox = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            #endregion

            #region paramétrage de l'auto-completion 
            txtNom.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtNom.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var source = new AutoCompleteStringCollection();
            string message;
            List<string> lesNoms;
            if (Passerelle.getLesVisiteurs(out lesNoms, out message))
            {
                source.AddRange(lesNoms.ToArray());
                txtNom.AutoCompleteCustomSource = source;
            }
            else
            {
                MessageBox.Show(message, "L'application ne peut être lancée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
            #endregion
        }

        private bool controlerNom()
        {
            string nom = txtNom.Text.Trim();
            if (nom == string.Empty)
            {
                messageNom.Text = "Champ obligatoire";
                messageNom.Visible = true;
                return false;
            }
            txtNom.Text = nom;
            messageNom.Text = "";
            messageNom.Visible = false;
            return true;
        }

        private bool controlerMdp()
        {
            string mdp = txtMdp.Text.Trim();
            if (mdp == string.Empty)
            {
                messageMdp.Text = "Champ obligatoire";
                messageMdp.Visible = true;
                return false;
            }
            Regex uneExpression = new Regex(@"^[0-9]{8}$");
            if (!uneExpression.IsMatch(mdp))
            {
                messageMdp.Text = "Format invalide";
                messageMdp.Visible = true;
                return false;
            }
            txtMdp.Text = mdp;
            messageMdp.Text = "";
            messageMdp.Visible = false;
            return true;
        }
        #endregion
    }
}
