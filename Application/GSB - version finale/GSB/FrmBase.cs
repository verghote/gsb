﻿// ------------------------------------------
// Nom du fichier : FrmBase.cs
// Objet : Formulaire de base hérité par tous les formulaire afin de posséder la même ergonomie
// Auteur : M. Verghote
// Date mise à jour : 01/04/2019
// ------------------------------------------

using System;
using System.Windows.Forms;

namespace GSB
{
    public partial class FrmBase : Form
    {
        public FrmBase()
        {
            InitializeComponent();
        }

        #region procédures événementielles
        private void FrmBase_Load(object sender, EventArgs e)
        {

            if (DesignMode) return;
            lblVisiteur.Text = "Visiteur : " + Globale.leVisiteur.NomPrenom;
            this.WindowState = FormWindowState.Maximized;
            modifierLaPlannificationToolStripMenuItem.Enabled = Globale.db.LesVisites.FindAll(element => element.Date >= DateTime.Today).Count > 0;
            enregistrerLeBilanToolStripMenuItem.Enabled = Globale.db.LesVisites.FindAll(element => element.Date <= DateTime.Today && element.Bilan == null).Count > 0;
        }

        private void consulterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultation unFrmVoirVisite = new FrmConsultation();
            this.Close();
            unFrmVoirVisite.Show();
        }
             
        private void créerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAjout unFrmSaisieVisite = new FrmAjout();
            this.Close();
            unFrmSaisieVisite.Show();
        }

        private void modifierLaPlannificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmModification unFrmModification = new FrmModification();
            this.Close();
            unFrmModification.Show();
        }

        private void enregistrerLeBilanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmBilan unFrmBilan = new FrmBilan();
            this.Close();
            unFrmBilan.Show();
        }

        private void seDéconnecterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Globale.FormulaireParent.Show();
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Globale.FormulaireParent.Close();
        }

        private void imprimerLesRendezvousToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImpressionRendezVous unFrmImpressionRendezVous = new FrmImpressionRendezVous();
            this.Close();
            unFrmImpressionRendezVous.Show();
        }

        private void ficheMédicamentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMedicament unFrmMedicament = new FrmMedicament();
            this.Close();
            unFrmMedicament.Show();
        }

        private void ajouterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAjoutPraticien unFrmAjoutPraticien = new FrmAjoutPraticien();
            this.Close();
            unFrmAjoutPraticien.Show();
        }

        private void listeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmListePraticien unFrmListePraticien = new FrmListePraticien();
            this.Close();
            unFrmListePraticien.Show();
        }


        #endregion

        private void médicamentsDistribuésToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMedicamentDistribue unFrmMedicamentDistribue = new FrmMedicamentDistribue();
            this.Close();
            unFrmMedicamentDistribue.Show();
        }

        private void gérerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmModificationPraticien unFrmModificationPraticien = new FrmModificationPraticien();
            this.Close();
            unFrmModificationPraticien.Show();
        }
    }
}
