﻿namespace GSB
{
    partial class FrmBilan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.zoneSelection = new System.Windows.Forms.Panel();
            this.cbxVisite = new System.Windows.Forms.ComboBox();
            this.message = new System.Windows.Forms.Label();
            this.zoneSaisie = new System.Windows.Forms.Panel();
            this.txtIdVisite = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.messageMedicamentDistribue = new System.Windows.Forms.Label();
            this.checkMedicament = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.txtHeure = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSpecialite = new System.Windows.Forms.TextBox();
            this.lblSpecialite = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPraticien = new System.Windows.Forms.TextBox();
            this.txtMotif = new System.Windows.Forms.TextBox();
            this.txtVille = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dgvPanier = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.lblQuantite = new System.Windows.Forms.Label();
            this.cptQuantite = new System.Windows.Forms.NumericUpDown();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.txtMedicament = new System.Windows.Forms.TextBox();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.txtBilan = new System.Windows.Forms.TextBox();
            this.messageSecondMedicament = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.messageBilan = new System.Windows.Forms.Label();
            this.messagePremierMedicament = new System.Windows.Forms.Label();
            this.cbxSecondMedicament = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxPremierMedicament = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.zoneSelection.SuspendLayout();
            this.zoneSaisie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPanier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cptQuantite)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitre
            // 
            this.lblTitre.Size = new System.Drawing.Size(1561, 49);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-229, 19);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Famille";
            // 
            // zoneSelection
            // 
            this.zoneSelection.Controls.Add(this.cbxVisite);
            this.zoneSelection.Controls.Add(this.message);
            this.zoneSelection.Dock = System.Windows.Forms.DockStyle.Top;
            this.zoneSelection.Location = new System.Drawing.Point(0, 73);
            this.zoneSelection.Name = "zoneSelection";
            this.zoneSelection.Size = new System.Drawing.Size(1561, 86);
            this.zoneSelection.TabIndex = 1;
            // 
            // cbxVisite
            // 
            this.cbxVisite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxVisite.FormattingEnabled = true;
            this.cbxVisite.Location = new System.Drawing.Point(301, 21);
            this.cbxVisite.Name = "cbxVisite";
            this.cbxVisite.Size = new System.Drawing.Size(260, 26);
            this.cbxVisite.TabIndex = 53;
            // l'événement sur changement doit être ajouté après la mise à jour de la propriété DataSource 
            // this.cbxVisite.SelectedIndexChanged += new System.EventHandler(this.cbxVisite_SelectedIndexChanged);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.Location = new System.Drawing.Point(33, 24);
            this.message.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(237, 18);
            this.message.TabIndex = 44;
            this.message.Text = "Sélection de la visite à compléter";
            // 
            // zoneSaisie
            // 
            this.zoneSaisie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zoneSaisie.Controls.Add(this.btnEnregistrer);
            this.zoneSaisie.Controls.Add(this.txtIdVisite);
            this.zoneSaisie.Controls.Add(this.label15);
            this.zoneSaisie.Controls.Add(this.messageMedicamentDistribue);
            this.zoneSaisie.Controls.Add(this.checkMedicament);
            this.zoneSaisie.Controls.Add(this.label11);
            this.zoneSaisie.Controls.Add(this.label6);
            this.zoneSaisie.Controls.Add(this.txtDate);
            this.zoneSaisie.Controls.Add(this.txtHeure);
            this.zoneSaisie.Controls.Add(this.label16);
            this.zoneSaisie.Controls.Add(this.label17);
            this.zoneSaisie.Controls.Add(this.txtSpecialite);
            this.zoneSaisie.Controls.Add(this.lblSpecialite);
            this.zoneSaisie.Controls.Add(this.txtType);
            this.zoneSaisie.Controls.Add(this.label10);
            this.zoneSaisie.Controls.Add(this.txtPraticien);
            this.zoneSaisie.Controls.Add(this.txtMotif);
            this.zoneSaisie.Controls.Add(this.txtVille);
            this.zoneSaisie.Controls.Add(this.label12);
            this.zoneSaisie.Controls.Add(this.label13);
            this.zoneSaisie.Controls.Add(this.label14);
            this.zoneSaisie.Controls.Add(this.label7);
            this.zoneSaisie.Controls.Add(this.label9);
            this.zoneSaisie.Controls.Add(this.dgvPanier);
            this.zoneSaisie.Controls.Add(this.label8);
            this.zoneSaisie.Controls.Add(this.lblQuantite);
            this.zoneSaisie.Controls.Add(this.cptQuantite);
            this.zoneSaisie.Controls.Add(this.btnAjouter);
            this.zoneSaisie.Controls.Add(this.txtMedicament);
            this.zoneSaisie.Controls.Add(this.txtBilan);
            this.zoneSaisie.Controls.Add(this.messageSecondMedicament);
            this.zoneSaisie.Controls.Add(this.label3);
            this.zoneSaisie.Controls.Add(this.messageBilan);
            this.zoneSaisie.Controls.Add(this.messagePremierMedicament);
            this.zoneSaisie.Controls.Add(this.cbxSecondMedicament);
            this.zoneSaisie.Controls.Add(this.label2);
            this.zoneSaisie.Controls.Add(this.label5);
            this.zoneSaisie.Controls.Add(this.cbxPremierMedicament);
            this.zoneSaisie.Controls.Add(this.label1);
            this.zoneSaisie.Dock = System.Windows.Forms.DockStyle.Top;
            this.zoneSaisie.Location = new System.Drawing.Point(0, 159);
            this.zoneSaisie.Margin = new System.Windows.Forms.Padding(2);
            this.zoneSaisie.Name = "zoneSaisie";
            this.zoneSaisie.Size = new System.Drawing.Size(1561, 567);
            this.zoneSaisie.TabIndex = 54;
            this.zoneSaisie.Visible = false;
            // 
            // txtIdVisite
            // 
            this.txtIdVisite.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtIdVisite.Enabled = false;
            this.txtIdVisite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdVisite.Location = new System.Drawing.Point(149, 67);
            this.txtIdVisite.Margin = new System.Windows.Forms.Padding(2);
            this.txtIdVisite.Name = "txtIdVisite";
            this.txtIdVisite.Size = new System.Drawing.Size(93, 26);
            this.txtIdVisite.TabIndex = 82;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(11, 73);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(140, 20);
            this.label15.TabIndex = 81;
            this.label15.Text = "Numéro de la fiche";
            // 
            // messageMedicamentDistribue
            // 
            this.messageMedicamentDistribue.AutoSize = true;
            this.messageMedicamentDistribue.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageMedicamentDistribue.ForeColor = System.Drawing.Color.Red;
            this.messageMedicamentDistribue.Location = new System.Drawing.Point(846, 39);
            this.messageMedicamentDistribue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageMedicamentDistribue.Name = "messageMedicamentDistribue";
            this.messageMedicamentDistribue.Size = new System.Drawing.Size(39, 17);
            this.messageMedicamentDistribue.TabIndex = 80;
            this.messageMedicamentDistribue.Text = "msg";
            this.messageMedicamentDistribue.Visible = false;
            // 
            // checkMedicament
            // 
            this.checkMedicament.AutoSize = true;
            this.checkMedicament.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkMedicament.Location = new System.Drawing.Point(849, 14);
            this.checkMedicament.Name = "checkMedicament";
            this.checkMedicament.Size = new System.Drawing.Size(407, 22);
            this.checkMedicament.TabIndex = 79;
            this.checkMedicament.Text = "Je n\'ai distribué aucun médicament lors de cette visite";
            this.checkMedicament.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(846, 18);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 18);
            this.label11.TabIndex = 78;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(856, 136);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(243, 18);
            this.label6.TabIndex = 77;
            this.label6.Text = "Liste des médicaments distribués";
            // 
            // txtDate
            // 
            this.txtDate.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtDate.Enabled = false;
            this.txtDate.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(149, 110);
            this.txtDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(270, 26);
            this.txtDate.TabIndex = 76;
            // 
            // txtHeure
            // 
            this.txtHeure.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtHeure.Enabled = false;
            this.txtHeure.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeure.Location = new System.Drawing.Point(149, 156);
            this.txtHeure.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeure.Name = "txtHeure";
            this.txtHeure.Size = new System.Drawing.Size(171, 26);
            this.txtHeure.TabIndex = 74;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(10, 162);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 20);
            this.label16.TabIndex = 73;
            this.label16.Text = "Horaire";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(10, 111);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 20);
            this.label17.TabIndex = 72;
            this.label17.Text = "Date";
            // 
            // txtSpecialite
            // 
            this.txtSpecialite.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtSpecialite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpecialite.Location = new System.Drawing.Point(149, 321);
            this.txtSpecialite.Multiline = true;
            this.txtSpecialite.Name = "txtSpecialite";
            this.txtSpecialite.Size = new System.Drawing.Size(270, 55);
            this.txtSpecialite.TabIndex = 70;
            // 
            // lblSpecialite
            // 
            this.lblSpecialite.AutoSize = true;
            this.lblSpecialite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpecialite.Location = new System.Drawing.Point(11, 323);
            this.lblSpecialite.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSpecialite.Name = "lblSpecialite";
            this.lblSpecialite.Size = new System.Drawing.Size(75, 18);
            this.lblSpecialite.TabIndex = 69;
            this.lblSpecialite.Text = "Spécialité";
            // 
            // txtType
            // 
            this.txtType.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtType.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtType.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtType.Location = new System.Drawing.Point(149, 287);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(270, 26);
            this.txtType.TabIndex = 68;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 287);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 18);
            this.label10.TabIndex = 67;
            this.label10.Text = "Type praticien";
            // 
            // txtPraticien
            // 
            this.txtPraticien.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtPraticien.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPraticien.Location = new System.Drawing.Point(149, 255);
            this.txtPraticien.Name = "txtPraticien";
            this.txtPraticien.Size = new System.Drawing.Size(270, 26);
            this.txtPraticien.TabIndex = 66;
            // 
            // txtMotif
            // 
            this.txtMotif.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtMotif.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotif.Location = new System.Drawing.Point(149, 224);
            this.txtMotif.Name = "txtMotif";
            this.txtMotif.Size = new System.Drawing.Size(270, 26);
            this.txtMotif.TabIndex = 65;
            // 
            // txtVille
            // 
            this.txtVille.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtVille.Enabled = false;
            this.txtVille.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVille.Location = new System.Drawing.Point(149, 191);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(270, 26);
            this.txtVille.TabIndex = 64;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 224);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 18);
            this.label12.TabIndex = 63;
            this.label12.Text = "Motif";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(11, 197);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 20);
            this.label13.TabIndex = 62;
            this.label13.Text = "Lieu";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(11, 255);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 20);
            this.label14.TabIndex = 61;
            this.label14.Text = "Praticien";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 29);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 18);
            this.label7.TabIndex = 58;
            this.label7.Text = "Fiche visite ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(404, -16);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(243, 18);
            this.label9.TabIndex = 57;
            this.label9.Text = "Liste des médicaments distribués";
            // 
            // dgvPanier
            // 
            this.dgvPanier.BackgroundColor = System.Drawing.Color.White;
            this.dgvPanier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPanier.Location = new System.Drawing.Point(849, 165);
            this.dgvPanier.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.dgvPanier.Name = "dgvPanier";
            this.dgvPanier.RowTemplate.Height = 28;
            this.dgvPanier.Size = new System.Drawing.Size(685, 360);
            this.dgvPanier.TabIndex = 56;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(846, 75);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 18);
            this.label8.TabIndex = 55;
            this.label8.Text = "Médicament";
            // 
            // lblQuantite
            // 
            this.lblQuantite.AutoSize = true;
            this.lblQuantite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantite.Location = new System.Drawing.Point(1120, 75);
            this.lblQuantite.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblQuantite.Name = "lblQuantite";
            this.lblQuantite.Size = new System.Drawing.Size(70, 18);
            this.lblQuantite.TabIndex = 54;
            this.lblQuantite.Text = "Quantité";
            // 
            // cptQuantite
            // 
            this.cptQuantite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cptQuantite.Location = new System.Drawing.Point(1123, 95);
            this.cptQuantite.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cptQuantite.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.cptQuantite.Name = "cptQuantite";
            this.cptQuantite.Size = new System.Drawing.Size(52, 26);
            this.cptQuantite.TabIndex = 53;
            this.cptQuantite.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnAjouter
            // 
            this.btnAjouter.BackColor = System.Drawing.Color.Red;
            this.btnAjouter.Font = new System.Drawing.Font("Georgia", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouter.Location = new System.Drawing.Point(1179, 95);
            this.btnAjouter.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(355, 38);
            this.btnAjouter.TabIndex = 52;
            this.btnAjouter.Text = "Ajouter ce médicament dans la liste";
            this.btnAjouter.UseVisualStyleBackColor = false;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // txtMedicament
            // 
            this.txtMedicament.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMedicament.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMedicament.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedicament.Location = new System.Drawing.Point(849, 95);
            this.txtMedicament.Margin = new System.Windows.Forms.Padding(2);
            this.txtMedicament.Name = "txtMedicament";
            this.txtMedicament.Size = new System.Drawing.Size(270, 26);
            this.txtMedicament.TabIndex = 51;
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.BackColor = System.Drawing.Color.Red;
            this.btnEnregistrer.Font = new System.Drawing.Font("Georgia", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistrer.Location = new System.Drawing.Point(427, 487);
            this.btnEnregistrer.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(264, 38);
            this.btnEnregistrer.TabIndex = 50;
            this.btnEnregistrer.Text = "Enregistrer la fiche visite";
            this.btnEnregistrer.UseVisualStyleBackColor = false;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // txtBilan
            // 
            this.txtBilan.AcceptsReturn = true;
            this.txtBilan.AcceptsTab = true;
            this.txtBilan.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBilan.Location = new System.Drawing.Point(427, 300);
            this.txtBilan.Margin = new System.Windows.Forms.Padding(2);
            this.txtBilan.Multiline = true;
            this.txtBilan.Name = "txtBilan";
            this.txtBilan.Size = new System.Drawing.Size(404, 119);
            this.txtBilan.TabIndex = 38;
            // 
            // messageSecondMedicament
            // 
            this.messageSecondMedicament.AutoSize = true;
            this.messageSecondMedicament.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageSecondMedicament.ForeColor = System.Drawing.Color.Red;
            this.messageSecondMedicament.Location = new System.Drawing.Point(424, 251);
            this.messageSecondMedicament.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageSecondMedicament.Name = "messageSecondMedicament";
            this.messageSecondMedicament.Size = new System.Drawing.Size(39, 17);
            this.messageSecondMedicament.TabIndex = 46;
            this.messageSecondMedicament.Text = "msg";
            this.messageSecondMedicament.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(424, 29);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(292, 18);
            this.label3.TabIndex = 45;
            this.label3.Text = "Informations à renseigner après la visite";
            // 
            // messageBilan
            // 
            this.messageBilan.AutoSize = true;
            this.messageBilan.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageBilan.ForeColor = System.Drawing.Color.Red;
            this.messageBilan.Location = new System.Drawing.Point(424, 432);
            this.messageBilan.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageBilan.Name = "messageBilan";
            this.messageBilan.Size = new System.Drawing.Size(39, 17);
            this.messageBilan.TabIndex = 44;
            this.messageBilan.Text = "msg";
            this.messageBilan.Visible = false;
            // 
            // messagePremierMedicament
            // 
            this.messagePremierMedicament.AutoSize = true;
            this.messagePremierMedicament.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messagePremierMedicament.ForeColor = System.Drawing.Color.Red;
            this.messagePremierMedicament.Location = new System.Drawing.Point(424, 165);
            this.messagePremierMedicament.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messagePremierMedicament.Name = "messagePremierMedicament";
            this.messagePremierMedicament.Size = new System.Drawing.Size(39, 17);
            this.messagePremierMedicament.TabIndex = 43;
            this.messagePremierMedicament.Text = "msg";
            this.messagePremierMedicament.Visible = false;
            // 
            // cbxSecondMedicament
            // 
            this.cbxSecondMedicament.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxSecondMedicament.FormattingEnabled = true;
            this.cbxSecondMedicament.Location = new System.Drawing.Point(427, 222);
            this.cbxSecondMedicament.Name = "cbxSecondMedicament";
            this.cbxSecondMedicament.Size = new System.Drawing.Size(260, 26);
            this.cbxSecondMedicament.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(424, 199);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 18);
            this.label2.TabIndex = 36;
            this.label2.Text = "Second médicament proposé";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(424, 113);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(223, 18);
            this.label5.TabIndex = 34;
            this.label5.Text = "Premier médicament présenté";
            // 
            // cbxPremierMedicament
            // 
            this.cbxPremierMedicament.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPremierMedicament.FormattingEnabled = true;
            this.cbxPremierMedicament.Location = new System.Drawing.Point(427, 136);
            this.cbxPremierMedicament.Name = "cbxPremierMedicament";
            this.cbxPremierMedicament.Size = new System.Drawing.Size(260, 26);
            this.cbxPremierMedicament.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(424, 280);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 18);
            this.label1.TabIndex = 33;
            this.label1.Text = "Bilan de la visite";
            // 
            // FrmBilan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1561, 774);
            this.Controls.Add(this.zoneSaisie);
            this.Controls.Add(this.zoneSelection);
            this.Controls.Add(this.label4);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmBilan";
            this.Load += new System.EventHandler(this.FrmBilan_Load);
            this.Controls.SetChildIndex(this.lblTitre, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.zoneSelection, 0);
            this.Controls.SetChildIndex(this.zoneSaisie, 0);
            this.zoneSelection.ResumeLayout(false);
            this.zoneSelection.PerformLayout();
            this.zoneSaisie.ResumeLayout(false);
            this.zoneSaisie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPanier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cptQuantite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel zoneSelection;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Panel zoneSaisie;
        private System.Windows.Forms.Label messageMedicamentDistribue;
        private System.Windows.Forms.CheckBox checkMedicament;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtHeure;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSpecialite;
        private System.Windows.Forms.Label lblSpecialite;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPraticien;
        private System.Windows.Forms.TextBox txtMotif;
        private System.Windows.Forms.TextBox txtVille;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvPanier;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblQuantite;
        private System.Windows.Forms.NumericUpDown cptQuantite;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.TextBox txtMedicament;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.TextBox txtBilan;
        private System.Windows.Forms.Label messageSecondMedicament;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label messageBilan;
        private System.Windows.Forms.Label messagePremierMedicament;
        private System.Windows.Forms.ComboBox cbxSecondMedicament;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbxPremierMedicament;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIdVisite;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbxVisite;
    }
}