﻿// ------------------------------------------
// Nom du fichier : FrmBilan.cs
// Objet : Formulaire de saisie du bilan d'une visite
// Auteur : M. Verghote
// Date mise à jour : 07/05/2019
// ------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmBilan : FrmBase
    {
        private Visite uneVisite; // mémorise la visite en cours de mise à jour
        private Panier panierMedicament;
        private List<Visite> lesVisites = Globale.db.LesVisites.FindAll(element => element.Date <= DateTime.Today && element.Bilan == null);

        public FrmBilan()
        {
            InitializeComponent();
        }

        #region procédures événementielles
        private void FrmBilan_Load(object sender, EventArgs e)
        {
            panierMedicament = new Panier();
            parametrerComposant();
        }

        private void cbxVisite_SelectedIndexChanged(object sender, EventArgs e)
        {
            uneVisite = (Visite)cbxVisite.SelectedItem;
            remplirVisite();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            modification();
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            Medicament medicamentDistribue = Globale.db.LesMedicaments.Find(element => element.Nom == txtMedicament.Text);
            if (medicamentDistribue == null)
            {
                MessageBox.Show("Vous dévez sélectionner le médicament dans la liste", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                panierMedicament.ajouter(medicamentDistribue, (int)cptQuantite.Value);
                dgvPanier.Rows.Clear();
                foreach (KeyValuePair<Medicament, int> unCouple in panierMedicament.Contenu)
                {
                    Medicament unMedicament = unCouple.Key;
                    int quantite = unCouple.Value;
                    dgvPanier.Rows.Add(unMedicament, unMedicament.Nom, quantite);
                }
            }
        }

        private void dgvPanier_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex != 3) return;
            Medicament unMedicament = (Medicament)dgvPanier[0, e.RowIndex].Value;
            panierMedicament.supprimer(unMedicament);
            dgvPanier.Rows.RemoveAt(e.RowIndex);
        }



       
        #endregion

        #region méthodes

        private void remplirVisite()
        {
            txtIdVisite.Text = uneVisite.Id.ToString();
            txtDate.Text = uneVisite.Date.ToLongDateString();
            txtHeure.Text = uneVisite.Heure;
            txtVille.Text = uneVisite.LePraticien.Ville;
            txtMotif.Text = uneVisite.LeMotif.Libelle;
            txtPraticien.Text = uneVisite.LePraticien.NomPrenom;
            txtType.Text = uneVisite.LePraticien.Type.Libelle;
            if (uneVisite.LePraticien.Specialite != null)
            {
                lblSpecialite.Visible = true;
                txtSpecialite.Visible = true;
                txtSpecialite.Text = uneVisite.LePraticien.Specialite.Libelle;
            } else
            {
                lblSpecialite.Visible = false;
                txtSpecialite.Visible = false;
            }

            cbxPremierMedicament.SelectedItem = uneVisite.PremierMedicament == null ? null : uneVisite.PremierMedicament;
            cbxSecondMedicament.SelectedItem = uneVisite.SecondMedicament == null ? null : uneVisite.SecondMedicament;
            txtBilan.Text = uneVisite.Bilan == null ? string.Empty : uneVisite.Bilan;

            // il faut aussi mettre à jour le panier 
            dgvPanier.Rows.Clear();
            foreach (KeyValuePair<Medicament, int> unCouple in uneVisite.LesMedicamentsDistribues)
            {
                Medicament unMedicament = unCouple.Key;
                int quantite = unCouple.Value;
                dgvPanier.Rows.Add(unMedicament, unMedicament.Nom, quantite);
            }



            zoneSaisie.Visible = true;
        }

        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Enregistrement du bilan d'une visite";
            #endregion

            #region paramétrage zone de liste pour les médicaments proposés

            cbxPremierMedicament.DataSource = Globale.db.LesMedicaments;
            cbxPremierMedicament.DisplayMember = "Nom";
            cbxPremierMedicament.ValueMember = "Id";
            cbxPremierMedicament.SelectedIndex = -1;
            cbxPremierMedicament.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbxPremierMedicament.AutoCompleteSource = AutoCompleteSource.ListItems;

            // important : Pour pouvoir utiliser le même dataSource sur deux listes
            // il faut créer un nouveau context sinon les deux listes sont synchronisées
            cbxSecondMedicament.BindingContext = new BindingContext();

            cbxSecondMedicament.DataSource = Globale.db.LesMedicaments;
            cbxSecondMedicament.DisplayMember = "Nom";
            cbxSecondMedicament.ValueMember = "Id";
            cbxSecondMedicament.SelectedItem = null;
            cbxSecondMedicament.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbxSecondMedicament.AutoCompleteSource = AutoCompleteSource.ListItems;

            #endregion

            #region paramétrage dgvPanier

            dgvPanier.Width = 700;

            // définition du style par défaut pour les cellules d'entête de colonne

            DataGridViewCellStyle style = dgvPanier.ColumnHeadersDefaultCellStyle;
            style.BackColor = Color.Aquamarine;
            style.ForeColor = Color.White;
            style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            style.Font = new Font("Georgia", 12, FontStyle.Bold);

            // définition du mode de sélection des lignes
            dgvPanier.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            // suppression de la mise en forme de la ligne sélectionnée
            dgvPanier.RowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            dgvPanier.RowsDefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;

            // la ligne d'entête est visible
            dgvPanier.ColumnHeadersVisible = true;
            // La colonne d'entête n'est pas visible
            dgvPanier.RowHeadersVisible = false;
            // l'utilisateur ne peut ni ajouter ni supprimer des lignes
            dgvPanier.AllowUserToDeleteRows = false;
            dgvPanier.AllowUserToAddRows = false;

            // définition des colonnes
            dgvPanier.ColumnCount = 3;

            dgvPanier.Columns[0].Visible = false;

            dgvPanier.Columns[1].Name = "Medicament";
            dgvPanier.Columns[1].Width = 400;

            dgvPanier.Columns[2].Name = "Quantite";
            dgvPanier.Columns[2].Width = 150;
            dgvPanier.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // ajout d'une colonne contenant le bouton supprimer
            DataGridViewButtonColumn uneColonneBouton = new DataGridViewButtonColumn();
            // uneColonneBouton.HeaderText = "Action";
            // uneColonneBouton.Name = "Action";
            uneColonneBouton.Text = "Supprimer";
            uneColonneBouton.UseColumnTextForButtonValue = true;
            dgvPanier.Columns.Add(uneColonneBouton);
            dgvPanier.Columns[3].Width = 150;



            #endregion
            
            #region paramètrage zone de liste visite (praticien)
            cbxVisite.DataSource = lesVisites;
            cbxVisite.DisplayMember = "LePraticien";
            cbxVisite.ValueMember = "Id";
            cbxVisite.SelectedIndex = -1;

            // l'événement sur changement dans une zone de liste se déclenche lors de la modification de la propriété DataSource
            // l'événement doit donc être ajouté après avoir modifier la propriété DataSource
            cbxVisite.SelectedIndexChanged += new System.EventHandler(this.cbxVisite_SelectedIndexChanged);
            cbxVisite.SelectedIndex = 0;

            #endregion

            #region paramétrage txtMedicament
            txtMedicament.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtMedicament.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection source = new AutoCompleteStringCollection();
            foreach (Medicament unMedicament in Globale.db.LesMedicaments)
            {
                source.Add(unMedicament.Nom);
            }
            txtMedicament.AutoCompleteCustomSource = source;


            #endregion
        }

        private void modification()
        {
            bool premierMedicamentOk = controlerPremierMedicament();
            bool secondMedicamentOk = controlerSecondMedicament();
            bool bilanOk = controlerBilan();
            if (checkMedicament.Checked)
            {
                if (panierMedicament.Contenu.Count > 0)
                {
                    messageMedicamentDistribue.Text = "Vous ne devez pas cocher la case si vous avez distribué des médicaments";
                    messageMedicamentDistribue.Visible = true;
                }
                else
                {
                    messageMedicamentDistribue.Visible = false;
                }
            }
            else
            {
                if (panierMedicament.Contenu.Count == 0)
                {
                    messageMedicamentDistribue.Text = "Vous devez cocher la case si vous n'avez pas distribué des médicaments";
                    messageMedicamentDistribue.Visible = true;
                }
                else
                {
                    messageMedicamentDistribue.Visible = false;
                }
            }
            if (premierMedicamentOk && secondMedicamentOk && bilanOk && !messageMedicamentDistribue.Visible)
            {
                modifier();
            }
        }

        private bool controlerPremierMedicament()
        {
            if (cbxPremierMedicament.SelectedIndex == -1)
            {
                messagePremierMedicament.Text = "Veuillez sélectionner un médicament.";
                messagePremierMedicament.Visible = true;
                return false;
            }
            messagePremierMedicament.Text = "";
            messagePremierMedicament.Visible = false;
            return true;
        }

        private bool controlerSecondMedicament()
        {
            if (cbxPremierMedicament.SelectedIndex == cbxSecondMedicament.SelectedIndex)
            {
                messageSecondMedicament.Text = "Veuillez sélectionner un autre médicament.";
                messageSecondMedicament.Visible = true;
                return false;
            }
            messageSecondMedicament.Text = "";
            messageSecondMedicament.Visible = false;
            return true;
        }

        private bool controlerBilan()
        {
            if (txtBilan.Text == string.Empty)
            {
                messageBilan.Text = "Le bilan doit être complété ";
                messageBilan.Visible = true;
                return false;
            }
            messageBilan.Text = "";
            messageBilan.Visible = false;
            return true;
        }

        // enregistrement du bilan, des médicaments présentés et des médicaments distribués
        private void modifier()
        {
            Medicament premierMedicament = (Medicament)cbxPremierMedicament.SelectedItem;
            Medicament secondMedicament = null;
            bool ok;
            string message;
            if (cbxSecondMedicament.SelectedIndex >= 0)
            {
                secondMedicament = (Medicament)cbxSecondMedicament.SelectedItem;
                ok = Passerelle.enregistrerBilanVisite(uneVisite.Id, txtBilan.Text, premierMedicament.Id, secondMedicament.Id, panierMedicament.Contenu, out message);

            }
            else
            {
                ok = Passerelle.enregistrerBilanVisite(uneVisite.Id, txtBilan.Text, premierMedicament.Id, null, panierMedicament.Contenu,  out message);
            }
            if (ok)
            {
                // mise à jour des données en mémoire
                uneVisite.PremierMedicament = premierMedicament;
                uneVisite.SecondMedicament = secondMedicament;
                uneVisite.Bilan = txtBilan.Text;
                // la mise à jour des médicaments distribués
                uneVisite.LesMedicamentsDistribues = panierMedicament.Contenu;
                MessageBox.Show("Bilan visite mémorisé", "", MessageBoxButtons.OK);

            }
            else
            {
                MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

    }
}


