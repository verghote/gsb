﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmAjoutPraticien : FrmBase
    {
        public FrmAjoutPraticien()
        {
            InitializeComponent();
        }

        #region procédures événementielles

        private void FrmAjoutPraticien_Load(object sender, EventArgs e)
        {
            parametrerComposant();
        }

       

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            ajout();
        }

        #endregion

        #region méthodes

        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Enregistrer un nouveau praticien";
            #endregion

            #region paramètrage des zones de liste
            // alimentation de la zone de liste déroulante contenant les type de praticiens
            cbxType.DataSource = Globale.db.LesTypes;
            cbxType.DisplayMember = "Libelle";
            cbxType.ValueMember = "Id";
            cbxType.SelectedIndex = -1;

            // alimentation de la zone de liste déroulante contenant les spécialités
            cbxSpecialite.DataSource = Globale.db.LesSpecialites;
            cbxSpecialite.DisplayMember = "Libelle";
            cbxSpecialite.ValueMember = "Id";
            cbxSpecialite.SelectedItem = null;
            #endregion

            #region Paramètrage auto complétion sur la ville
            // paramétrage de l'heure
            txtVille.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtVille.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var source = new AutoCompleteStringCollection();
            foreach (Ville uneVille in Globale.db.LesVilles)
            {
                source.Add(uneVille.Nom);
            }
            txtVille.AutoCompleteCustomSource = source;
            #endregion
        }

        private void ajout()
        {
            // pas de contrôle sur les zone de liste qui peuvent rester vide
            bool nomOk = controlerNom();
            bool prenomOk = controlerPrenom();
            bool rueOk = controlerRue();
            bool villeOk = controlerVille();
            bool emailOk = controlerEmail();
            bool telephoneOk = controlerTelephone();
            bool typeOk = controlerType();
            if (nomOk && prenomOk && rueOk && villeOk && emailOk && telephoneOk && typeOk)
            {
                ajouter();
            }
        }

        private bool controlerNom()
        {
            if (txtNom.Text == string.Empty)
            {
                messageNom.Text = "Le nom du praticien doit être précisé";
                messageNom.Visible = true;
                return false;
            }
            messageNom.Text = "";
            messageNom.Visible = false;
            return true;
        }

        private bool controlerPrenom()
        {
            if (txtPrenom.Text == string.Empty)
            {
                messagePrenom.Text = "Le prénom du praticien doit être précisé";
                messagePrenom.Visible = true;
                return false;
            }
            messagePrenom.Text = "";
            messagePrenom.Visible = false;
            return true;
        }

        private bool controlerRue()
        {
            if (txtRue.Text == string.Empty)
            {
                messageRue.Text = "La rue du praticien doit être précisée";
                messageRue.Visible = true;
                return false;
            }
            messageRue.Text = "";
            messageRue.Visible = false;
            return true;
        }

        private bool controlerVille()
        {
            if (txtVille.Text == string.Empty)
            {
                messageVille.Text = "La ville du praticien doit être précisée";
                messageVille.Visible = true;
                return false;
            }
            Ville uneVille = Globale.db.LesVilles.Find(element => element.Nom == txtVille.Text);
            if (uneVille == null)
            {
                messageVille.Text = "Vous devez sélectionner une ville dans la liste";
                messageVille.Visible = true;
                return false;
            }
            messageVille.Text = "";
            messageVille.Visible = false;
            return true;
        }

        private bool controlerEmail()
        {
            if (txtEmail.Text == string.Empty)
            {
                messageEmail.Text = "L'adresse mail du praticien doit être précisée";
                messageEmail.Visible = true;
                return false;
            }
            Regex uneExpression = new Regex(@"^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*[.][a-zA-Z]{2,3}$");
            if (!uneExpression.IsMatch(txtEmail.Text)) {
                messageEmail.Text = "L'adresse mail n'est pas valide";
                messageEmail.Visible = true;
                return false;
            }

            messageEmail.Text = "";
            messageEmail.Visible = false;
            return true;
        }

        private bool controlerTelephone()
        {
            if (txtTelephone.Text == string.Empty)
            {
                messageTelephone.Text = "Le téléphone du praticien doit être précisée";
                messageTelephone.Visible = true;
                return false;
            }

            Regex uneExpression = new Regex(@"^0[1-9]([\s.-]*\d{2}){4}$");
            if (!uneExpression.IsMatch(txtTelephone.Text))
            {
                messageTelephone.Text = "Le numéro de téléphone n'est pas valide";
                messageTelephone.Visible = true;
                return false;
            }

            messageTelephone.Text = "";
            messageTelephone.Visible = false;
            return true;
        }

        private bool controlerType()
        {
            if (cbxType.SelectedIndex == -1)
            {
                messageType.Text = "Veuillez sélectionner le type de praticien.";
                messageType.Visible = true;
                return false;
            }
            messageType.Text = "";
            messageType.Visible = false;
            return true;
        }

        private void ajouter()
        {
            string nom = txtNom.Text;
            string prenom = txtPrenom.Text;
            string rue = txtRue.Text;
            string ville = txtVille.Text;
            Ville uneVille = Globale.db.LesVilles.Find(element => element.Nom == txtVille.Text);
            string cp = uneVille.Code;
            string email = txtEmail.Text;
            string telephone = txtTelephone.Text;
            // mise en forme du téléphone : on retire tous les espaces éventuels et on ajoute un espace tous les deux chiffres
            // telephone  = telephone.Replace(" ", string.Empty);
            telephone  = Regex.Replace(telephone, @"\s+", "");
            telephone = telephone.Substring(0, 2) + ' ' + telephone.Substring(2, 2) + ' ' + telephone.Substring(4, 2) + ' ' + telephone.Substring(6, 2) + ' ' + telephone.Substring(8);
            string idType = (string)cbxType.SelectedValue;
            string idSpecialite = (string)cbxSpecialite.SelectedValue;
            
            string message;
            int idPraticien = Passerelle.ajouterPraticien(nom, prenom, rue, cp, ville, telephone, email, idType,  idSpecialite, out message);
            if (idPraticien == 0)
            {
                MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                // mise à jour des données en mémoire : la collection des praticiens
                TypePraticien unType = Globale.db.LesTypes.Find(element => element.Id == idType);
                Specialite uneSpecialite = null;
                if (idSpecialite != null)
                {
                    uneSpecialite = Globale.db.LesSpecialites.Find(element => element.Id == idSpecialite);
                }
                Praticien unPraticien = new Praticien(idPraticien, nom, prenom, rue, cp, ville, email, telephone, unType, uneSpecialite);
                Globale.db.LesPraticiens.Add(unPraticien);

                Globale.db.LesPraticiens.Sort();

                // mise à jour de l'interface
                cbxSpecialite.SelectedIndex = -1;
                cbxType.SelectedIndex = -1;
                txtNom.Text = "";
                txtPrenom.Text = "";
                txtRue.Text = "";
                txtVille.Text = "";
                txtTelephone.Text = "";
                txtEmail.Text = "";
                MessageBox.Show("Praticien ajouté", "", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }
        #endregion
    }
}
