﻿// ------------------------------------------
// Nom du fichier : Passerelle.cs
// Objet : classe Passerelle assurant l'alimentation des objets en mémoire
// Auteur : M. Verghote
// Date mise à jour : 07/05/2019
// ------------------------------------------

using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using lesClasses;

namespace GSB
{
    static class Passerelle
    {
        static private ConnectionStringSettings uneChaine = ConfigurationManager.ConnectionStrings["sgbd"];
        static private string maChaineDeConnexion = uneChaine.ConnectionString;
        static private SqlConnection maConnexion = new SqlConnection(maChaineDeConnexion);

        static public bool testConnexion(out string message)
        {
            bool ok = true;
            message = "";
            try
            {
                maConnexion.Open();
            }
            catch
            {
                ok = false;
                message = "Erreur dans les paramètres de connexion à la base de données, contacter la maintenance";

            }
            finally
            {
                maConnexion.Close();
            }
            return ok;
        }

        static public bool getLesVisiteurs(out List<string> lesNoms, out string message)
        {
            bool ok = true;
            message = "";
            lesNoms = new List<string>();
            maConnexion.Open();
            SqlCommand uneCommande = new SqlCommand("getLesVisiteurs", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            try
            {
                SqlDataReader unCurseur = uneCommande.ExecuteReader();

                while (unCurseur.Read())
                {
                    lesNoms.Add(unCurseur["nom"].ToString());
                }
                unCurseur.Close();
                if (lesNoms.Count == 0) { 
                
                    ok = false;
                    message = "Aucun visiteur n'est actuellement référencé sur le serveur";
                }
            }
            catch (Exception e)
            {
                ok = false;
                message = "Procédure getLesVisiteurs non trouvé sur le serveur, contacter la maintenance";
               // message = e.ToString();

            }
            finally
            {
                maConnexion.Close();
            }
            return ok;
        }


        static public bool getVisiteur(string nom, string mdp, out Visiteur unVisiteur, out string message)
        {
            bool ok;
            unVisiteur = null;
            maConnexion.Open();
            SqlCommand uneCommande = new SqlCommand("verifierConnexion", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("nom", nom);
            uneCommande.Parameters.AddWithValue("mdp", mdp);
            try
            {
                SqlDataReader unCurseur = uneCommande.ExecuteReader();
                if (unCurseur.Read())
                {
                    string unId = (string)unCurseur["id"]; ;
                    string unNom = (string)unCurseur["nom"]; ;
                    string unPrenom = (string)unCurseur["prenom"];
                    string uneRue = (string)unCurseur["rue"];
                    string unCodePostal = (string)unCurseur["codePostal"];
                    string uneVille = (string)unCurseur["ville"];
                    // DateTime uneDateEmbauche = (DateTime)unCurseur["dateEmbauche"];
                    DateTime uneDateEmbauche = unCurseur.GetDateTime(6);
                    unVisiteur = new Visiteur(unId, unNom, unPrenom, uneRue, unCodePostal, uneVille, uneDateEmbauche);
                    ok = true;
                    message = "Visiteur authentifié";
                }
                else
                {
                    ok = false;
                    message = "Les paramètres de connexion sont incorrects";
                }
            }
            catch(Exception e) 
            {
                ok = false;
                message = "La fonction d'identification est absente sur le serveur, contacter la maintenance";
                message = e.ToString();

            }
            finally
            {
                maConnexion.Close();
            }
            return ok;
        }
    
        // chargement des données pour le visiteur connectés : id
        static public Donnee chargerDonnees(string id)
        {
            Donnee bd = new Donnee();
            maConnexion.Open();
            
            // Chargement des objets Motifs
            SqlCommand uneCommande = new SqlCommand("getLesMotifs", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataReader unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                int unId = (Int32)unCurseur["id"];
                string unLibelle = (string)unCurseur["libelle"];
                // création de l'objet et ajout dans donnees
                Motif unMotif = new Motif(unId, unLibelle);
                bd.LesMotifs.Add(unMotif);
            }
            unCurseur.Close();

            // Chargement des objets TypePraticien
            uneCommande = new SqlCommand("getLesTypes", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                string unId = unCurseur["id"].ToString();
                string unLibelle = unCurseur["libelle"].ToString();
                // création de l'objet et ajout dans donnees
                TypePraticien unType = new TypePraticien(unId, unLibelle);
                bd.LesTypes.Add(unType);
            }
            unCurseur.Close();


            // Chargement des objets Specialite
            uneCommande = new SqlCommand("getLesSpecialites", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                string unId = unCurseur["id"].ToString();
                string unLibelle = unCurseur["libelle"].ToString();
                // création de l'objet et ajout dans donnees
                Specialite uneSpecialite = new Specialite(unId, unLibelle);
                bd.LesSpecialites.Add(uneSpecialite);
            }
            unCurseur.Close();


            // Chargement des objets Famille
            uneCommande = new SqlCommand("getLesFamilles", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;

            unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                string unId = (string)unCurseur["id"];
                string unLibelle = (string)unCurseur["libelle"];
                // création de l'objet et ajout dans donnees
                Famille uneFamille = new Famille(unId, unLibelle);
                bd.LesFamilles.Add(unId, uneFamille);
            }
            unCurseur.Close();

            // chargement des objets médicaments
            uneCommande = new SqlCommand("getLesMedicaments", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                string unId = (string)unCurseur["id"];
                string unNom = (string)unCurseur["nom"];
                string uneComposition = (string)unCurseur["composition"];
                string unEffet = (string)unCurseur["effets"];
                string uneContreIndication = (string)unCurseur["contreIndication"];
                string unIdFamille = (string)unCurseur["idFamille"];
                // récupération de l'objet Famille
                Famille uneFamille = bd.LesFamilles[unIdFamille];
                // création de l'objet et ajout dans donnees
                Medicament unMedicament = new Medicament(unId, unNom, uneComposition, unEffet, uneContreIndication, uneFamille);
                bd.LesMedicaments.Add(unMedicament);
            }
            unCurseur.Close();

            // chargement des praticiens
            uneCommande = new SqlCommand("getLesPraticiens", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("idVisiteur", id);
            unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                int unId = (Int16)unCurseur["id"];
                string unNom = (string)unCurseur["nom"];
                string unPrenom = (string)unCurseur["prenom"];
                string uneRue = (string)unCurseur["rue"];
                string unCodePostal = (string)unCurseur["codePostal"];
                string uneVille = (string)unCurseur["ville"];
                string unEmail =  (string)unCurseur["email"];
                string unTelephone =  (string)unCurseur["telephone"];
                string idType = (string)unCurseur["idType"];
                string idSpecialite = unCurseur.IsDBNull(9) ? null : (string)unCurseur["idSpecialite"];
                // création de l'objet et ajout dans donnees

                TypePraticien unType = bd.LesTypes.Find(element => element.Id == idType);


                Specialite uneSpecialite = null;
                if (idSpecialite != null)
                {
                    uneSpecialite = bd.LesSpecialites.Find(element => element.Id == idSpecialite);
                }

                Praticien unPraticien = new Praticien(unId, unNom, unPrenom, uneRue, unCodePostal, uneVille, unEmail, unTelephone, unType, uneSpecialite);
                bd.LesPraticiens.Add(unPraticien);
            }
            unCurseur.Close();



            // chargement des visites du visiteur connecté
            uneCommande = new SqlCommand("getLesVisites", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("idVisiteur", id);
            unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                int unId = (Int32)unCurseur["id"];
                int unIdPraticien = (Int16) unCurseur["idPraticien"];
                int unIdMotif = (Int32) unCurseur["idMotif"];
                string unBilan = unCurseur.IsDBNull(3) ? null : (string) unCurseur["bilan"];
                DateTime uneDate = unCurseur.GetDateTime(1);
               // DateTime uneHeure = unCurseur.GetDateTime(2);
               string uneHeure =  unCurseur["heure"].ToString();
               Medicament premierMedicament = unCurseur.IsDBNull(6) ? null : bd.LesMedicaments.Find(element => element.Id == unCurseur["premierMedicament"].ToString());
               Medicament secondMedicament = unCurseur.IsDBNull(7) ? null : bd.LesMedicaments.Find(element => element.Id == unCurseur["secondMedicament"].ToString());

                // récupération des objets liés
                Motif unMotif = bd.LesMotifs.Find(element => element.Id == unIdMotif);
                Praticien unPraticien = bd.LesPraticiens.Find(element => element.Id == unIdPraticien);
                // création de l'objet et ajout dans donnees
                Visite uneVisite = new Visite(unId, unPraticien, unMotif, uneDate, uneHeure, unBilan, premierMedicament, secondMedicament);
                
                bd.LesVisites.Add(uneVisite);
            }
            unCurseur.Close();

            // chargement des médicaments distribués
            uneCommande = new SqlCommand("getLesMedicamentsDistribues", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("idVisiteur", id);
            unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                int unIdVisite = (Int32)unCurseur["idVisite"];
                string unIdMedicament =  unCurseur["idMedicament"].ToString();
                int uneQuantite = (Int32)unCurseur["quantite"];

                Medicament unMedicament = bd.LesMedicaments.Find(element => element.Id == unIdMedicament);
                Visite uneVisite = bd.LesVisites.Find(element => element.Id == unIdVisite);
                uneVisite.LesMedicamentsDistribues.Add(unMedicament, uneQuantite);
            }
            unCurseur.Close();

            // chargement des villes du département du visiteur pour la mise en place de l'auto complétion sur l'ajout d'un praticien
            uneCommande = new SqlCommand("getLesVilles", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("idVisiteur", id);
            unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                string nom = unCurseur["nom"].ToString();
                string code = unCurseur["codePostal"].ToString();
                Ville uneVille = new Ville(nom, code);
                bd.LesVilles.Add(uneVille);
            }

            unCurseur.Close();

            // chargement de la synthèse des médicaments distribués par le visiteur
            uneCommande = new SqlCommand("getTotalMedicamentsDistribues", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("idVisiteur", id);
            unCurseur = uneCommande.ExecuteReader();
            while (unCurseur.Read())
            {
                string nom = unCurseur["nom"].ToString();
                int total =  (Int32) unCurseur["total"];
                bd.LesDistributions.Add(nom, total);
            }
            unCurseur.Close();

            maConnexion.Close();
            return bd;
        }


        /// <summary>
        ///     Ajout d'une nouvelle visite
        /// </summary>
        /// <param name="idPraticien"></param>
        /// <param name="idMotif"></param>
        /// <param name="uneDate"></param>
        /// <param name="uneHeure"></param>
        /// <param name="message"></param>
        /// <returns>identifiant de la nouvelle visite ou 0 si erreur lors de la création</returns>
        static public int ajouterVisite(int idPraticien, int idMotif, DateTime uneDate, string uneHeure, out string message)
        {
            int idVisite = 0;
            message = string.Empty;
            maConnexion.Open();
            SqlCommand uneCommande = new SqlCommand("ajoutervisite", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;

            uneCommande.Parameters.AddWithValue("idVisiteur", Globale.leVisiteur.Id);
            uneCommande.Parameters.AddWithValue("idPraticien", idPraticien);
            uneCommande.Parameters.AddWithValue("idMotif", idMotif);
            uneCommande.Parameters.AddWithValue("date", uneDate);
            uneCommande.Parameters.AddWithValue("heure", uneHeure);

            uneCommande.Parameters.Add("idVisite", System.Data.SqlDbType.Int);
            uneCommande.Parameters["idVisite"].Direction = System.Data.ParameterDirection.Output;
            try
            {
                uneCommande.ExecuteNonQuery();
                idVisite = (Int32)uneCommande.Parameters["idVisite"].Value;
            }

            catch (System.Data.SqlClient.SqlException e)
            {
                message += e.Message;

            }
           
            maConnexion.Close();
            return idVisite;
        }

        static public bool supprimerVisite(int idVisite, out string message)
        {
            message = string.Empty;
            bool ok;
            maConnexion.Open();
            SqlCommand uneCommande = new SqlCommand("supprimervisite", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("idVisite",idVisite);
            try
            {
                uneCommande.ExecuteNonQuery();
                ok = true;
            }

            catch (System.Data.SqlClient.SqlException e)
            {
                message += e.Message;
                ok = false;
            }

           


            maConnexion.Close();
            return ok;
        }

        static public bool modifierPlanificationVisite(int idVisite, DateTime uneDate, string uneHeure, out string message)
        {
            message = string.Empty;
            bool ok;
            maConnexion.Open();
            SqlCommand uneCommande = new SqlCommand("modifierPlanificationVisite", maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("idVisite", idVisite);
            uneCommande.Parameters.AddWithValue("date", uneDate);
            uneCommande.Parameters.AddWithValue("heure", uneHeure);
            try
            {
                uneCommande.ExecuteNonQuery();
                ok = true;
            }

            catch (System.Data.SqlClient.SqlException e)
            {
                message += e.Message;
                ok = false;
            }

         


            maConnexion.Close();
            return ok;
        }

        static public bool enregistrerBilanVisite(int idVisite, string unBilan, string premierMedicament, string secondMedicament, SortedDictionary<Medicament, int> lesMedicamentsDistribues, out string message)
        {
            message = string.Empty;
            bool ok;
            maConnexion.Open();

            SqlTransaction uneTransaction = maConnexion.BeginTransaction();

            // enregistremement du bilan des médicaments présentés
            string nomProcedure = secondMedicament == null ? "enregistrerBilanVisite2" : "enregistrerBilanVisite"; 
            SqlCommand uneCommande = new SqlCommand(nomProcedure, maConnexion);
            uneCommande.Transaction = uneTransaction;
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("idVisite", idVisite);
            uneCommande.Parameters.AddWithValue("bilan", unBilan);
            uneCommande.Parameters.AddWithValue("premierMedicament", premierMedicament);
            if (secondMedicament != null)
            {
                uneCommande.Parameters.AddWithValue("secondMedicament", secondMedicament);
            }
           
            try
            {
                uneCommande.ExecuteNonQuery();
                // enregistrement des médicaments distribués

                // suppression des anciennes valeurs mémorisées
                uneCommande = new SqlCommand("supprimerMedicamentsDistribues", maConnexion);
                uneCommande.Transaction = uneTransaction;
                uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
                uneCommande.Parameters.AddWithValue("idVisite", idVisite);
                uneCommande.ExecuteNonQuery();

                // parcours du dictionnaire pour ajouter chaque médicament distribué
                foreach (KeyValuePair<Medicament, int> unCouple in lesMedicamentsDistribues)
                {
                    uneCommande = new SqlCommand("enregistrerMedicamentsDistribues", maConnexion);
                    uneCommande.Transaction = uneTransaction;
                    uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
                    uneCommande.Parameters.AddWithValue("idVisite", idVisite);
                    uneCommande.Parameters.AddWithValue("idMedicament", unCouple.Key.Id);
                    uneCommande.Parameters.AddWithValue("quantite", unCouple.Value);
                    try
                    {
                        uneCommande.ExecuteNonQuery();
                    }

                    catch (System.Data.SqlClient.SqlException e)
                    {
                        message += e.Message;
                        uneTransaction.Rollback();
                        maConnexion.Close();
                        return false;
                    }
                }

            }

            catch (System.Data.SqlClient.SqlException e)
            {
                message += e.Message;
                uneTransaction.Rollback();
                maConnexion.Close();
                return false;
            }
            uneTransaction.Commit();
            maConnexion.Close();
            return true;
        }


        static public int ajouterPraticien(string unNom, string unPrenom, string uneRue, string unCodePostal, string uneVille, string unTelephone, string unEmail, string unType, string uneSpecialite, out string message)
        {
            int idPraticien = 0;
            message = string.Empty;
            maConnexion.Open();
            string nomProcedure = uneSpecialite == null ? "ajouterPraticien2" : "ajouterPraticien";
            SqlCommand uneCommande = new SqlCommand(nomProcedure, maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;

            uneCommande.Parameters.AddWithValue("nom", unNom);
            uneCommande.Parameters.AddWithValue("prenom", unPrenom);
            uneCommande.Parameters.AddWithValue("rue", uneRue);
            uneCommande.Parameters.AddWithValue("ville", uneVille );
            uneCommande.Parameters.AddWithValue("codePostal", unCodePostal );
            uneCommande.Parameters.AddWithValue("telephone", unTelephone );
            uneCommande.Parameters.AddWithValue("email", unEmail );
            uneCommande.Parameters.AddWithValue("idType", unType );
            if (uneSpecialite != null)
            {
                uneCommande.Parameters.AddWithValue("specialite", uneSpecialite);
            }

            uneCommande.Parameters.Add("idPraticien", System.Data.SqlDbType.Int);
            uneCommande.Parameters["idPraticien"].Direction = System.Data.ParameterDirection.Output;
            try
            {
                uneCommande.ExecuteNonQuery();
                idPraticien = (Int32)uneCommande.Parameters["idPraticien"].Value;
            }

            catch (System.Data.SqlClient.SqlException e)
            {
                message += e.Message;

            }

            maConnexion.Close();
            return idPraticien;
        }


        static public bool modifierPraticien(int unId, string unNom, string unPrenom, string uneRue, string unCodePostal, string uneVille, string unTelephone, string unEmail, string unType, string uneSpecialite, out string message)
        {
            bool ok = true;
            message = string.Empty;
            maConnexion.Open();
            string nomProcedure = uneSpecialite == null ? "modifierPraticien2" : "modifierPraticien";
            SqlCommand uneCommande = new SqlCommand(nomProcedure, maConnexion);
            uneCommande.CommandType = System.Data.CommandType.StoredProcedure;
            uneCommande.Parameters.AddWithValue("id", unId);
            uneCommande.Parameters.AddWithValue("nom", unNom);
            uneCommande.Parameters.AddWithValue("prenom", unPrenom);
            uneCommande.Parameters.AddWithValue("rue", uneRue);
            uneCommande.Parameters.AddWithValue("ville", uneVille);
            uneCommande.Parameters.AddWithValue("codePostal", unCodePostal);
            uneCommande.Parameters.AddWithValue("telephone", unTelephone);
            uneCommande.Parameters.AddWithValue("email", unEmail);
            uneCommande.Parameters.AddWithValue("idType", unType);
            if (uneSpecialite != null)
            {
                uneCommande.Parameters.AddWithValue("idSpecialite", uneSpecialite);
            }

            try
            {
                uneCommande.ExecuteNonQuery();
            }

            catch (System.Data.SqlClient.SqlException e)
            {
                ok = false;
                message += e.Message;

            }

            maConnexion.Close();
            return ok;
        }



    }
}
