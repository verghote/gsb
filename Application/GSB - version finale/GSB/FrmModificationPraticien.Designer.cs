﻿namespace GSB
{
    partial class FrmModificationPraticien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbxPraticien = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panelFiche = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.messageEmail = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.messageTelephone = new System.Windows.Forms.Label();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.messageVille = new System.Windows.Forms.Label();
            this.txtVille = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.messageRue = new System.Windows.Forms.Label();
            this.txtRue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.messagePrenom = new System.Windows.Forms.Label();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.messageNom = new System.Windows.Forms.Label();
            this.messageSpecialite = new System.Windows.Forms.Label();
            this.messageType = new System.Windows.Forms.Label();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbxSpecialite = new System.Windows.Forms.ComboBox();
            this.cbxType = new System.Windows.Forms.ComboBox();
            this.btnModifier = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panelFiche.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitre
            // 
            this.lblTitre.Size = new System.Drawing.Size(1142, 49);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbxPraticien);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1142, 58);
            this.panel1.TabIndex = 19;
            // 
            // cbxPraticien
            // 
            this.cbxPraticien.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxPraticien.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxPraticien.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPraticien.FormattingEnabled = true;
            this.cbxPraticien.Location = new System.Drawing.Point(570, 14);
            this.cbxPraticien.Name = "cbxPraticien";
            this.cbxPraticien.Size = new System.Drawing.Size(246, 26);
            this.cbxPraticien.TabIndex = 21;
           
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 17);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(477, 18);
            this.label7.TabIndex = 20;
            this.label7.Text = "Sélectionner le visiteur dont les coordonnées doivent être  modifiés";
            // 
            // panelFiche
            // 
            this.panelFiche.Controls.Add(this.panel3);
            this.panelFiche.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFiche.Location = new System.Drawing.Point(0, 131);
            this.panelFiche.Name = "panelFiche";
            this.panelFiche.Size = new System.Drawing.Size(1142, 536);
            this.panelFiche.TabIndex = 20;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.messageEmail);
            this.panel3.Controls.Add(this.txtEmail);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.messageTelephone);
            this.panel3.Controls.Add(this.txtTelephone);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.messageVille);
            this.panel3.Controls.Add(this.txtVille);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.messageRue);
            this.panel3.Controls.Add(this.txtRue);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.messagePrenom);
            this.panel3.Controls.Add(this.txtPrenom);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.messageNom);
            this.panel3.Controls.Add(this.messageSpecialite);
            this.panel3.Controls.Add(this.messageType);
            this.panel3.Controls.Add(this.txtNom);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.cbxSpecialite);
            this.panel3.Controls.Add(this.cbxType);
            this.panel3.Controls.Add(this.btnModifier);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1072, 536);
            this.panel3.TabIndex = 18;
            // 
            // messageEmail
            // 
            this.messageEmail.AutoSize = true;
            this.messageEmail.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageEmail.ForeColor = System.Drawing.Color.Red;
            this.messageEmail.Location = new System.Drawing.Point(37, 336);
            this.messageEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageEmail.Name = "messageEmail";
            this.messageEmail.Size = new System.Drawing.Size(39, 17);
            this.messageEmail.TabIndex = 73;
            this.messageEmail.Text = "msg";
            this.messageEmail.Visible = false;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(128, 310);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(245, 26);
            this.txtEmail.TabIndex = 72;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(36, 316);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 20);
            this.label10.TabIndex = 71;
            this.label10.Text = "Email";
            // 
            // messageTelephone
            // 
            this.messageTelephone.AutoSize = true;
            this.messageTelephone.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageTelephone.ForeColor = System.Drawing.Color.Red;
            this.messageTelephone.Location = new System.Drawing.Point(38, 280);
            this.messageTelephone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageTelephone.Name = "messageTelephone";
            this.messageTelephone.Size = new System.Drawing.Size(39, 17);
            this.messageTelephone.TabIndex = 70;
            this.messageTelephone.Text = "msg";
            this.messageTelephone.Visible = false;
            // 
            // txtTelephone
            // 
            this.txtTelephone.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelephone.Location = new System.Drawing.Point(129, 254);
            this.txtTelephone.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(245, 26);
            this.txtTelephone.TabIndex = 69;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(37, 260);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 20);
            this.label9.TabIndex = 68;
            this.label9.Text = "Téléphone";
            // 
            // messageVille
            // 
            this.messageVille.AutoSize = true;
            this.messageVille.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageVille.ForeColor = System.Drawing.Color.Red;
            this.messageVille.Location = new System.Drawing.Point(38, 217);
            this.messageVille.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageVille.Name = "messageVille";
            this.messageVille.Size = new System.Drawing.Size(39, 17);
            this.messageVille.TabIndex = 67;
            this.messageVille.Text = "msg";
            this.messageVille.Visible = false;
            // 
            // txtVille
            // 
            this.txtVille.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtVille.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtVille.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVille.Location = new System.Drawing.Point(129, 191);
            this.txtVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(245, 26);
            this.txtVille.TabIndex = 66;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(37, 197);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.TabIndex = 65;
            this.label6.Text = "Ville";
            // 
            // messageRue
            // 
            this.messageRue.AutoSize = true;
            this.messageRue.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageRue.ForeColor = System.Drawing.Color.Red;
            this.messageRue.Location = new System.Drawing.Point(38, 157);
            this.messageRue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageRue.Name = "messageRue";
            this.messageRue.Size = new System.Drawing.Size(39, 17);
            this.messageRue.TabIndex = 64;
            this.messageRue.Text = "msg";
            this.messageRue.Visible = false;
            // 
            // txtRue
            // 
            this.txtRue.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRue.Location = new System.Drawing.Point(129, 131);
            this.txtRue.Margin = new System.Windows.Forms.Padding(2);
            this.txtRue.Name = "txtRue";
            this.txtRue.Size = new System.Drawing.Size(431, 26);
            this.txtRue.TabIndex = 63;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(37, 137);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 20);
            this.label5.TabIndex = 62;
            this.label5.Text = "Rue";
            // 
            // messagePrenom
            // 
            this.messagePrenom.AutoSize = true;
            this.messagePrenom.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messagePrenom.ForeColor = System.Drawing.Color.Red;
            this.messagePrenom.Location = new System.Drawing.Point(38, 99);
            this.messagePrenom.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messagePrenom.Name = "messagePrenom";
            this.messagePrenom.Size = new System.Drawing.Size(39, 17);
            this.messagePrenom.TabIndex = 61;
            this.messagePrenom.Text = "msg";
            this.messagePrenom.Visible = false;
            // 
            // txtPrenom
            // 
            this.txtPrenom.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrenom.Location = new System.Drawing.Point(129, 73);
            this.txtPrenom.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(245, 26);
            this.txtPrenom.TabIndex = 60;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(37, 79);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 20);
            this.label8.TabIndex = 59;
            this.label8.Text = "Prénom";
            // 
            // messageNom
            // 
            this.messageNom.AutoSize = true;
            this.messageNom.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageNom.ForeColor = System.Drawing.Color.Red;
            this.messageNom.Location = new System.Drawing.Point(38, 44);
            this.messageNom.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageNom.Name = "messageNom";
            this.messageNom.Size = new System.Drawing.Size(39, 17);
            this.messageNom.TabIndex = 58;
            this.messageNom.Text = "msg";
            this.messageNom.Visible = false;
            // 
            // messageSpecialite
            // 
            this.messageSpecialite.AutoSize = true;
            this.messageSpecialite.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageSpecialite.ForeColor = System.Drawing.Color.Red;
            this.messageSpecialite.Location = new System.Drawing.Point(426, 405);
            this.messageSpecialite.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageSpecialite.Name = "messageSpecialite";
            this.messageSpecialite.Size = new System.Drawing.Size(39, 17);
            this.messageSpecialite.TabIndex = 57;
            this.messageSpecialite.Text = "msg";
            this.messageSpecialite.UseWaitCursor = true;
            this.messageSpecialite.Visible = false;
            // 
            // messageType
            // 
            this.messageType.AutoSize = true;
            this.messageType.Font = new System.Drawing.Font("Georgia", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageType.ForeColor = System.Drawing.Color.Red;
            this.messageType.Location = new System.Drawing.Point(37, 405);
            this.messageType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.messageType.Name = "messageType";
            this.messageType.Size = new System.Drawing.Size(39, 17);
            this.messageType.TabIndex = 56;
            this.messageType.Text = "msg";
            this.messageType.Visible = false;
            // 
            // txtNom
            // 
            this.txtNom.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom.Location = new System.Drawing.Point(129, 17);
            this.txtNom.Margin = new System.Windows.Forms.Padding(2);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(245, 26);
            this.txtNom.TabIndex = 55;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(37, 23);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 20);
            this.label11.TabIndex = 54;
            this.label11.Text = "Nom";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(37, 376);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 20);
            this.label12.TabIndex = 53;
            this.label12.Text = "Type";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(426, 382);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 20);
            this.label13.TabIndex = 52;
            this.label13.Text = "Specialité";
            // 
            // cbxSpecialite
            // 
            this.cbxSpecialite.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxSpecialite.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxSpecialite.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxSpecialite.FormattingEnabled = true;
            this.cbxSpecialite.Location = new System.Drawing.Point(547, 376);
            this.cbxSpecialite.Name = "cbxSpecialite";
            this.cbxSpecialite.Size = new System.Drawing.Size(246, 26);
            this.cbxSpecialite.TabIndex = 51;
            // 
            // cbxType
            // 
            this.cbxType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxType.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxType.FormattingEnabled = true;
            this.cbxType.Location = new System.Drawing.Point(128, 376);
            this.cbxType.Name = "cbxType";
            this.cbxType.Size = new System.Drawing.Size(246, 26);
            this.cbxType.TabIndex = 50;
            // 
            // btnModifier
            // 
            this.btnModifier.BackColor = System.Drawing.Color.Red;
            this.btnModifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifier.Location = new System.Drawing.Point(692, 460);
            this.btnModifier.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(330, 38);
            this.btnModifier.TabIndex = 49;
            this.btnModifier.Text = "Modifier la fiche du praticien";
            this.btnModifier.UseVisualStyleBackColor = false;
            this.btnModifier.Click += new System.EventHandler(this.btnModifier_Click);
            // 
            // FrmModificationPraticien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 718);
            this.Controls.Add(this.panelFiche);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmModificationPraticien";
            this.Text = "FrmModificationPraticien";
            this.Load += new System.EventHandler(this.FrmModificationPraticien_Load);
            this.Controls.SetChildIndex(this.lblTitre, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panelFiche, 0);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelFiche.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panelFiche;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cbxPraticien;
        private System.Windows.Forms.Label messageEmail;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label messageTelephone;
        private System.Windows.Forms.TextBox txtTelephone;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label messageVille;
        private System.Windows.Forms.TextBox txtVille;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label messageRue;
        private System.Windows.Forms.TextBox txtRue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label messagePrenom;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label messageNom;
        private System.Windows.Forms.Label messageSpecialite;
        private System.Windows.Forms.Label messageType;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbxSpecialite;
        private System.Windows.Forms.ComboBox cbxType;
        private System.Windows.Forms.Button btnModifier;
    }
}