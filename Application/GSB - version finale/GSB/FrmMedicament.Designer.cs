﻿namespace GSB
{
    partial class FrmMedicament
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelFiche = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtContreIndication = new System.Windows.Forms.TextBox();
            this.txtFamille = new System.Windows.Forms.TextBox();
            this.txtComposition = new System.Windows.Forms.TextBox();
            this.txtEffet = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMedicament = new System.Windows.Forms.TextBox();
            this.btnFiche = new System.Windows.Forms.Button();
            this.panelFiche.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitre
            // 
            this.lblTitre.Size = new System.Drawing.Size(1299, 49);
            // 
            // panelFiche
            // 
            this.panelFiche.Controls.Add(this.panel3);
            this.panelFiche.Controls.Add(this.panel2);
            this.panelFiche.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFiche.Location = new System.Drawing.Point(0, 131);
            this.panelFiche.Name = "panelFiche";
            this.panelFiche.Size = new System.Drawing.Size(1299, 459);
            this.panelFiche.TabIndex = 16;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtContreIndication);
            this.panel3.Controls.Add(this.txtFamille);
            this.panel3.Controls.Add(this.txtComposition);
            this.panel3.Controls.Add(this.txtEffet);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(175, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1072, 459);
            this.panel3.TabIndex = 18;
            // 
            // txtContreIndication
            // 
            this.txtContreIndication.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContreIndication.Location = new System.Drawing.Point(13, 361);
            this.txtContreIndication.Multiline = true;
            this.txtContreIndication.Name = "txtContreIndication";
            this.txtContreIndication.Size = new System.Drawing.Size(1059, 71);
            this.txtContreIndication.TabIndex = 19;
            // 
            // txtFamille
            // 
            this.txtFamille.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFamille.Location = new System.Drawing.Point(13, 17);
            this.txtFamille.Name = "txtFamille";
            this.txtFamille.Size = new System.Drawing.Size(545, 26);
            this.txtFamille.TabIndex = 18;
            // 
            // txtComposition
            // 
            this.txtComposition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtComposition.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtComposition.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComposition.Location = new System.Drawing.Point(13, 87);
            this.txtComposition.Margin = new System.Windows.Forms.Padding(2);
            this.txtComposition.Multiline = true;
            this.txtComposition.Name = "txtComposition";
            this.txtComposition.Size = new System.Drawing.Size(1299, 82);
            this.txtComposition.TabIndex = 17;
            // 
            // txtEffet
            // 
            this.txtEffet.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtEffet.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtEffet.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEffet.Location = new System.Drawing.Point(13, 186);
            this.txtEffet.Margin = new System.Windows.Forms.Padding(2);
            this.txtEffet.Multiline = true;
            this.txtEffet.Name = "txtEffet";
            this.txtEffet.Size = new System.Drawing.Size(1299, 150);
            this.txtEffet.TabIndex = 16;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(175, 459);
            this.panel2.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(33, 385);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 18);
            this.label4.TabIndex = 24;
            this.label4.Text = "Contre indication";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 230);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 18);
            this.label3.TabIndex = 23;
            this.label3.Text = "Effet";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 107);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 18);
            this.label2.TabIndex = 22;
            this.label2.Text = "Composition";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 18);
            this.label1.TabIndex = 21;
            this.label1.Text = "Famille";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtMedicament);
            this.panel1.Controls.Add(this.btnFiche);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1299, 58);
            this.panel1.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 17);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 18);
            this.label7.TabIndex = 20;
            this.label7.Text = "Médicament";
            // 
            // txtMedicament
            // 
            this.txtMedicament.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMedicament.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMedicament.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedicament.Location = new System.Drawing.Point(142, 9);
            this.txtMedicament.Margin = new System.Windows.Forms.Padding(2);
            this.txtMedicament.Name = "txtMedicament";
            this.txtMedicament.Size = new System.Drawing.Size(270, 26);
            this.txtMedicament.TabIndex = 19;
            // 
            // btnFiche
            // 
            this.btnFiche.BackColor = System.Drawing.Color.Red;
            this.btnFiche.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiche.Location = new System.Drawing.Point(443, 9);
            this.btnFiche.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnFiche.Name = "btnFiche";
            this.btnFiche.Size = new System.Drawing.Size(182, 38);
            this.btnFiche.TabIndex = 18;
            this.btnFiche.Text = "Voir la fiche";
            this.btnFiche.UseVisualStyleBackColor = false;
            this.btnFiche.Click += new System.EventHandler(this.btnFiche_Click);
            // 
            // FrmMedicament
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1299, 686);
            this.Controls.Add(this.panelFiche);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmMedicament";
            this.Text = "FrmMedicament";
            this.Load += new System.EventHandler(this.FrmMedicament_Load);
            this.Controls.SetChildIndex(this.lblTitre, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.panelFiche, 0);
            this.panelFiche.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelFiche;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtContreIndication;
        private System.Windows.Forms.TextBox txtFamille;
        private System.Windows.Forms.TextBox txtComposition;
        private System.Windows.Forms.TextBox txtEffet;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMedicament;
        private System.Windows.Forms.Button btnFiche;
    }
}