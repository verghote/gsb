﻿using System;
using System.Windows.Forms;
using lesClasses;

namespace GSB
{
    public partial class FrmMedicament : FrmBase
    {
        public FrmMedicament()
        {
            InitializeComponent();
        }

        #region procédures événementielles

        private void FrmMedicament_Load(object sender, EventArgs e)
        {
            parametrerComposant();
        }

        private void btnFiche_Click(object sender, EventArgs e)
        {
            panelFiche.Visible = false;
            if (txtMedicament.Text == string.Empty)
            {
                MessageBox.Show("Vous devez sélectionner un médicament");
            }
            else
            {
                Medicament unMedicament = Globale.db.LesMedicaments.Find(element => element.Nom == txtMedicament.Text);
                if (unMedicament == null)
                {
                    MessageBox.Show("Aucun médicament correspondant");
                }
                else
                {
                    panelFiche.Visible = true;
                    txtComposition.Text = unMedicament.ContreIndication;
                    txtEffet.Text = unMedicament.Effets;
                    txtFamille.Text = unMedicament.LaFamille.Libelle;
                    txtContreIndication.Text = unMedicament.ContreIndication;
                }

            }
        }


        #endregion

        #region méthodes
        private void parametrerComposant()
        {
            #region paramétrage de la fenêtre
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.WindowState = FormWindowState.Maximized;
            this.Text = Globale.TITRE;
            this.lblTitre.Text = "Fiche médicament";
            #endregion

            #region paramétrage txtMedicament
            txtMedicament.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtMedicament.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection source = new AutoCompleteStringCollection();
            foreach (Medicament unMedicament in Globale.db.LesMedicaments)
            {
                source.Add(unMedicament.Nom);
            }
            txtMedicament.AutoCompleteCustomSource = source;

            #endregion
            panelFiche.Visible = false;
        }

        #endregion
    }
}
