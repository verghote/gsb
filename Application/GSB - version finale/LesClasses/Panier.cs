﻿/// <summary>
/// Nom du fichier : Panier.cs
/// Objet : Défintion de la classe Panier à l'aide d'un dictionnaire
/// Auteur : Guy Verghote
/// Date mise à jour : 14/03/2019
/// </summary>

using System.Collections.Generic;

namespace lesClasses
{

    public class Panier
    {
        // contructeur alimentation le membre id et instanciant le dictionnaire
        public Panier()
        {
            Contenu = new SortedDictionary<Medicament, int>();
        }

        // propriété automatique

        public SortedDictionary<Medicament, int> Contenu { get; }

        // méthode

        // retourne la quantité associée au médicament passé en paramètre
        // retourne 0 si le médicament n'est pas dans le panier 
        public int getQuantite(Medicament unMedicament)
        {
            if (Contenu.ContainsKey(unMedicament))
                return Contenu[unMedicament];
            else
                return 0;
        }

        // ajoute un médicament dans le panier
        // si le médicament est déjà dans le panier on cumule les quantités
        public void ajouter(Medicament unMedicament, int uneQuantite)
        {
            if (Contenu.ContainsKey(unMedicament))
            {
                Contenu[unMedicament] += uneQuantite;
            }
            else
            {
                Contenu.Add(unMedicament, uneQuantite);
            }
        }

        // modifie la quantité d'un médicament dans le panier
        // retourne 1 si le médicament est présent, 0 si le médicament n'est pas présent
        public int modifierQuantite(Medicament unMedicament, int quantite)
        {
            if (Contenu.ContainsKey(unMedicament))
            {
                Contenu[unMedicament] = quantite;
                return 1;
            }
            else
            {
                return 0;
            }
        }


        // supprime un médicament du  panier
        // retourne 1 si le médicament est supprimé, 0 si le médicament n'est pas présent
        public int supprimer(Medicament unMedicament)
        {
            if (Contenu.ContainsKey(unMedicament))
            {
                Contenu.Remove(unMedicament);
                return 1;
            }
            else
            {
                return 0;
            }
        }
        
        // vide le panier
        public void vider()
        {
            Contenu.Clear();
        }

    }
}
