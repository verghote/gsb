﻿// ------------------------------------------
// Nom du fichier : visite.cs
// Objet : Définition de la classe Visite
// Auteur : M. Verghote
// Date mise à jour : 07/05/2019
// ------------------------------------------

using System;
using System.Collections.Generic;

namespace lesClasses
{
    [Serializable]
    public class Visite : IComparable<Visite>
    {

        public int CompareTo(Visite v)
        {
            int res = Date.CompareTo(v.Date);
            if (res == 0)
            {
                res = Heure.CompareTo(v.Heure);
            }
            return res;
        }

        // Propriétés sans attribut privé
        public int Id { get; }
        public Praticien LePraticien { get; set; }
        public Motif LeMotif { get; set; }
        public DateTime Date { get; set; }
        public string Heure { get; set; }
        public string Bilan { get; set; }
        public Medicament PremierMedicament { get; set; }
        public Medicament SecondMedicament { get; set; }
        public SortedDictionary<Medicament, int> LesMedicamentsDistribues { get; set; }

        // Constructeur
        public Visite(int unId, Praticien unPraticien, Motif unMotif, DateTime uneDate, string uneHeure, String unBilan, Medicament premierMedicament, Medicament secondMedicament)
        {
            Id = unId;
            LePraticien = unPraticien;
            LeMotif = unMotif;
            Date = uneDate;
            Heure = uneHeure;
            Bilan = unBilan;
            PremierMedicament = premierMedicament;
            SecondMedicament = secondMedicament;
            LesMedicamentsDistribues = new SortedDictionary<Medicament, int>();
        }

        // Surcharge du constructeur
        public Visite(int unId, Praticien unPraticien, Motif unMotif, DateTime uneDate, string uneHeure)
        {
            Id = unId;
            LePraticien = unPraticien;
            LeMotif = unMotif;
            Date = uneDate;
            Heure = uneHeure;
            Bilan = null;
            PremierMedicament = null;
            SecondMedicament = null;
            LesMedicamentsDistribues = new SortedDictionary<Medicament, int>();
        }
       
        
    }
       
}
