﻿// ------------------------------------------
// Nom du fichier : famille.cs
// Objet : classe ville
// Auteur : M. Verghote
// Date mise à jour : 30/04/2019
// ------------------------------------------

using System;
using System.Collections.Generic;

namespace lesClasses
{
    [Serializable]
    public class Ville
    {

        // Constructeur
        public Ville (string unNom, string unCode)
        {
            Code = unCode;
            Nom = unNom;
        }


        // Propriétés
        public string Nom { get; set; }
        public string Code { get; set; }

    }
}
