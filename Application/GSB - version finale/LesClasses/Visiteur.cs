﻿// ------------------------------------------
// Nom du fichier : visiteur.cs
// Objet : classe Visiteur
// Auteur : M. Verghote
// Date mise à jour : 28/02/2019
// ------------------------------------------

using System;
using System.Collections.Generic;

namespace lesClasses
{
    [Serializable]
    public class Visiteur : Personne
    {
        // Constructeur
        public Visiteur (string unId, string unNom, string unPrenom, string uneRue, string unCodePostal, string uneVille, DateTime uneDateEmbauche) 
            : base(unNom, unPrenom, uneRue, unCodePostal, uneVille)
        {
            Id = unId;
            DateEmbauche = uneDateEmbauche;
        }

        // Propriétés
        public string Id { get; set; }
        public DateTime DateEmbauche { get; set; }

        // Implémentation de la fonction abstraite "getType()" qui retourne le type de l'objet.
        public override string getType()
        {
            return "Visiteur";
        }
    }
}
