﻿// ------------------------------------------
// Nom du fichier : specialite.cs
// Objet : Définition de la classe Specialite
// Auteur : M. Verghote
// Date mise à jour : 01/03/2019
// ------------------------------------------

namespace lesClasses
{
    public class Specialite
    {
        // Constructeur
        public Specialite(string unId, string unLibelle)
        {
            Id = unId;
            Libelle = unLibelle;
        }


        // Propriétés
        public string Id { get; set; }
        public string Libelle { get; set; }
    }
}
