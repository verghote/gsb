﻿// ------------------------------------------
// Nom du fichier : praticien.cs
// Objet : classe Praticien
// Auteur : M. Verghote
// Date mise à jour : 30/04/2019
// ------------------------------------------

using System;

namespace lesClasses
{
    public class Praticien : Personne, IComparable<Praticien>
    {

        public int CompareTo(Praticien o)
        {
            int res = Nom.CompareTo(o.Nom);
            if (res == 0)
            {
                res = Prenom.CompareTo(o.Prenom);
            }
            return res;
        }

        // Constructeur
        public Praticien (int unId, string unNom, string unPrenom, string uneRue, string unCodePostal, string uneVille, string unEmail, string unTelephone, TypePraticien unType, Specialite uneSpecialite) 
            : base (unNom, unPrenom, uneRue, unCodePostal, uneVille)
        {
            Id = unId;
            Email = unEmail;
            Telephone = unTelephone;
            Type = unType;
            Specialite = uneSpecialite;
        }

        // Propriétés sans attribut privé
        public int Id { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public TypePraticien Type { get; set;}
        public Specialite Specialite { get; set; }


        // Méthodes 
        public override string getType()  { return "Praticien"; }

       
    }
}
