﻿// ------------------------------------------
// Nom du fichier : typePraticien.cs
// Objet : Définition de la classe TypePraticien
// Auteur : M. Verghote
// Date mise à jour : 01/03/2019
// ------------------------------------------

namespace lesClasses
{
    public class TypePraticien
    {
        // Constructeur
        public TypePraticien(string unId, string unLibelle)
        {
            Id = unId;
            Libelle = unLibelle;
        }


        // Propriétés
        public string Id { get; set; }
        public string Libelle { get; set; }
    }
}
