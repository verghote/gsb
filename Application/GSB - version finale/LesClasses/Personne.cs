﻿// ------------------------------------------
// Nom du fichier : personne.cs
// Objet : classe Personne
// Auteur : M. Verghote
// Date mise à jour : 28/02/2019
// ------------------------------------------

using System;
using System.Collections.Generic;

namespace lesClasses
{
    public abstract class Personne
    {
      
        // Constructeur
        public Personne(string unNom, string unPrenom, string uneRue, string unCodePostal, string uneVille)
        {
            Nom = unNom;
            Prenom = unPrenom;
            Rue = uneRue;
            CodePostal = unCodePostal;
            Ville = uneVille;
        }

      
        // Propriétés
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Rue { get; set; }
        public string CodePostal { get; set; }
        public string Ville { get; set; }
        public string NomPrenom { get { return Nom + " " + Prenom; } }


        public override string ToString()
        {
            return NomPrenom;
        }

        // Méthodes 
        public abstract string getType();
    }
}
